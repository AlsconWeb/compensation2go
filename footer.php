</main>
<footer id="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-md-6">
						<?php if ( $slogan = get_field( 'slogan', 'option' ) ): ?>
							<span class="title"><?php echo $slogan; ?></span>
						<?php endif; ?>
						<!--<?php if ( $star_block = get_field( 'star_block_content', 'option' ) ): ?>
									<div class="valuation-box">
										<?php echo $star_block; ?>
									</div>
								<?php endif; ?>-->
						<!-- Ausgezeichnet.org-Siegel: Anfang -->
						<div style="width: 80px; margin-top: 15px; margin-bottom: 25px;">
							<div id="auorg-bg">
								<a href="https://www.ausgezeichnet.org" target="_blank"
								   title="Unabh&auml;ngige Bewertungen, Kundenbewertungen und G&uuml;tesiegel von Ausgezeichnet.org"
								   class="auorg-br">
									<span style="font-size:8px;font-weight:normal;text-transform:uppercase;">Ausgezeichnet.org</span></a>
							</div>
							<script type="text/javascript"
									src="//siegel.ausgezeichnet.org/widgets/js/57dffbc00cf271618230bb20/widget.js"></script>
							<!-- Ausgezeichnet.org-Siegel: Ende -->

						</div>
						<div style="margin-bottom: 25px;">
							<span id="siteseal"><script async type="text/javascript"
														src="https://seal.starfieldtech.com/getSeal?sealID=TAfj1GoxMPVudFabUXqjhYOzbgpiGixA9iOPbLzfq5KIQFBgl7eWdWsKTKFt"></script> </span>
						</div>
					</div>
					<div class="col-md-6">
						<?php if ( $contact_title = get_field( 'contact_title', 'option' ) ): ?>
							<span class="title"><?php echo $contact_title; ?></span>
						<?php endif; ?>
						<?php if ( have_rows( 'contact', 'option' ) ): ?>
							<dl class="contact-block">
								<?php while ( have_rows( 'contact', 'option' ) ) : the_row(); ?>
									<?php if ( $title = get_sub_field( 'title', 'option' ) ): ?>
										<dt><?php echo $title; ?></dt>
									<?php endif; ?>
									<?php $type = get_sub_field( 'type', 'option' ); ?>
									<?php $data = get_sub_field( 'data', 'option' ); ?>
									<?php if ( $type == 'phone' ): ?>
										<dd>
											<a href="tel:<?php echo clean_phone( $data ); ?>">
												<?php echo $data; ?>
											</a>
											<?php if ( $note = get_sub_field( 'note', 'option' ) ): ?> (
												<?php echo $note; ?>)
											<?php endif; ?>
										</dd>
									<?php elseif ( $type == 'email' ): ?>
										<dd>
											<a href="mailto:<?php echo antispambot( $data ); ?>">
												<?php echo $data; ?>
											</a>
										</dd>
									<?php endif; ?>
								<?php endwhile; ?>
							</dl>
							<?php if ( $address = get_field( 'address', 'option' ) ): ?>
								<address><?php echo $address; ?></address>
							<?php endif; ?>
							<a href="http://www.haas-und-partner.com" target="_blank" rel="nofollow"><img
										src="//compensation2go.com/layout-media/haasundpartner_300dpi.png"
										style="width: 200px; height: auto; display: block; margin-top: 15px;"/></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<nav class="nav-footer">
					<?php if ( has_nav_menu( 'footer' ) ) {
						wp_nav_menu( [
								'container'      => false,
								'theme_location' => 'footer',
								'menu_id'        => 'navigation',
								'menu_class'     => 'navigation',
								'items_wrap'     => '<div class="row">%3$s</div>',
								'walker'         => new Custom_Footer_Walker_Nav_Menu,
							]
						);
					} ?>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<span style="display: block; margin-top: 30px;"><sub style="position: relative; top: -5px">1</sub> Auszahlung innerhalb von 24 Stunden nach Bestätigung des Vertragsschlusses per E-Mail. Die Bestätigung erfolgt in der Regel am selben oder folgenden Werktag (je nach Uhrzeit des Eingangs).</span>
			</div>
		</div>
	</div>
</footer>
</div>

<?php wp_footer(); ?>

<!-- Google Tag Manager -->
<script>
	( function( w, d, s, l, i ) {
		w[ l ] = w[ l ] || [];
		w[ l ].push( {
			'gtm.start':
				new Date().getTime(), event: 'gtm.js'
		} );
		var f = d.getElementsByTagName( s )[ 0 ],
			j = d.createElement( s ), dl = l != 'dataLayer' ? '&l=' + l : '';
		j.async = true;
		j.src =
			'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		f.parentNode.insertBefore( j, f );
	} )( window, document, 'script', 'dataLayer', 'GTM-K9DHNCT' );</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	var pathInfo = {
		base: '<?php echo esc_url( get_template_directory_uri() ); ?>/',
		css: 'css/',
		js: 'js/',
		swf: 'swf/',
	};

</script>

<!-- Global site tag (gtag.js) - Google Ads: 759737834 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-759737834"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push( arguments );
	}

	gtag( 'js', new Date() );

	gtag( 'config', 'AW-759737834' );
</script>

<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9DHNCT"
			height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

</body>

</html>