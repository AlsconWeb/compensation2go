<?php get_header(); ?>
	<div class="container-fluid">
		<div id="content">
			<?php if ( have_posts() ) : ?>
				<div class="title">
					<h1><?php echo str_replace("Category: ", "", get_the_archive_title()); ?></h1>
				</div>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'blocks/content', get_post_type() ); ?>
				<?php endwhile; ?>
				<?php get_template_part( 'blocks/pager' ); ?>
			<?php else : ?>
				<?php get_template_part( 'blocks/not_found' ); ?>
			<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>