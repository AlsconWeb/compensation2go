jQuery( function() {
	initFormRedirect();
	initBlockArrow();
	initStickyScrollBlock();
	initMobileNav();
	initRetinaCover();
	initAutocomplete();
	initSlickCarousel();
	initTabs();
	initCustomForms();
	initFormValidation();
} );

jQuery( window ).on( 'load', initStickyTableHead );

function initStickyTableHead() {
	jQuery( '.jcf-scrollable table' ).each( function() {
		var $table = jQuery( this );
		var $stickyTable = $table.clone().addClass( 'sticky-table' ).removeAttr( 'id' );
		var $scrollBox = $table.closest( '.jcf-scrollable' );

		$table.closest( '.jcf-scrollable-wrapper' ).addClass( 'layout-wrap' );

		$stickyTable.find( 'tbody' ).empty();
		$stickyTable.insertBefore( $table );

		function resizeHadler() {
			var $stickyTh = $stickyTable.find( 'thead th' );

			$stickyTable.css( {
				width: $table.outerWidth()
			} );

			$table.find( 'thead th' ).each( function( i ) {
				$stickyTh.eq( i ).css( {
					width: jQuery( this ).outerWidth()
				} );
			} );

			$stickyTable.css( {
				top: $table.position().top || 0
			} );
		}

		function scrollHandler() {
			$stickyTable.css( {
				left: -$scrollBox.scrollLeft() || 0
			} );
		}

		resizeHadler();

		$scrollBox.on( 'scroll', scrollHandler );
		jQuery( window ).on( 'load resize orientationchange', resizeHadler );
	} );
}

// initialize custom form elements
function initCustomForms() {
	jcf.replaceAll();
}

function initFormValidation() {
	jQuery( '.booking-form' ).formValidation( {
		errorClass: 'input-error',
		successClass: 'success'
	} );
}

function initFormRedirect() {
	jQuery( '.booking-form' ).each( function() {
		var $form = jQuery( this );

		function redirectSerialize() {
			var formAction = $form.attr( 'action' );
			var str = $form.serialize();
			var _str = str;

			var rezult = _str.split( '&' ).map( function( item ) {
				var prop = item.split( '=' )[ 0 ];
				var value = findWord( item.split( '=' )[ 1 ] );
				return prop + '=' + value;

			} ).join( '&' );

			function findWord( str ) {
				if ( str ) {
					return str.match( /\([A-Za-z0-9]{1,}\)/g )[ 0 ].slice( 1 ).slice( 0, -1 );
				}
			}

			var actionLink = ( formAction.indexOf( '#/' ) > 0 ) ? formAction.slice( 0, -2 ) : formAction;

			window.open( actionLink + '?' + rezult );
		}

		function notValidate() {
			var input = $form.find( '.form-control' );
			var validn = false;

			input.each( function() {
				validn += ( ! jQuery( this ).val().length );
			} );

			return validn;
		}

		function submitHandler( e ) {
			e.preventDefault();

			if ( ! notValidate() ) {
				redirectSerialize();
			} else {
				initFormValidation();
			}
		}

		$form.on( 'submit', submitHandler );
	} );
}

// content tabs init
function initTabs() {
	jQuery( '.tabset' ).tabset( {
		tabLinks: '.open-tab',
		addToParent: true,
		defaultTab: true,
		onChange: function( $link, $tab ) {
			function setTabbOffset() {
				var positionAllow = $link.offset().left + $link.outerWidth() < $tab.offset().left;
				var viewportAllow = window.innerWidth > 767;

				$tab.css( {
					marginTop: positionAllow && viewportAllow ? $link.position().top : ''
				} );
			}

			setTabbOffset();
			jQuery( window ).off( '.fixtaboffset' ).on( 'resize.fixtaboffset orientation.fixtaboffset', setTabbOffset );
		}
	} );
}

function initBlockArrow() {
	var $win = jQuery( window );
	var $items = jQuery( '.works-list li' );

	function resizeHandler() {
		$items.each( function() {
			var currParam = Math.floor( jQuery( this ).outerHeight() / 2 );

			jQuery( this ).find( '.arrow' ).css( {
				borderBottomWidth: currParam
			} );
			jQuery( this ).find( '.arrow span' ).css( {
				borderBottomWidth: currParam,
				top: currParam
			} );
		} );
	}

	$items.length && $win.on( 'resize orientation load', resizeHandler );
}

// slick init
function initSlickCarousel() {
	jQuery( '.slick-slider' )
		.on( 'init setPosition', centeringArrow )
		.slick( {
			slidesToScroll: 3,
			slidesToShow: 3,
			prevArrow: '<button class="slick-prev"></button>',
			nextArrow: '<button class="slick-next"></button>',
			dots: true,
			dotsClass: 'slick-dots',
			// adaptiveHeight: true,
			responsive: [ {
				breakpoint: 992,
				settings: {
					slidesToScroll: 2,
					slidesToShow: 2,
					variableWidth: false,
					centerMode: false,
					centerPadding: '0px'
				}
			}, {
				breakpoint: 768,
				settings: {
					slidesToScroll: 1,
					slidesToShow: 1
				}
			} ]
		} );

	function centeringArrow( e, instance ) {
		var $elems = instance.$slides.filter( function( i ) {
			return i >= instance.currentSlide && i < instance.currentSlide + instance.options.slidesToShow;
		} ).find( '.text-blockquote' );
		var elemParams = defineMaxParam( $elems.length ? $elems : instance.$slideTrack );

		instance.$nextArrow.add( instance.$prevArrow ).css( {
			top: elemParams.height / 2 + elemParams.position.top
		} );
	}

	function defineMaxParam( $elems ) {
		var obj = {};

		$elems.each( function() {
			var $elem = jQuery( this );
			var elemHeight = $elem.outerHeight();

			if ( ! obj.height || obj.height < elemHeight ) {
				obj = {
					height: elemHeight,
					position: $elem.position()
				};
			}
		} );

		return obj;
	}
}

function initRetinaCover() {
	jQuery( '.bg-retina' ).retinaCover();
}

function initAutocomplete() {
	jQuery( '.autocomplete' ).autoComplete();
}

// loaded state script
( function( w ) {
	w.addEventListener( 'load', function() {
		var loader = document.querySelector( 'html' );
		if ( loader ) {
			loader.classList.add( 'loaded' );
		}
	} );
}( window ) );

// mobile menu init
function initMobileNav() {
	jQuery( 'body' ).mobileNav( {
		menuActiveClass: 'nav-active',
		menuOpener: '.nav-opener',
		// hideOnClickOutside: true,
		menuDrop: 'nav-drop'
	} );
}

// initialize fixed blocks on scroll
function initStickyScrollBlock() {
	ResponsiveHelper.addRange( {
		'992..': {
			on: function() {
				jQuery( '#header' ).stickyScrollBlock( {
					setBoxHeight: true,
					activeClass: 'fixed-position',
					positionType: 'fixed'
				} );
			},
			off: function() {
				jQuery( '#header' ).stickyScrollBlock( 'destroy' );
			}
		}
	} );
}

/*
 * jQuery sticky box plugin
 */
;( function( $, $win ) {
	'use strict';

	function StickyScrollBlock( $stickyBox, options ) {
		this.options = options;
		this.$stickyBox = $stickyBox;
		this.init();
	}

	var StickyScrollBlockPrototype = {
		init: function() {
			this.findElements();
			this.attachEvents();
			this.makeCallback( 'onInit' );
		},

		findElements: function() {
			// find parent container in which will be box move
			this.$container = this.$stickyBox.closest( this.options.container );
			// define box wrap flag
			this.isWrap = this.options.positionType === 'fixed' && this.options.setBoxHeight;
			// define box move flag
			this.moveInContainer = !! this.$container.length;
			// wrapping box to set place in content
			if ( this.isWrap ) {
				this.$stickyBoxWrap = this.$stickyBox.wrap( '<div class="' + this.getWrapClass() + '"/>' ).parent();
			}
			//define block to add active class
			this.parentForActive = this.getParentForActive();
			this.isInit = true;
		},

		attachEvents: function() {
			var self = this;

			// bind events
			this.onResize = function() {
				if ( ! self.isInit ) return;
				self.resetState();
				self.recalculateOffsets();
				self.checkStickyPermission();
				self.scrollHandler();
			};

			this.onScroll = function() {
				self.scrollHandler();
			};

			// initial handler call
			this.onResize();

			// handle events
			$win.on( 'load resize orientationchange', this.onResize )
				.on( 'scroll', this.onScroll );
		},

		defineExtraTop: function() {
			// define box's extra top dimension
			var extraTop;

			if ( typeof this.options.extraTop === 'number' ) {
				extraTop = this.options.extraTop;
			} else if ( typeof this.options.extraTop === 'function' ) {
				extraTop = this.options.extraTop();
			}

			this.extraTop = this.options.positionType === 'absolute' ?
				extraTop :
				Math.min( this.winParams.height - this.data.boxFullHeight, extraTop );
		},

		checkStickyPermission: function() {
			// check the permission to set sticky
			this.isStickyEnabled = this.moveInContainer ?
				this.data.containerOffsetTop + this.data.containerHeight > this.data.boxFullHeight + this.data.boxOffsetTop + this.options.extraBottom :
				true;
		},

		getParentForActive: function() {
			if ( this.isWrap ) {
				return this.$stickyBoxWrap;
			}

			if ( this.$container.length ) {
				return this.$container;
			}

			return this.$stickyBox;
		},

		getWrapClass: function() {
			// get set of container classes
			try {
				return this.$stickyBox.attr( 'class' ).split( ' ' ).map( function( name ) {
					return 'sticky-wrap-' + name;
				} ).join( ' ' );
			} catch ( err ) {
				return 'sticky-wrap';
			}
		},

		resetState: function() {
			// reset dimensions and state
			this.stickyFlag = false;
			this.$stickyBox.css( {
				'-webkit-transition': '',
				'-webkit-transform': '',
				transition: '',
				transform: '',
				position: '',
				width: '',
				left: '',
				top: ''
			} ).removeClass( this.options.activeClass );

			if ( this.isWrap ) {
				this.$stickyBoxWrap.removeClass( this.options.activeClass ).removeAttr( 'style' );
			}

			if ( this.moveInContainer ) {
				this.$container.removeClass( this.options.activeClass );
			}
		},

		recalculateOffsets: function() {
			// define box and container dimensions
			this.winParams = this.getWindowParams();

			this.data = $.extend(
				this.getBoxOffsets(),
				this.getContainerOffsets()
			);

			this.defineExtraTop();
		},

		getBoxOffsets: function() {
			var boxOffset = this.$stickyBox.offset();
			var boxPosition = this.$stickyBox.position();

			return {
				// sticky box offsets
				boxOffsetLeft: boxOffset.left,
				boxOffsetTop: boxOffset.top,
				// sticky box positions
				boxTopPosition: boxPosition.top,
				boxLeftPosition: boxPosition.left,
				// sticky box width/height
				boxFullHeight: this.$stickyBox.outerHeight( true ),
				boxHeight: this.$stickyBox.outerHeight(),
				boxWidth: this.$stickyBox.outerWidth()
			};
		},

		getContainerOffsets: function() {
			var containerOffset = this.moveInContainer ? this.$container.offset() : null;

			return containerOffset ? {
				// container offsets
				containerOffsetLeft: containerOffset.left,
				containerOffsetTop: containerOffset.top,
				// container height
				containerHeight: this.$container.outerHeight()
			} : {};
		},

		getWindowParams: function() {
			return {
				height: window.innerHeight || document.documentElement.clientHeight
			};
		},

		makeCallback: function( name ) {
			if ( typeof this.options[ name ] === 'function' ) {
				var args = Array.prototype.slice.call( arguments );
				args.shift();
				this.options[ name ].apply( this, args );
			}
		},

		destroy: function() {
			this.isInit = false;
			// remove event handlers and styles
			$win.off( 'load resize orientationchange', this.onResize )
				.off( 'scroll', this.onScroll );
			this.resetState();
			this.$stickyBox.removeData( 'StickyScrollBlock' );
			if ( this.isWrap ) {
				this.$stickyBox.unwrap();
			}
			this.makeCallback( 'onDestroy' );
		}
	};

	var stickyMethods = {
		fixed: {
			scrollHandler: function() {
				this.winScrollTop = $win.scrollTop();
				var isActiveSticky = this.winScrollTop -
					( this.options.showAfterScrolled ? this.extraTop : 0 ) -
					( this.options.showAfterScrolled ? this.data.boxHeight + this.extraTop : 0 ) >
					this.data.boxOffsetTop - this.extraTop;

				if ( isActiveSticky ) {
					this.isStickyEnabled && this.stickyOn();
				} else {
					this.stickyOff();
				}
			},

			stickyOn: function() {
				if ( ! this.stickyFlag ) {
					this.stickyFlag = true;
					this.parentForActive.addClass( this.options.activeClass );
					this.$stickyBox.css( {
						width: this.data.boxWidth,
						position: this.options.positionType
					} );
					if ( this.isWrap ) {
						this.$stickyBoxWrap.css( {
							height: this.data.boxFullHeight
						} );
					}
					this.makeCallback( 'fixedOn' );
				}
				this.setDynamicPosition();
			},

			stickyOff: function() {
				if ( this.stickyFlag ) {
					this.stickyFlag = false;
					this.resetState();
					this.makeCallback( 'fixedOff' );
				}
			},

			setDynamicPosition: function() {
				this.$stickyBox.css( {
					top: this.getTopPosition(),
					left: this.data.boxOffsetLeft - $win.scrollLeft()
				} );
			},

			getTopPosition: function() {
				if ( this.moveInContainer ) {
					var currScrollTop = this.winScrollTop + this.data.boxHeight + this.options.extraBottom;

					return Math.min( this.extraTop, ( this.data.containerHeight + this.data.containerOffsetTop ) - currScrollTop );
				} else {
					return this.extraTop;
				}
			}
		},
		absolute: {
			scrollHandler: function() {
				this.winScrollTop = $win.scrollTop();
				var isActiveSticky = this.winScrollTop > this.data.boxOffsetTop - this.extraTop;

				if ( isActiveSticky ) {
					this.isStickyEnabled && this.stickyOn();
				} else {
					this.stickyOff();
				}
			},

			stickyOn: function() {
				if ( ! this.stickyFlag ) {
					this.stickyFlag = true;
					this.parentForActive.addClass( this.options.activeClass );
					this.$stickyBox.css( {
						width: this.data.boxWidth,
						transition: 'transform ' + this.options.animSpeed + 's ease',
						'-webkit-transition': 'transform ' + this.options.animSpeed + 's ease',
					} );

					if ( this.isWrap ) {
						this.$stickyBoxWrap.css( {
							height: this.data.boxFullHeight
						} );
					}

					this.makeCallback( 'fixedOn' );
				}

				this.clearTimer();
				this.timer = setTimeout( function() {
					this.setDynamicPosition();
				}.bind( this ), this.options.animDelay * 1000 );
			},

			stickyOff: function() {
				if ( this.stickyFlag ) {
					this.clearTimer();
					this.stickyFlag = false;

					this.timer = setTimeout( function() {
						this.setDynamicPosition();
						setTimeout( function() {
							this.resetState();
						}.bind( this ), this.options.animSpeed * 1000 );
					}.bind( this ), this.options.animDelay * 1000 );
					this.makeCallback( 'fixedOff' );
				}
			},

			clearTimer: function() {
				clearTimeout( this.timer );
			},

			setDynamicPosition: function() {
				var topPosition = Math.max( 0, this.getTopPosition() );

				this.$stickyBox.css( {
					transform: 'translateY(' + topPosition + 'px)',
					'-webkit-transform': 'translateY(' + topPosition + 'px)'
				} );
			},

			getTopPosition: function() {
				var currTopPosition = this.winScrollTop - this.data.boxOffsetTop + this.extraTop;

				if ( this.moveInContainer ) {
					var currScrollTop = this.winScrollTop + this.data.boxHeight + this.options.extraBottom;
					var diffOffset = Math.abs( Math.min( 0, ( this.data.containerHeight + this.data.containerOffsetTop ) - currScrollTop - this.extraTop ) );

					return currTopPosition - diffOffset;
				} else {
					return currTopPosition;
				}
			}
		}
	};

	// jQuery plugin interface
	$.fn.stickyScrollBlock = function( opt ) {
		var args = Array.prototype.slice.call( arguments );
		var method = args[ 0 ];

		var options = $.extend( {
			container: null,
			positionType: 'fixed', // 'fixed' or 'absolute'
			activeClass: 'fixed-position',
			setBoxHeight: true,
			showAfterScrolled: false,
			extraTop: 0,
			extraBottom: 0,
			animDelay: 0.1,
			animSpeed: 0.2
		}, opt );

		return this.each( function() {
			var $stickyBox = jQuery( this );
			var instance = $stickyBox.data( 'StickyScrollBlock' );

			if ( typeof opt === 'object' || typeof opt === 'undefined' ) {
				StickyScrollBlock.prototype = $.extend( stickyMethods[ options.positionType ], StickyScrollBlockPrototype );
				$stickyBox.data( 'StickyScrollBlock', new StickyScrollBlock( $stickyBox, options ) );
			} else if ( typeof method === 'string' && instance ) {
				if ( typeof instance[ method ] === 'function' ) {
					args.shift();
					instance[ method ].apply( instance, args );
				}
			}
		} );
	};

	// module exports
	window.StickyScrollBlock = StickyScrollBlock;
}( jQuery, jQuery( window ) ) );

/*
 * jQuery Tabs plugin
 */

;( function( $, $win ) {

	'use strict';

	function Tabset( $holder, options ) {
		this.$holder = $holder;
		this.options = options;

		this.init();
	}

	Tabset.prototype = {
		init: function() {
			this.$tabLinks = this.$holder.find( this.options.tabLinks );

			this.setStartActiveIndex();
			this.setActiveTab();

			if ( this.options.autoHeight ) {
				this.$tabHolder = $( this.$tabLinks.eq( 0 ).attr( this.options.attrib ) ).parent();
			}

			this.makeCallback( 'onInit', this );
		},

		setStartActiveIndex: function() {
			var $classTargets = this.getClassTarget( this.$tabLinks );
			var $activeLink = $classTargets.filter( '.' + this.options.activeClass );
			var $hashLink = this.$tabLinks.filter( '[' + this.options.attrib + '="' + location.hash + '"]' );
			var activeIndex;

			if ( this.options.checkHash && $hashLink.length ) {
				$activeLink = $hashLink;
			}

			activeIndex = $classTargets.index( $activeLink );

			this.activeTabIndex = this.prevTabIndex = ( activeIndex === -1 ? ( this.options.defaultTab ? 0 : null ) : activeIndex );
		},

		setActiveTab: function() {
			var self = this;

			this.$tabLinks.each( function( i, link ) {
				var $link = $( link );
				var $classTarget = self.getClassTarget( $link );
				var $tab = $( $link.attr( self.options.attrib ) );

				if ( i !== self.activeTabIndex ) {
					$classTarget.removeClass( self.options.activeClass );
					$tab.addClass( self.options.tabHiddenClass ).removeClass( self.options.activeClass );
				} else {
					$classTarget.addClass( self.options.activeClass );
					$tab.removeClass( self.options.tabHiddenClass ).addClass( self.options.activeClass );
					self.makeCallback( 'onChange', $link, $tab );
				}

				self.attachTabLink( $link, i );
			} );
		},

		attachTabLink: function( $link, i ) {
			var self = this;

			$link.on( this.options.event + '.tabset', function( e ) {
				e.preventDefault();

				if ( self.activeTabIndex === self.prevTabIndex && self.activeTabIndex !== i ) {
					self.activeTabIndex = i;
					self.switchTabs();
				}
			} );
		},

		resizeHolder: function( height ) {
			var self = this;

			if ( height ) {
				this.$tabHolder.height( height );
				setTimeout( function() {
					self.$tabHolder.addClass( 'transition' );
				}, 10 );
			} else {
				self.$tabHolder.removeClass( 'transition' ).height( '' );
			}
		},

		switchTabs: function() {
			var self = this;

			var $prevLink = this.$tabLinks.eq( this.prevTabIndex );
			var $nextLink = this.$tabLinks.eq( this.activeTabIndex );

			var $prevTab = this.getTab( $prevLink );
			var $nextTab = this.getTab( $nextLink );

			$prevTab.removeClass( this.options.activeClass );

			if ( self.haveTabHolder() ) {
				this.resizeHolder( $prevTab.outerHeight() );
			}

			setTimeout( function() {
				self.getClassTarget( $prevLink ).removeClass( self.options.activeClass );

				$prevTab.addClass( self.options.tabHiddenClass );
				$nextTab.removeClass( self.options.tabHiddenClass ).addClass( self.options.activeClass );

				self.getClassTarget( $nextLink ).addClass( self.options.activeClass );

				if ( self.haveTabHolder() ) {
					self.resizeHolder( $nextTab.outerHeight() );

					setTimeout( function() {
						self.resizeHolder();
						self.prevTabIndex = self.activeTabIndex;
					}, self.options.animSpeed );
				} else {
					self.prevTabIndex = self.activeTabIndex;
				}
				self.makeCallback( 'onChange', $nextLink, $nextTab );
			}, this.options.autoHeight ? this.options.animSpeed : 1 );
		},

		getClassTarget: function( $link ) {
			return this.options.addToParent ? $link.parent() : $link;
		},

		getActiveTab: function() {
			return this.getTab( this.$tabLinks.eq( this.activeTabIndex ) );
		},

		getTab: function( $link ) {
			return $( $link.attr( this.options.attrib ) );
		},

		haveTabHolder: function() {
			return this.$tabHolder && this.$tabHolder.length;
		},

		destroy: function() {
			var self = this;

			this.$tabLinks.off( '.tabset' ).each( function() {
				var $link = $( this );

				self.getClassTarget( $link ).removeClass( self.options.activeClass );
				$( $link.attr( self.options.attrib ) ).removeClass( self.options.activeClass + ' ' + self.options.tabHiddenClass );
			} );

			this.$holder.removeData( 'Tabset' );
		},

		makeCallback: function( name ) {
			if ( typeof this.options[ name ] === 'function' ) {
				var args = Array.prototype.slice.call( arguments );
				args.shift();
				this.options[ name ].apply( this, args );
			}
		}
	};

	$.fn.tabset = function( opt ) {
		var args = Array.prototype.slice.call( arguments );
		var method = args[ 0 ];

		var options = $.extend( {
			activeClass: 'active',
			addToParent: false,
			autoHeight: false,
			checkHash: false,
			defaultTab: true,
			animSpeed: 500,
			tabLinks: 'a',
			attrib: 'href',
			event: 'click',
			tabHiddenClass: 'js-tab-hidden'
		}, opt );
		options.autoHeight = options.autoHeight && $.support.opacity;

		return this.each( function() {
			var $holder = jQuery( this );
			var instance = $holder.data( 'Tabset' );

			if ( typeof opt === 'object' || typeof opt === 'undefined' ) {
				$holder.data( 'Tabset', new Tabset( $holder, options ) );
			} else if ( typeof method === 'string' && instance ) {
				if ( typeof instance[ method ] === 'function' ) {
					args.shift();
					instance[ method ].apply( instance, args );
				}
			}
		} );
	};
}( jQuery, jQuery( window ) ) );

/*
 * Responsive Layout helper
 */
window.ResponsiveHelper = ( function( $ ) {
	// init variables
	var handlers = [],
		prevWinWidth,
		win = $( window ),
		nativeMatchMedia = false;

	// detect match media support
	if ( window.matchMedia ) {
		if ( window.Window && window.matchMedia === Window.prototype.matchMedia ) {
			nativeMatchMedia = true;
		} else if ( window.matchMedia.toString().indexOf( 'native' ) > -1 ) {
			nativeMatchMedia = true;
		}
	}

	// prepare resize handler
	function resizeHandler() {
		var winWidth = win.width();
		if ( winWidth !== prevWinWidth ) {
			prevWinWidth = winWidth;

			// loop through range groups
			$.each( handlers, function( index, rangeObject ) {
				// disable current active area if needed
				$.each( rangeObject.data, function( property, item ) {
					if ( item.currentActive && ! matchRange( item.range[ 0 ], item.range[ 1 ] ) ) {
						item.currentActive = false;
						if ( typeof item.disableCallback === 'function' ) {
							item.disableCallback();
						}
					}
				} );

				// enable areas that match current width
				$.each( rangeObject.data, function( property, item ) {
					if ( ! item.currentActive && matchRange( item.range[ 0 ], item.range[ 1 ] ) ) {
						// make callback
						item.currentActive = true;
						if ( typeof item.enableCallback === 'function' ) {
							item.enableCallback();
						}
					}
				} );
			} );
		}
	}

	win.bind( 'load resize orientationchange', resizeHandler );

	// test range
	function matchRange( r1, r2 ) {
		var mediaQueryString = '';
		if ( r1 > 0 ) {
			mediaQueryString += '(min-width: ' + r1 + 'px)';
		}
		if ( r2 < Infinity ) {
			mediaQueryString += ( mediaQueryString ? ' and ' : '' ) + '(max-width: ' + r2 + 'px)';
		}
		return matchQuery( mediaQueryString, r1, r2 );
	}

	// media query function
	function matchQuery( query, r1, r2 ) {
		if ( window.matchMedia && nativeMatchMedia ) {
			return matchMedia( query ).matches;
		} else if ( window.styleMedia ) {
			return styleMedia.matchMedium( query );
		} else if ( window.media ) {
			return media.matchMedium( query );
		} else {
			return prevWinWidth >= r1 && prevWinWidth <= r2;
		}
	}

	// range parser
	function parseRange( rangeStr ) {
		var rangeData = rangeStr.split( '..' );
		var x1 = parseInt( rangeData[ 0 ], 10 ) || -Infinity;
		var x2 = parseInt( rangeData[ 1 ], 10 ) || Infinity;
		return [ x1, x2 ].sort( function( a, b ) {
			return a - b;
		} );
	}

	// export public functions
	return {
		addRange: function( ranges ) {
			// parse data and add items to collection
			var result = { data: {} };
			$.each( ranges, function( property, data ) {
				result.data[ property ] = {
					range: parseRange( property ),
					enableCallback: data.on,
					disableCallback: data.off
				};
			} );
			handlers.push( result );

			// call resizeHandler to recalculate all events
			prevWinWidth = null;
			resizeHandler();
		}
	};
}( jQuery ) );

/*
 * Simple Mobile Navigation
 */
;( function( $ ) {
	function MobileNav( options ) {
		this.options = $.extend( {
			container: null,
			hideOnClickOutside: false,
			menuActiveClass: 'nav-active',
			menuOpener: '.nav-opener',
			menuDrop: '.nav-drop',
			toggleEvent: 'click',
			outsideClickEvent: 'click touchstart pointerdown MSPointerDown'
		}, options );
		this.initStructure();
		this.attachEvents();
	}

	MobileNav.prototype = {
		initStructure: function() {
			this.page = $( 'html' );
			this.container = $( this.options.container );
			this.opener = this.container.find( this.options.menuOpener );
			this.drop = this.container.find( this.options.menuDrop );
		},
		attachEvents: function() {
			var self = this;

			if ( activateResizeHandler ) {
				activateResizeHandler();
				activateResizeHandler = null;
			}

			this.outsideClickHandler = function( e ) {
				if ( self.isOpened() ) {
					var target = $( e.target );
					if ( ! target.closest( self.opener ).length && ! target.closest( self.drop ).length ) {
						self.hide();
					}
				}
			};

			this.openerClickHandler = function( e ) {
				e.preventDefault();
				self.toggle();
			};

			this.opener.on( this.options.toggleEvent, this.openerClickHandler );
		},
		isOpened: function() {
			return this.container.hasClass( this.options.menuActiveClass );
		},
		show: function() {
			this.container.addClass( this.options.menuActiveClass );
			if ( this.options.hideOnClickOutside ) {
				this.page.on( this.options.outsideClickEvent, this.outsideClickHandler );
			}
		},
		hide: function() {
			this.container.removeClass( this.options.menuActiveClass );
			if ( this.options.hideOnClickOutside ) {
				this.page.off( this.options.outsideClickEvent, this.outsideClickHandler );
			}
		},
		toggle: function() {
			if ( this.isOpened() ) {
				this.hide();
			} else {
				this.show();
			}
		},
		destroy: function() {
			this.container.removeClass( this.options.menuActiveClass );
			this.opener.off( this.options.toggleEvent, this.clickHandler );
			this.page.off( this.options.outsideClickEvent, this.outsideClickHandler );
		}
	};

	var activateResizeHandler = function() {
		var win = $( window ),
			doc = $( 'html' ),
			resizeClass = 'resize-active',
			flag, timer;
		var removeClassHandler = function() {
			flag = false;
			doc.removeClass( resizeClass );
		};
		var resizeHandler = function() {
			if ( ! flag ) {
				flag = true;
				doc.addClass( resizeClass );
			}
			clearTimeout( timer );
			timer = setTimeout( removeClassHandler, 500 );
		};
		win.on( 'resize orientationchange', resizeHandler );
	};

	$.fn.mobileNav = function( opt ) {
		var args = Array.prototype.slice.call( arguments );
		var method = args[ 0 ];

		return this.each( function() {
			var $container = jQuery( this );
			var instance = $container.data( 'MobileNav' );

			if ( typeof opt === 'object' || typeof opt === 'undefined' ) {
				$container.data( 'MobileNav', new MobileNav( $.extend( {
					container: this
				}, opt ) ) );
			} else if ( typeof method === 'string' && instance ) {
				if ( typeof instance[ method ] === 'function' ) {
					args.shift();
					instance[ method ].apply( instance, args );
				}
			}
		} );
	};
}( jQuery ) );

/*
 * jQuery retina cover plugin
 */
;( function( $ ) {

	'use strict';

	var styleRules = {};
	var templates = {
		'2x': [
			'(-webkit-min-device-pixel-ratio: 1.5)',
			'(min-resolution: 192dpi)',
			'(min-device-pixel-ratio: 1.5)',
			'(min-resolution: 1.5dppx)'
		],
		'3x': [
			'(-webkit-min-device-pixel-ratio: 3)',
			'(min-resolution: 384dpi)',
			'(min-device-pixel-ratio: 3)',
			'(min-resolution: 3dppx)'
		]
	};

	function addSimple( imageSrc, media, id ) {
		var style = buildRule( id, imageSrc );

		addRule( media, style );
	}

	function addRetina( imageData, media, id ) {
		var currentRules = templates[ imageData[ 1 ] ].slice();
		var patchedRules = currentRules;
		var style = buildRule( id, imageData[ 0 ] );

		if ( media !== 'default' ) {
			patchedRules = $.map( currentRules, function( ele, i ) {
				return ele + ' and ' + media;
			} );
		}

		media = patchedRules.join( ',' );

		addRule( media, style );
	}

	function buildRule( id, src ) {
		return '#' + id + '{background-image: url("' + src + '");}';
	}

	function addRule( media, rule ) {
		var $styleTag = styleRules[ media ];
		var styleTagData;
		var rules = '';

		if ( media === 'default' ) {
			rules = rule + ' ';
		} else {
			rules = '@media ' + media + '{' + rule + '}';
		}

		if ( ! $styleTag ) {
			styleRules[ media ] = $( '<style>' ).text( rules ).appendTo( 'head' );
		} else {
			styleTagData = $styleTag.text();
			styleTagData = styleTagData.substring( 0, styleTagData.length - 2 ) + ' }' + rule + '}';
			$styleTag.text( styleTagData );
		}
	}

	$.fn.retinaCover = function() {
		return this.each( function() {
			var $block = $( this );
			var $items = $block.children( '[data-srcset]' );
			var id = 'bg-stretch' + Date.now() + ( Math.random() * 1000 ).toFixed( 0 );

			if ( $items.length ) {
				$block.attr( 'id', id );

				$items.each( function() {
					var $item = $( this );
					var data = $item.data( 'srcset' ).split( ', ' );
					var media = $item.data( 'media' ) || 'default';
					var dataLength = data.length;
					var itemData;
					var i;

					for ( i = 0; i < dataLength; i++ ) {
						itemData = data[ i ].split( ' ' );

						if ( itemData.length === 1 ) {
							addSimple( itemData[ 0 ], media, id );
						} else {
							addRetina( itemData, media, id );
						}
					}
				} );
			}

			$items.detach();
		} );
	};
}( jQuery ) );

;( function( root, factory ) {
	'use strict';
	if ( typeof define === 'function' && define.amd ) {
		define( [ 'jquery' ], factory );
	} else if ( typeof exports === 'object' ) {
		module.exports = factory( require( 'jquery' ) );
	} else {
		root.AutoComplete = factory( jQuery );
	}
}( this, function( $ ) {
	'use strict';

	function AutoComplete( options ) {
		this.options = $.extend( {
			minSearchLetter: 2,
			focusClass: 'focus'
		}, options );
		this.init();
	}

	AutoComplete.prototype = {
		init: function() {
			if ( this.options.input ) {
				this.findElements();
				this.attachEvents();
				this.makeCallback( 'onInit' );
			}
		},

		findElements: function() {
			this.$input = $( this.options.input ).data( 'AutoComplete', this );
			this.$list = $( '<ul class="autocompete-drop"/>' );
		},

		attachEvents: function() {
			var self = this;

			this.keyupHandler = function( e ) {
				if (
					e.keyCode !== 27 &&
					e.keyCode !== 38 &&
					e.keyCode !== 40 &&
					e.keyCode !== 13 &&
					this.value.length >= self.options.minSearchLetter
				) {
					self.value = this.value;
					self.doSearch();
				} else if ( self.isOpened && this.value.length < self.options.minSearchLetter ) {
					self.hideResult();
				}
			};

			this.keydownHandler = function( e ) {
				switch ( e.keyCode ) {
					case 13:
						if ( self.isOpened ) {
							self.choose();
						}
						e.preventDefault();
						break;
					case 38:
						if ( self.isOpened ) {
							self.setFocus( 1 );
							e.preventDefault();
						}
						break;
					case 40:
						if ( self.isOpened ) {
							self.setFocus( -1 );
							e.preventDefault();
						}
						break;
					case 27:
						self.$input.prop( 'value', self.value );
						self.hideResult();
						e.preventDefault();
						break;
				}
			};

			this.selectHandler = function() {
				self.hideResult();
			};

			this.outsideClickHandler = function( e ) {
				if ( ! $( e.target ).closest( self.$input ).length && ! $( e.target ).closest( self.$list ).length ) {
					self.hideResult();
				}
			};

			this.focusMouseHandler = function() {
				$( this ).siblings().removeClass( self.options.focusClass );
				$( this ).addClass( self.options.focusClass );
				self.$input.prop( 'value', $( this ).text() );
			};

			this.$input.on( 'keyup', this.keyupHandler );
			this.$input.on( 'keydown', this.keydownHandler );
			$( window ).on( 'resize orientationchange', function() {
				if ( self.isOpened ) {
					self.hideResult();
				}
			} );
		},

		choose: function() {
			var $activeItem = this.$list.find( '.' + this.options.focusClass );

			if ( $activeItem.length ) {
				this.$input.prop( 'value', $activeItem.text() );
			}
			this.hideResult();
		},

		setFocus: function( direction ) {
			var $activeItem = this.$list.find( '.' + this.options.focusClass );
			var $items = this.$list.children();
			var currIndex = $activeItem.index();

			if ( direction > 0 && currIndex > 0 ) {
				currIndex--;
			} else if ( direction < 0 && currIndex < $items.length - 1 ) {
				currIndex++;
			}

			$items.removeClass( this.options.focusClass );
			$items.eq( currIndex ).addClass( this.options.focusClass );
			this.$input.prop( 'value', $items.eq( currIndex ).text() );
		},

		doSearch: function() {
			this.$list.remove();
			this.ajaxRequest();
		},

		setPosition: function() {
			var inputOffset = this.$input.offset();

			this.$list.css( {
				width: this.$input.outerWidth(),
				left: inputOffset.left,
				top: inputOffset.top + this.$input.outerHeight()
			} );
		},

		showLoader: function() {
			this.$list.appendTo( 'body' );
			this.$list.html( '<li class="empty-result">Loading...</li>' );
			this.setPosition();
		},

		showResult: function( result ) {
			var content = '';

			this.$list.off( 'mouseenter', 'li', this.focusMouseHandler );
			this.$list.off( 'click', 'li', this.selectHandler );
			$( document ).off( 'click', this.outsideClickHandler );

			if ( result && result.length ) {
				result.forEach( function( obj ) {
					var iata = obj.iata ? ( ' (' + obj.iata + ')' ) : '';
					var name = obj.name ? obj.name : '';
					var city = obj.city ? obj.city + ( iata || name ? ' - ' : '' ) : '';

					content += '<li>' + city + ' ' + name + ' ' + iata + '</li>';
				} );
				this.$list.on( 'mouseenter', 'li', this.focusMouseHandler );
				this.$list.on( 'click', 'li', this.selectHandler );
			} else {
				content = '<li class="empty-result">Keinen Flughafen gefunden.</li>';
			}

			$( document ).on( 'click', this.outsideClickHandler );

			this.$list.html( content );
			this.isOpened = true;
		},

		hideResult: function() {
			this.$list.off( 'mouseenter', 'li', this.focusMouseHandler );
			this.$list.off( 'click', 'li', this.selectHandler );
			$( document ).off( 'click', this.outsideClickHandler );
			this.$list.remove();
			this.isOpened = false;
		},

		ajaxRequest: function() {
			this.xhr && this.xhr.abort && this.xhr.abort();

			this.showLoader();

			this.xhr = $.ajax( {
				url: this.$input.data( 'source' ),
				type: 'GET',
				data: this.$input.serialize(),
				success: function( result ) {
					this.showResult( result.airports );
					this.xhr = null;
				}.bind( this ),
				complete: function() {
					this.xhr = null;
				}.bind( this )
			} );
		},

		makeCallback: function( name ) {
			if ( typeof this.options[ name ] === 'function' ) {
				var args = Array.prototype.slice.call( arguments );
				args.shift();
				this.options[ name ].apply( this, args );
			}
		},

		destroy: function() {
			this.$input.removeData( 'AutoComplete' );
			this.makeCallback( 'onDestroy' );
		}
	};

	$.fn.autoComplete = function( opt ) {
		var args = Array.prototype.slice.call( arguments );
		var method = args[ 0 ];

		return this.each( function() {
			var $input = jQuery( this );
			var instance = $input.data( 'AutoComplete' );

			if ( typeof opt === 'object' || typeof opt === 'undefined' ) {
				new AutoComplete( $.extend( {
					input: this
				}, opt ) );
			} else if ( typeof method === 'string' && instance ) {
				if ( typeof instance[ method ] === 'function' ) {
					args.shift();
					instance[ method ].apply( instance, args );
				}
			}
		} );
	};

	return AutoComplete;
} ) );

/*!
 * JavaScript Custom Forms
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.1.3
 */
;( function( root, factory ) {
	'use strict';
	if ( typeof define === 'function' && define.amd ) {
		define( [ 'jquery' ], factory );
	} else if ( typeof exports === 'object' ) {
		module.exports = factory( require( 'jquery' ) );
	} else {
		root.jcf = factory( jQuery );
	}
}( this, function( $ ) {
	'use strict';

	// define version
	var version = '1.1.3';

	// private variables
	var customInstances = [];

	// default global options
	var commonOptions = {
		optionsKey: 'jcf',
		dataKey: 'jcf-instance',
		rtlClass: 'jcf-rtl',
		focusClass: 'jcf-focus',
		pressedClass: 'jcf-pressed',
		disabledClass: 'jcf-disabled',
		hiddenClass: 'jcf-hidden',
		resetAppearanceClass: 'jcf-reset-appearance',
		unselectableClass: 'jcf-unselectable'
	};

	// detect device type
	var isTouchDevice = ( 'ontouchstart' in window ) || window.DocumentTouch && document instanceof window.DocumentTouch,
		isWinPhoneDevice = /Windows Phone/.test( navigator.userAgent );
	commonOptions.isMobileDevice = !! ( isTouchDevice || isWinPhoneDevice );

	var isIOS = /(iPad|iPhone).*OS ([0-9_]*) .*/.exec( navigator.userAgent );
	if ( isIOS ) isIOS = parseFloat( isIOS[ 2 ].replace( /_/g, '.' ) );
	commonOptions.ios = isIOS;

	// create global stylesheet if custom forms are used
	var createStyleSheet = function() {
		var styleTag = $( '<style>' ).appendTo( 'head' ),
			styleSheet = styleTag.prop( 'sheet' ) || styleTag.prop( 'styleSheet' );

		// crossbrowser style handling
		var addCSSRule = function( selector, rules, index ) {
			if ( styleSheet.insertRule ) {
				styleSheet.insertRule( selector + '{' + rules + '}', index );
			} else {
				styleSheet.addRule( selector, rules, index );
			}
		};

		// add special rules
		addCSSRule( '.' + commonOptions.hiddenClass, 'position:absolute !important;left:-9999px !important;height:1px !important;width:1px !important;margin:0 !important;border-width:0 !important;-webkit-appearance:none;-moz-appearance:none;appearance:none' );
		addCSSRule( '.' + commonOptions.rtlClass + ' .' + commonOptions.hiddenClass, 'right:-9999px !important; left: auto !important' );
		addCSSRule( '.' + commonOptions.unselectableClass, '-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; -webkit-tap-highlight-color: rgba(0,0,0,0);' );
		addCSSRule( '.' + commonOptions.resetAppearanceClass, 'background: none; border: none; -webkit-appearance: none; appearance: none; opacity: 0; filter: alpha(opacity=0);' );

		// detect rtl pages
		var html = $( 'html' ), body = $( 'body' );
		if ( html.css( 'direction' ) === 'rtl' || body.css( 'direction' ) === 'rtl' ) {
			html.addClass( commonOptions.rtlClass );
		}

		// handle form reset event
		html.on( 'reset', function() {
			setTimeout( function() {
				api.refreshAll();
			}, 0 );
		} );

		// mark stylesheet as created
		commonOptions.styleSheetCreated = true;
	};

	// simplified pointer events handler
	( function() {
		var pointerEventsSupported = navigator.pointerEnabled || navigator.msPointerEnabled,
			touchEventsSupported = ( 'ontouchstart' in window ) || window.DocumentTouch && document instanceof window.DocumentTouch,
			eventList, eventMap = {}, eventPrefix = 'jcf-';

		// detect events to attach
		if ( pointerEventsSupported ) {
			eventList = {
				pointerover: navigator.pointerEnabled ? 'pointerover' : 'MSPointerOver',
				pointerdown: navigator.pointerEnabled ? 'pointerdown' : 'MSPointerDown',
				pointermove: navigator.pointerEnabled ? 'pointermove' : 'MSPointerMove',
				pointerup: navigator.pointerEnabled ? 'pointerup' : 'MSPointerUp'
			};
		} else {
			eventList = {
				pointerover: 'mouseover',
				pointerdown: 'mousedown' + ( touchEventsSupported ? ' touchstart' : '' ),
				pointermove: 'mousemove' + ( touchEventsSupported ? ' touchmove' : '' ),
				pointerup: 'mouseup' + ( touchEventsSupported ? ' touchend' : '' )
			};
		}

		// create event map
		$.each( eventList, function( targetEventName, fakeEventList ) {
			$.each( fakeEventList.split( ' ' ), function( index, fakeEventName ) {
				eventMap[ fakeEventName ] = targetEventName;
			} );
		} );

		// jQuery event hooks
		$.each( eventList, function( eventName, eventHandlers ) {
			eventHandlers = eventHandlers.split( ' ' );
			$.event.special[ eventPrefix + eventName ] = {
				setup: function() {
					var self = this;
					$.each( eventHandlers, function( index, fallbackEvent ) {
						if ( self.addEventListener ) self.addEventListener( fallbackEvent, fixEvent, commonOptions.isMobileDevice ? { passive: false } : false );
						else self[ 'on' + fallbackEvent ] = fixEvent;
					} );
				},
				teardown: function() {
					var self = this;
					$.each( eventHandlers, function( index, fallbackEvent ) {
						if ( self.addEventListener ) self.removeEventListener( fallbackEvent, fixEvent, commonOptions.isMobileDevice ? { passive: false } : false );
						else self[ 'on' + fallbackEvent ] = null;
					} );
				}
			};
		} );

		// check that mouse event are not simulated by mobile browsers
		var lastTouch = null;
		var mouseEventSimulated = function( e ) {
			var dx = Math.abs( e.pageX - lastTouch.x ),
				dy = Math.abs( e.pageY - lastTouch.y ),
				rangeDistance = 25;

			if ( dx <= rangeDistance && dy <= rangeDistance ) {
				return true;
			}
		};

		// normalize event
		var fixEvent = function( e ) {
			var origEvent = e || window.event,
				touchEventData = null,
				targetEventName = eventMap[ origEvent.type ];

			e = $.event.fix( origEvent );
			e.type = eventPrefix + targetEventName;

			if ( origEvent.pointerType ) {
				switch ( origEvent.pointerType ) {
					case 2:
						e.pointerType = 'touch';
						break;
					case 3:
						e.pointerType = 'pen';
						break;
					case 4:
						e.pointerType = 'mouse';
						break;
					default:
						e.pointerType = origEvent.pointerType;
				}
			} else {
				e.pointerType = origEvent.type.substr( 0, 5 ); // "mouse" or "touch" word length
			}

			if ( ! e.pageX && ! e.pageY ) {
				touchEventData = origEvent.changedTouches ? origEvent.changedTouches[ 0 ] : origEvent;
				e.pageX = touchEventData.pageX;
				e.pageY = touchEventData.pageY;
			}

			if ( origEvent.type === 'touchend' ) {
				lastTouch = { x: e.pageX, y: e.pageY };
			}
			if ( e.pointerType === 'mouse' && lastTouch && mouseEventSimulated( e ) ) {
				return;
			} else {
				return ( $.event.dispatch || $.event.handle ).call( this, e );
			}
		};
	}() );

	// custom mousewheel/trackpad handler
	( function() {
		var wheelEvents = ( 'onwheel' in document || document.documentMode >= 9 ? 'wheel' : 'mousewheel DOMMouseScroll' ).split( ' ' ),
			shimEventName = 'jcf-mousewheel';

		$.event.special[ shimEventName ] = {
			setup: function() {
				var self = this;
				$.each( wheelEvents, function( index, fallbackEvent ) {
					if ( self.addEventListener ) self.addEventListener( fallbackEvent, fixEvent, false );
					else self[ 'on' + fallbackEvent ] = fixEvent;
				} );
			},
			teardown: function() {
				var self = this;
				$.each( wheelEvents, function( index, fallbackEvent ) {
					if ( self.addEventListener ) self.removeEventListener( fallbackEvent, fixEvent, false );
					else self[ 'on' + fallbackEvent ] = null;
				} );
			}
		};

		var fixEvent = function( e ) {
			var origEvent = e || window.event;
			e = $.event.fix( origEvent );
			e.type = shimEventName;

			// old wheel events handler
			if ( 'detail' in origEvent ) {
				e.deltaY = -origEvent.detail;
			}
			if ( 'wheelDelta' in origEvent ) {
				e.deltaY = -origEvent.wheelDelta;
			}
			if ( 'wheelDeltaY' in origEvent ) {
				e.deltaY = -origEvent.wheelDeltaY;
			}
			if ( 'wheelDeltaX' in origEvent ) {
				e.deltaX = -origEvent.wheelDeltaX;
			}

			// modern wheel event handler
			if ( 'deltaY' in origEvent ) {
				e.deltaY = origEvent.deltaY;
			}
			if ( 'deltaX' in origEvent ) {
				e.deltaX = origEvent.deltaX;
			}

			// handle deltaMode for mouse wheel
			e.delta = e.deltaY || e.deltaX;
			if ( origEvent.deltaMode === 1 ) {
				var lineHeight = 16;
				e.delta *= lineHeight;
				e.deltaY *= lineHeight;
				e.deltaX *= lineHeight;
			}

			return ( $.event.dispatch || $.event.handle ).call( this, e );
		};
	}() );

	// extra module methods
	var moduleMixin = {
		// provide function for firing native events
		fireNativeEvent: function( elements, eventName ) {
			$( elements ).each( function() {
				var element = this, eventObject;
				if ( element.dispatchEvent ) {
					eventObject = document.createEvent( 'HTMLEvents' );
					eventObject.initEvent( eventName, true, true );
					element.dispatchEvent( eventObject );
				} else if ( document.createEventObject ) {
					eventObject = document.createEventObject();
					eventObject.target = element;
					element.fireEvent( 'on' + eventName, eventObject );
				}
			} );
		},
		// bind event handlers for module instance (functions beggining with "on")
		bindHandlers: function() {
			var self = this;
			$.each( self, function( propName, propValue ) {
				if ( propName.indexOf( 'on' ) === 0 && $.isFunction( propValue ) ) {
					// dont use $.proxy here because it doesn't create unique handler
					self[ propName ] = function() {
						return propValue.apply( self, arguments );
					};
				}
			} );
		}
	};

	// public API
	var api = {
		version: version,
		modules: {},
		getOptions: function() {
			return $.extend( {}, commonOptions );
		},
		setOptions: function( moduleName, moduleOptions ) {
			if ( arguments.length > 1 ) {
				// set module options
				if ( this.modules[ moduleName ] ) {
					$.extend( this.modules[ moduleName ].prototype.options, moduleOptions );
				}
			} else {
				// set common options
				$.extend( commonOptions, moduleName );
			}
		},
		addModule: function( proto ) {
			// add module to list
			var Module = function( options ) {
				// save instance to collection
				if ( ! options.element.data( commonOptions.dataKey ) ) {
					options.element.data( commonOptions.dataKey, this );
				}
				customInstances.push( this );

				// save options
				this.options = $.extend( {}, commonOptions, this.options, getInlineOptions( options.element ), options );

				// bind event handlers to instance
				this.bindHandlers();

				// call constructor
				this.init.apply( this, arguments );
			};

			// parse options from HTML attribute
			var getInlineOptions = function( element ) {
				var dataOptions = element.data( commonOptions.optionsKey ),
					attrOptions = element.attr( commonOptions.optionsKey );

				if ( dataOptions ) {
					return dataOptions;
				} else if ( attrOptions ) {
					try {
						return $.parseJSON( attrOptions );
					} catch ( e ) {
						// ignore invalid attributes
					}
				}
			};

			// set proto as prototype for new module
			Module.prototype = proto;

			// add mixin methods to module proto
			$.extend( proto, moduleMixin );
			if ( proto.plugins ) {
				$.each( proto.plugins, function( pluginName, plugin ) {
					$.extend( plugin.prototype, moduleMixin );
				} );
			}

			// override destroy method
			var originalDestroy = Module.prototype.destroy;
			Module.prototype.destroy = function() {
				this.options.element.removeData( this.options.dataKey );

				for ( var i = customInstances.length - 1; i >= 0; i-- ) {
					if ( customInstances[ i ] === this ) {
						customInstances.splice( i, 1 );
						break;
					}
				}

				if ( originalDestroy ) {
					originalDestroy.apply( this, arguments );
				}
			};

			// save module to list
			this.modules[ proto.name ] = Module;
		},
		getInstance: function( element ) {
			return $( element ).data( commonOptions.dataKey );
		},
		replace: function( elements, moduleName, customOptions ) {
			var self = this,
				instance;

			if ( ! commonOptions.styleSheetCreated ) {
				createStyleSheet();
			}

			$( elements ).each( function() {
				var moduleOptions,
					element = $( this );

				instance = element.data( commonOptions.dataKey );
				if ( instance ) {
					instance.refresh();
				} else {
					if ( ! moduleName ) {
						$.each( self.modules, function( currentModuleName, module ) {
							if ( module.prototype.matchElement.call( module.prototype, element ) ) {
								moduleName = currentModuleName;
								return false;
							}
						} );
					}
					if ( moduleName ) {
						moduleOptions = $.extend( { element: element }, customOptions );
						instance = new self.modules[ moduleName ]( moduleOptions );
					}
				}
			} );
			return instance;
		},
		refresh: function( elements ) {
			$( elements ).each( function() {
				var instance = $( this ).data( commonOptions.dataKey );
				if ( instance ) {
					instance.refresh();
				}
			} );
		},
		destroy: function( elements ) {
			$( elements ).each( function() {
				var instance = $( this ).data( commonOptions.dataKey );
				if ( instance ) {
					instance.destroy();
				}
			} );
		},
		replaceAll: function( context ) {
			var self = this;
			$.each( this.modules, function( moduleName, module ) {
				$( module.prototype.selector, context ).each( function() {
					if ( this.className.indexOf( 'jcf-ignore' ) < 0 ) {
						self.replace( this, moduleName );
					}
				} );
			} );
		},
		refreshAll: function( context ) {
			if ( context ) {
				$.each( this.modules, function( moduleName, module ) {
					$( module.prototype.selector, context ).each( function() {
						var instance = $( this ).data( commonOptions.dataKey );
						if ( instance ) {
							instance.refresh();
						}
					} );
				} );
			} else {
				for ( var i = customInstances.length - 1; i >= 0; i-- ) {
					customInstances[ i ].refresh();
				}
			}
		},
		destroyAll: function( context ) {
			if ( context ) {
				$.each( this.modules, function( moduleName, module ) {
					$( module.prototype.selector, context ).each( function( index, element ) {
						var instance = $( element ).data( commonOptions.dataKey );
						if ( instance ) {
							instance.destroy();
						}
					} );
				} );
			} else {
				while ( customInstances.length ) {
					customInstances[ 0 ].destroy();
				}
			}
		}
	};

	// always export API to the global window object
	window.jcf = api;

	return api;
} ) );

/*!
 * JavaScript Custom Forms : Scrollbar Module
 *
 * Copyright 2014-2015 PSD2HTML - http://psd2html.com/jcf
 * Released under the MIT license (LICENSE.txt)
 *
 * Version: 1.1.3
 */
;( function( $, window ) {
	'use strict';

	jcf.addModule( {
		name: 'Scrollable',
		selector: '.jcf-scrollable',
		plugins: {
			ScrollBar: ScrollBar
		},
		options: {
			mouseWheelStep: 150,
			handleResize: true,
			alwaysShowScrollbars: false,
			alwaysPreventMouseWheel: false,
			scrollAreaStructure: '<div class="jcf-scrollable-wrapper"></div>'
		},
		matchElement: function( element ) {
			return element.is( '.jcf-scrollable' );
		},
		init: function() {
			this.initStructure();
			this.attachEvents();
			this.rebuildScrollbars();
		},
		initStructure: function() {
			// prepare structure
			this.doc = $( document );
			this.win = $( window );
			this.realElement = $( this.options.element );
			this.scrollWrapper = $( this.options.scrollAreaStructure ).insertAfter( this.realElement );

			// set initial styles
			this.scrollWrapper.css( 'position', 'relative' );
			this.realElement.css( 'overflow', 'hidden' );
			this.vBarEdge = 0;
		},
		attachEvents: function() {
			// create scrollbars
			var self = this;
			this.vBar = new ScrollBar( {
				holder: this.scrollWrapper,
				vertical: true,
				onScroll: function( scrollTop ) {
					self.realElement.scrollTop( scrollTop );
				}
			} );
			this.hBar = new ScrollBar( {
				holder: this.scrollWrapper,
				vertical: false,
				onScroll: function( scrollLeft ) {
					self.realElement.scrollLeft( scrollLeft );
				}
			} );

			// add event handlers
			this.realElement.on( 'scroll', this.onScroll );
			if ( this.options.handleResize ) {
				this.win.on( 'resize orientationchange load', this.onResize );
			}

			// add pointer/wheel event handlers
			this.realElement.on( 'jcf-mousewheel', this.onMouseWheel );
			this.realElement.on( 'jcf-pointerdown', this.onTouchBody );
		},
		onScroll: function() {
			this.redrawScrollbars();
		},
		onResize: function() {
			// do not rebuild scrollbars if form field is in focus
			if ( ! $( document.activeElement ).is( ':input' ) ) {
				this.rebuildScrollbars();
			}
		},
		onTouchBody: function( e ) {
			if ( e.pointerType === 'touch' ) {
				this.touchData = {
					scrollTop: this.realElement.scrollTop(),
					scrollLeft: this.realElement.scrollLeft(),
					left: e.pageX,
					top: e.pageY
				};
				this.doc.on( {
					'jcf-pointermove': this.onMoveBody,
					'jcf-pointerup': this.onReleaseBody
				} );
			}
		},
		onMoveBody: function( e ) {
			var targetScrollTop,
				targetScrollLeft,
				verticalScrollAllowed = this.verticalScrollActive,
				horizontalScrollAllowed = this.horizontalScrollActive;

			if ( e.pointerType === 'touch' ) {
				targetScrollTop = this.touchData.scrollTop - e.pageY + this.touchData.top;
				targetScrollLeft = this.touchData.scrollLeft - e.pageX + this.touchData.left;

				// check that scrolling is ended and release outer scrolling
				if ( this.verticalScrollActive && ( targetScrollTop < 0 || targetScrollTop > this.vBar.maxValue ) ) {
					verticalScrollAllowed = false;
				}
				if ( this.horizontalScrollActive && ( targetScrollLeft < 0 || targetScrollLeft > this.hBar.maxValue ) ) {
					horizontalScrollAllowed = false;
				}

				this.realElement.scrollTop( targetScrollTop );
				this.realElement.scrollLeft( targetScrollLeft );

				if ( verticalScrollAllowed || horizontalScrollAllowed ) {
					e.preventDefault();
				} else {
					this.onReleaseBody( e );
				}
			}
		},
		onReleaseBody: function( e ) {
			if ( e.pointerType === 'touch' ) {
				delete this.touchData;
				this.doc.off( {
					'jcf-pointermove': this.onMoveBody,
					'jcf-pointerup': this.onReleaseBody
				} );
			}
		},
		onMouseWheel: function( e ) {
			var currentScrollTop = this.realElement.scrollTop(),
				currentScrollLeft = this.realElement.scrollLeft(),
				maxScrollTop = this.realElement.prop( 'scrollHeight' ) - this.embeddedDimensions.innerHeight,
				maxScrollLeft = this.realElement.prop( 'scrollWidth' ) - this.embeddedDimensions.innerWidth,
				extraLeft, extraTop, preventFlag;

			// check edge cases
			if ( ! this.options.alwaysPreventMouseWheel ) {
				if ( this.verticalScrollActive && e.deltaY ) {
					if ( ! ( currentScrollTop <= 0 && e.deltaY < 0 ) && ! ( currentScrollTop >= maxScrollTop && e.deltaY > 0 ) ) {
						preventFlag = true;
					}
				}
				if ( this.horizontalScrollActive && e.deltaX ) {
					if ( ! ( currentScrollLeft <= 0 && e.deltaX < 0 ) && ! ( currentScrollLeft >= maxScrollLeft && e.deltaX > 0 ) ) {
						preventFlag = true;
					}
				}
				if ( ! this.verticalScrollActive && ! this.horizontalScrollActive ) {
					return;
				}
			}

			// prevent default action and scroll item
			if ( preventFlag || this.options.alwaysPreventMouseWheel ) {
				e.preventDefault();
			} else {
				return;
			}

			extraLeft = e.deltaX / 100 * this.options.mouseWheelStep;
			extraTop = e.deltaY / 100 * this.options.mouseWheelStep;

			this.realElement.scrollTop( currentScrollTop + extraTop );
			this.realElement.scrollLeft( currentScrollLeft + extraLeft );
		},
		setScrollBarEdge: function( edgeSize ) {
			this.vBarEdge = edgeSize || 0;
			this.redrawScrollbars();
		},
		saveElementDimensions: function() {
			this.savedDimensions = {
				top: this.realElement.width(),
				left: this.realElement.height()
			};
			return this;
		},
		restoreElementDimensions: function() {
			if ( this.savedDimensions ) {
				this.realElement.css( {
					width: this.savedDimensions.width,
					height: this.savedDimensions.height
				} );
			}
			return this;
		},
		saveScrollOffsets: function() {
			this.savedOffsets = {
				top: this.realElement.scrollTop(),
				left: this.realElement.scrollLeft()
			};
			return this;
		},
		restoreScrollOffsets: function() {
			if ( this.savedOffsets ) {
				this.realElement.scrollTop( this.savedOffsets.top );
				this.realElement.scrollLeft( this.savedOffsets.left );
			}
			return this;
		},
		getContainerDimensions: function() {
			// save current styles
			var desiredDimensions,
				currentStyles,
				currentHeight,
				currentWidth;

			if ( this.isModifiedStyles ) {
				desiredDimensions = {
					width: this.realElement.innerWidth() + this.vBar.getThickness(),
					height: this.realElement.innerHeight() + this.hBar.getThickness()
				};
			} else {
				// unwrap real element and measure it according to CSS
				this.saveElementDimensions().saveScrollOffsets();
				this.realElement.insertAfter( this.scrollWrapper );
				this.scrollWrapper.detach();

				// measure element
				currentStyles = this.realElement.prop( 'style' );
				currentWidth = parseFloat( currentStyles.width );
				currentHeight = parseFloat( currentStyles.height );

				// reset styles if needed
				if ( this.embeddedDimensions && currentWidth && currentHeight ) {
					this.isModifiedStyles |= ( currentWidth !== this.embeddedDimensions.width || currentHeight !== this.embeddedDimensions.height );
					this.realElement.css( {
						overflow: '',
						width: '',
						height: ''
					} );
				}

				// calculate desired dimensions for real element
				desiredDimensions = {
					width: this.realElement.outerWidth(),
					height: this.realElement.outerHeight()
				};

				// restore structure and original scroll offsets
				this.scrollWrapper.insertAfter( this.realElement );
				this.realElement.css( 'overflow', 'hidden' ).prependTo( this.scrollWrapper );
				this.restoreElementDimensions().restoreScrollOffsets();
			}

			return desiredDimensions;
		},
		getEmbeddedDimensions: function( dimensions ) {
			// handle scrollbars cropping
			var fakeBarWidth = this.vBar.getThickness(),
				fakeBarHeight = this.hBar.getThickness(),
				paddingWidth = this.realElement.outerWidth() - this.realElement.width(),
				paddingHeight = this.realElement.outerHeight() - this.realElement.height(),
				resultDimensions;

			if ( this.options.alwaysShowScrollbars ) {
				// simply return dimensions without custom scrollbars
				this.verticalScrollActive = true;
				this.horizontalScrollActive = true;
				resultDimensions = {
					innerWidth: dimensions.width - fakeBarWidth,
					innerHeight: dimensions.height - fakeBarHeight
				};
			} else {
				// detect when to display each scrollbar
				this.saveElementDimensions();
				this.verticalScrollActive = false;
				this.horizontalScrollActive = false;

				// fill container with full size
				this.realElement.css( {
					width: dimensions.width - paddingWidth,
					height: dimensions.height - paddingHeight
				} );

				this.horizontalScrollActive = this.realElement.prop( 'scrollWidth' ) > this.containerDimensions.width;
				this.verticalScrollActive = this.realElement.prop( 'scrollHeight' ) > this.containerDimensions.height;

				this.restoreElementDimensions();
				resultDimensions = {
					innerWidth: dimensions.width - ( this.verticalScrollActive ? fakeBarWidth : 0 ),
					innerHeight: dimensions.height - ( this.horizontalScrollActive ? fakeBarHeight : 0 )
				};
			}
			$.extend( resultDimensions, {
				width: resultDimensions.innerWidth - paddingWidth,
				height: resultDimensions.innerHeight - paddingHeight
			} );
			return resultDimensions;
		},
		rebuildScrollbars: function() {
			// resize wrapper according to real element styles
			this.containerDimensions = this.getContainerDimensions();
			this.embeddedDimensions = this.getEmbeddedDimensions( this.containerDimensions );

			// resize wrapper to desired dimensions
			this.scrollWrapper.css( {
				width: this.containerDimensions.width,
				height: this.containerDimensions.height
			} );

			// resize element inside wrapper excluding scrollbar size
			this.realElement.css( {
				overflow: 'hidden',
				width: this.embeddedDimensions.width,
				height: this.embeddedDimensions.height
			} );

			// redraw scrollbar offset
			this.redrawScrollbars();
		},
		redrawScrollbars: function() {
			var viewSize, maxScrollValue;

			// redraw vertical scrollbar
			if ( this.verticalScrollActive ) {
				viewSize = this.vBarEdge ? this.containerDimensions.height - this.vBarEdge : this.embeddedDimensions.innerHeight;
				maxScrollValue = Math.max( this.realElement.prop( 'offsetHeight' ), this.realElement.prop( 'scrollHeight' ) ) - this.vBarEdge;

				this.vBar.show().setMaxValue( maxScrollValue - viewSize ).setRatio( viewSize / maxScrollValue ).setSize( viewSize );
				this.vBar.setValue( this.realElement.scrollTop() );
			} else {
				this.vBar.hide();
			}

			// redraw horizontal scrollbar
			if ( this.horizontalScrollActive ) {
				viewSize = this.embeddedDimensions.innerWidth;
				maxScrollValue = this.realElement.prop( 'scrollWidth' );

				if ( maxScrollValue === viewSize ) {
					this.horizontalScrollActive = false;
				}
				this.hBar.show().setMaxValue( maxScrollValue - viewSize ).setRatio( viewSize / maxScrollValue ).setSize( viewSize );
				this.hBar.setValue( this.realElement.scrollLeft() );
			} else {
				this.hBar.hide();
			}

			// set "touch-action" style rule
			var touchAction = '';
			if ( this.verticalScrollActive && this.horizontalScrollActive ) {
				touchAction = 'none';
			} else if ( this.verticalScrollActive ) {
				touchAction = 'pan-x';
			} else if ( this.horizontalScrollActive ) {
				touchAction = 'pan-y';
			}
			// this.realElement.css('touchAction', touchAction);
		},
		refresh: function() {
			this.rebuildScrollbars();
		},
		destroy: function() {
			// remove event listeners
			this.win.off( 'resize orientationchange load', this.onResize );
			this.realElement.off( {
				'jcf-mousewheel': this.onMouseWheel,
				'jcf-pointerdown': this.onTouchBody
			} );
			this.doc.off( {
				'jcf-pointermove': this.onMoveBody,
				'jcf-pointerup': this.onReleaseBody
			} );

			// restore structure
			this.saveScrollOffsets();
			this.vBar.destroy();
			this.hBar.destroy();
			this.realElement.insertAfter( this.scrollWrapper ).css( {
				touchAction: '',
				overflow: '',
				width: '',
				height: ''
			} );
			this.scrollWrapper.remove();
			this.restoreScrollOffsets();
		}
	} );

	// custom scrollbar
	function ScrollBar( options ) {
		this.options = $.extend( {
			holder: null,
			vertical: true,
			inactiveClass: 'jcf-inactive',
			verticalClass: 'jcf-scrollbar-vertical',
			horizontalClass: 'jcf-scrollbar-horizontal',
			scrollbarStructure: '<div class="jcf-scrollbar"><div class="jcf-scrollbar-dec"></div><div class="jcf-scrollbar-slider"><div class="jcf-scrollbar-handle"></div></div><div class="jcf-scrollbar-inc"></div></div>',
			btnDecSelector: '.jcf-scrollbar-dec',
			btnIncSelector: '.jcf-scrollbar-inc',
			sliderSelector: '.jcf-scrollbar-slider',
			handleSelector: '.jcf-scrollbar-handle',
			scrollInterval: 300,
			scrollStep: 400 // px/sec
		}, options );
		this.init();
	}

	$.extend( ScrollBar.prototype, {
		init: function() {
			this.initStructure();
			this.attachEvents();
		},
		initStructure: function() {
			// define proporties
			this.doc = $( document );
			this.isVertical = !! this.options.vertical;
			this.sizeProperty = this.isVertical ? 'height' : 'width';
			this.fullSizeProperty = this.isVertical ? 'outerHeight' : 'outerWidth';
			this.invertedSizeProperty = this.isVertical ? 'width' : 'height';
			this.thicknessMeasureMethod = 'outer' + this.invertedSizeProperty.charAt( 0 ).toUpperCase() + this.invertedSizeProperty.substr( 1 );
			this.offsetProperty = this.isVertical ? 'top' : 'left';
			this.offsetEventProperty = this.isVertical ? 'pageY' : 'pageX';

			// initialize variables
			this.value = this.options.value || 0;
			this.maxValue = this.options.maxValue || 0;
			this.currentSliderSize = 0;
			this.handleSize = 0;

			// find elements
			this.holder = $( this.options.holder );
			this.scrollbar = $( this.options.scrollbarStructure ).appendTo( this.holder );
			this.btnDec = this.scrollbar.find( this.options.btnDecSelector );
			this.btnInc = this.scrollbar.find( this.options.btnIncSelector );
			this.slider = this.scrollbar.find( this.options.sliderSelector );
			this.handle = this.slider.find( this.options.handleSelector );

			// set initial styles
			this.scrollbar.addClass( this.isVertical ? this.options.verticalClass : this.options.horizontalClass ).css( {
				touchAction: this.isVertical ? 'pan-x' : 'pan-y',
				position: 'absolute'
			} );
			this.slider.css( {
				position: 'relative'
			} );
			this.handle.css( {
				touchAction: 'none',
				position: 'absolute'
			} );
		},
		attachEvents: function() {
			this.bindHandlers();
			this.handle.on( 'jcf-pointerdown', this.onHandlePress );
			this.slider.add( this.btnDec ).add( this.btnInc ).on( 'jcf-pointerdown', this.onButtonPress );
		},
		onHandlePress: function( e ) {
			if ( e.pointerType === 'mouse' && e.button > 1 ) {
				return;
			} else {
				e.preventDefault();
				this.handleDragActive = true;
				this.sliderOffset = this.slider.offset()[ this.offsetProperty ];
				this.innerHandleOffset = e[ this.offsetEventProperty ] - this.handle.offset()[ this.offsetProperty ];

				this.doc.on( 'jcf-pointermove', this.onHandleDrag );
				this.doc.on( 'jcf-pointerup', this.onHandleRelease );
			}
		},
		onHandleDrag: function( e ) {
			e.preventDefault();
			this.calcOffset = e[ this.offsetEventProperty ] - this.sliderOffset - this.innerHandleOffset;
			this.setValue( this.calcOffset / ( this.currentSliderSize - this.handleSize ) * this.maxValue );
			this.triggerScrollEvent( this.value );
		},
		onHandleRelease: function() {
			this.handleDragActive = false;
			this.doc.off( 'jcf-pointermove', this.onHandleDrag );
			this.doc.off( 'jcf-pointerup', this.onHandleRelease );
		},
		onButtonPress: function( e ) {
			var direction, clickOffset;
			if ( e.pointerType === 'mouse' && e.button > 1 ) {
				return;
			} else {
				e.preventDefault();
				if ( ! this.handleDragActive ) {
					if ( this.slider.is( e.currentTarget ) ) {
						// slider pressed
						direction = this.handle.offset()[ this.offsetProperty ] > e[ this.offsetEventProperty ] ? -1 : 1;
						clickOffset = e[ this.offsetEventProperty ] - this.slider.offset()[ this.offsetProperty ];
						this.startPageScrolling( direction, clickOffset );
					} else {
						// scrollbar buttons pressed
						direction = this.btnDec.is( e.currentTarget ) ? -1 : 1;
						this.startSmoothScrolling( direction );
					}
					this.doc.on( 'jcf-pointerup', this.onButtonRelease );
				}
			}
		},
		onButtonRelease: function() {
			this.stopPageScrolling();
			this.stopSmoothScrolling();
			this.doc.off( 'jcf-pointerup', this.onButtonRelease );
		},
		startPageScrolling: function( direction, clickOffset ) {
			var self = this,
				stepValue = direction * self.currentSize;

			// limit checker
			var isFinishedScrolling = function() {
				var handleTop = ( self.value / self.maxValue ) * ( self.currentSliderSize - self.handleSize );

				if ( direction > 0 ) {
					return handleTop + self.handleSize >= clickOffset;
				} else {
					return handleTop <= clickOffset;
				}
			};

			// scroll by page when track is pressed
			var doPageScroll = function() {
				self.value += stepValue;
				self.setValue( self.value );
				self.triggerScrollEvent( self.value );

				if ( isFinishedScrolling() ) {
					clearInterval( self.pageScrollTimer );
				}
			};

			// start scrolling
			this.pageScrollTimer = setInterval( doPageScroll, this.options.scrollInterval );
			doPageScroll();
		},
		stopPageScrolling: function() {
			clearInterval( this.pageScrollTimer );
		},
		startSmoothScrolling: function( direction ) {
			var self = this, dt;
			this.stopSmoothScrolling();

			// simple animation functions
			var raf = window.requestAnimationFrame || function( func ) {
				setTimeout( func, 16 );
			};
			var getTimestamp = function() {
				return Date.now ? Date.now() : new Date().getTime();
			};

			// set animation limit
			var isFinishedScrolling = function() {
				if ( direction > 0 ) {
					return self.value >= self.maxValue;
				} else {
					return self.value <= 0;
				}
			};

			// animation step
			var doScrollAnimation = function() {
				var stepValue = ( getTimestamp() - dt ) / 1000 * self.options.scrollStep;

				if ( self.smoothScrollActive ) {
					self.value += stepValue * direction;
					self.setValue( self.value );
					self.triggerScrollEvent( self.value );

					if ( ! isFinishedScrolling() ) {
						dt = getTimestamp();
						raf( doScrollAnimation );
					}
				}
			};

			// start animation
			self.smoothScrollActive = true;
			dt = getTimestamp();
			raf( doScrollAnimation );
		},
		stopSmoothScrolling: function() {
			this.smoothScrollActive = false;
		},
		triggerScrollEvent: function( scrollValue ) {
			if ( this.options.onScroll ) {
				this.options.onScroll( scrollValue );
			}
		},
		getThickness: function() {
			return this.scrollbar[ this.thicknessMeasureMethod ]();
		},
		setSize: function( size ) {
			// resize scrollbar
			var btnDecSize = this.btnDec[ this.fullSizeProperty ](),
				btnIncSize = this.btnInc[ this.fullSizeProperty ]();

			// resize slider
			this.currentSize = size;
			this.currentSliderSize = size - btnDecSize - btnIncSize;
			this.scrollbar.css( this.sizeProperty, size );
			this.slider.css( this.sizeProperty, this.currentSliderSize );
			this.currentSliderSize = this.slider[ this.sizeProperty ]();

			// resize handle
			this.handleSize = Math.round( this.currentSliderSize * this.ratio );
			this.handle.css( this.sizeProperty, this.handleSize );
			this.handleSize = this.handle[ this.fullSizeProperty ]();

			return this;
		},
		setRatio: function( ratio ) {
			this.ratio = ratio;
			return this;
		},
		setMaxValue: function( maxValue ) {
			this.maxValue = maxValue;
			this.setValue( Math.min( this.value, this.maxValue ) );
			return this;
		},
		setValue: function( value ) {
			this.value = value;
			if ( this.value < 0 ) {
				this.value = 0;
			} else if ( this.value > this.maxValue ) {
				this.value = this.maxValue;
			}
			this.refresh();
		},
		setPosition: function( styles ) {
			this.scrollbar.css( styles );
			return this;
		},
		hide: function() {
			this.scrollbar.detach();
			return this;
		},
		show: function() {
			this.scrollbar.appendTo( this.holder );
			return this;
		},
		refresh: function() {
			// recalculate handle position
			if ( this.value === 0 || this.maxValue === 0 ) {
				this.calcOffset = 0;
			} else {
				this.calcOffset = ( this.value / this.maxValue ) * ( this.currentSliderSize - this.handleSize );
			}
			this.handle.css( this.offsetProperty, this.calcOffset );

			// toggle inactive classes
			this.btnDec.toggleClass( this.options.inactiveClass, this.value === 0 );
			this.btnInc.toggleClass( this.options.inactiveClass, this.value === this.maxValue );
			this.scrollbar.toggleClass( this.options.inactiveClass, this.maxValue === 0 );
		},
		destroy: function() {
			// remove event handlers and scrollbar block itself
			this.btnDec.add( this.btnInc ).off( 'jcf-pointerdown', this.onButtonPress );
			this.handle.off( 'jcf-pointerdown', this.onHandlePress );
			this.doc.off( 'jcf-pointermove', this.onHandleDrag );
			this.doc.off( 'jcf-pointerup', this.onHandleRelease );
			this.doc.off( 'jcf-pointerup', this.onButtonRelease );
			this.stopSmoothScrolling();
			this.stopPageScrolling();
			this.scrollbar.remove();
		}
	} );

}( jQuery, this ) );

/*
 * jQuery form validation plugin
 */
;( function( $ ) {

	'use strict';

	var FormValidation = ( function() {
		var Validator = function( $field, $fields ) {
			this.$field = $field;
			this.$fields = $fields;
		};

		Validator.prototype = {
			reg: {
				email: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$',
				number: '^[0-9]+$'
			},

			checkField: function() {
				return {
					state: this.run(),
					$fields: this.$field.add( this.additionalFields )
				};
			},

			run: function() {
				var fieldType;

				switch ( this.$field.get( 0 ).tagName.toUpperCase() ) {
					case 'SELECT':
						fieldType = 'select';
						break;

					case 'TEXTAREA':
						fieldType = 'text';
						break;

					default:
						fieldType = this.$field.data( 'type' ) || this.$field.attr( 'type' );
				}

				var functionName = 'check_' + fieldType;
				var state = true;

				if ( $.isFunction( this[ functionName ] ) ) {
					state = this[ functionName ]();

					if ( state && this.$field.data( 'confirm' ) ) {
						state = this.check_confirm();
					}
				}

				return state;
			},

			check_email: function() {
				var value = this.getValue();
				var required = this.$field.data( 'required' );
				var requiredOrValue = required || value.length;

				if ( ( requiredOrValue && ! this.check_regexp( value, this.reg.email ) ) ) {
					return false;
				}

				return requiredOrValue ? true : null;
			},

			check_number: function() {
				var value = this.getValue();
				var required = this.$field.data( 'required' );
				var isNumber = this.check_regexp( value, this.reg.number );
				var requiredOrValue = required || value.length;

				if ( requiredOrValue && ! isNumber ) {
					return false;
				}

				var min = this.$field.data( 'min' );
				var max = this.$field.data( 'max' );
				value = +value;

				if ( ( min && ( value < min || ! isNumber ) ) || ( max && ( value > max || ! isNumber ) ) ) {
					return false;
				}

				return ( requiredOrValue || min || max ) ? true : null;
			},

			check_password: function() {
				return this.check_text();
			},

			check_text: function() {
				var value = this.getValue();
				var required = this.$field.data( 'required' );

				if ( this.$field.data( 'required' ) && ! value.length ) {
					return false;
				}

				var min = +this.$field.data( 'min' );
				var max = +this.$field.data( 'max' );

				if ( ( min && value.length < min ) || ( max && value.length > max ) ) {
					return false;
				}

				var regExp = this.$field.data( 'regexp' );

				if ( regExp && ! this.check_regexp( value, regExp ) ) {
					return false;
				}

				return ( required || min || max || regExp ) ? true : null;
			},

			check_confirm: function() {
				var value = this.getValue();
				var $confirmFields = this.$fields.filter( '[data-confirm="' + this.$field.data( 'confirm' ) + '"]' );
				var confirmState = true;

				for ( var i = $confirmFields.length - 1; i >= 0; i-- ) {
					if ( $confirmFields.eq( i ).val() !== value || ! value.length ) {
						confirmState = false;
						break;
					}
				}

				this.additionalFields = $confirmFields;

				return confirmState;
			},

			check_select: function() {
				var required = this.$field.data( 'required' );

				if ( required && this.$field.get( 0 ).selectedIndex === 0 ) {
					return false;
				}

				return required ? true : null;
			},

			check_radio: function() {
				var $fields = this.$fields.filter( '[name="' + this.$field.attr( 'name' ) + '"]' );
				var required = this.$field.data( 'required' );

				if ( required && ! $fields.filter( ':checked' ).length ) {
					return false;
				}

				this.additionalFields = $fields;

				return required ? true : null;
			},

			check_checkbox: function() {
				var required = this.$field.data( 'required' );

				if ( required && ! this.$field.prop( 'checked' ) ) {
					return false;
				}

				return required ? true : null;
			},

			check_at_least_one: function() {
				var $fields = this.$fields.filter( '[data-name="' + this.$field.data( 'name' ) + '"]' );

				if ( ! $fields.filter( ':checked' ).length ) {
					return false;
				}

				this.additionalFields = $fields;

				return true;
			},

			check_regexp: function( val, exp ) {
				return new RegExp( exp ).test( val );
			},

			getValue: function() {
				if ( this.$field.data( 'trim' ) ) {
					this.$field.val( $.trim( this.$field.val() ) );
				}

				return this.$field.val();
			}
		};

		var publicClass = function( form, options ) {
			this.$form = $( form ).attr( 'novalidate', 'novalidate' );
			this.options = options;
		};

		publicClass.prototype = {
			buildSelector: function( input ) {
				return ':input:not(' + this.options.skipDefaultFields + ( this.options.skipFields ? ',' + this.options.skipFields : '' ) + ')';
			},

			init: function() {
				this.fieldsSelector = this.buildSelector( ':input' );

				this.$form
					.on( 'submit', this.submitHandler.bind( this ) )
					.on( 'keyup blur', this.fieldsSelector, this.changeHandler.bind( this ) )
					.on( 'change', this.buildSelector( 'select' ), this.changeHandler.bind( this ) )
					.on( 'focus', this.fieldsSelector, this.focusHandler.bind( this ) );
			},

			submitHandler: function( e ) {
				var self = this;
				var $fields = this.getFormFields();

				this.getClassTarget( $fields )
					.removeClass( this.options.errorClass + ' ' + this.options.successClass );

				this.setFormState( true );

				$fields.each( function( i, input ) {
					var $field = $( input );
					var $classTarget = self.getClassTarget( $field );

					// continue iteration if $field has error class already
					if ( $classTarget.hasClass( self.options.errorClass ) ) {
						return;
					}

					self.setState( new Validator( $field, $fields ).checkField() );
				} );

				return this.checkSuccess( $fields, e );
			},

			checkSuccess: function( $fields, e ) {
				var self = this;
				var success = this.getClassTarget( $fields || this.getFormFields() )
					.filter( '.' + this.options.errorClass ).length === 0;

				if ( e && success && this.options.successSendClass ) {
					e.preventDefault();

					$.ajax( {
						url: this.$form.removeClass( this.options.successSendClass ).attr( 'action' ) || '/',
						type: this.$form.attr( 'method' ) || 'POST',
						data: this.$form.serialize(),
						success: function() {
							self.$form.addClass( self.options.successSendClass );
						}
					} );
				}

				this.setFormState( success );

				return success;
			},

			changeHandler: function( e ) {
				var $field = $( e.target );

				if ( $field.data( 'interactive' ) ) {
					this.setState( new Validator( $field, this.getFormFields() ).checkField() );
				}

				this.checkSuccess();
			},

			focusHandler: function( e ) {
				var $field = $( e.target );

				this.getClassTarget( $field )
					.removeClass( this.options.errorClass + ' ' + this.options.successClass );

				this.checkSuccess();
			},

			setState: function( result ) {
				this.getClassTarget( result.$fields )
					.toggleClass( this.options.errorClass, result.state !== null && ! result.state )
					.toggleClass( this.options.successClass, result.state !== null && this.options.successClass && !! result.state );
			},

			setFormState: function( state ) {
				if ( this.options.errorFormClass ) {
					this.$form.toggleClass( this.options.errorFormClass, ! state );
				}
			},

			getClassTarget: function( $input ) {
				return ( this.options.addClassToParent ? $input.closest( this.options.addClassToParent ) : $input );
			},

			getFormFields: function() {
				return this.$form.find( this.fieldsSelector );
			}
		};

		return publicClass;
	}() );

	$.fn.formValidation = function( options ) {
		options = $.extend( {}, {
			errorClass: 'input-error',
			successClass: '',
			errorFormClass: '',
			addClassToParent: '',
			skipDefaultFields: ':button, :submit, :image, :hidden, :reset',
			skipFields: '',
			successSendClass: ''
		}, options );

		return this.each( function() {
			new FormValidation( this, options ).init();
		} );
	};
}( jQuery ) );


/*! Picturefill - v3.0.1 - 2015-09-30
 * http://scottjehl.github.io/picturefill
 * Copyright (c) 2015 https://github.com/scottjehl/picturefill/blob/master/Authors.txt; Licensed MIT
 */
! function( a ) {
	var b = navigator.userAgent;
	a.HTMLPictureElement && /ecko/.test( b ) && b.match( /rv\:(\d+)/ ) && RegExp.$1 < 41 && addEventListener( 'resize', function() {
		var b, c = document.createElement( 'source' ), d = function( a ) {
			var b, d, e = a.parentNode;
			'PICTURE' === e.nodeName.toUpperCase() ? ( b = c.cloneNode(), e.insertBefore( b, e.firstElementChild ), setTimeout( function() {
				e.removeChild( b );
			} ) ) : ( ! a._pfLastSize || a.offsetWidth > a._pfLastSize ) && ( a._pfLastSize = a.offsetWidth, d = a.sizes, a.sizes += ',100vw', setTimeout( function() {
				a.sizes = d;
			} ) );
		}, e = function() {
			var a, b = document.querySelectorAll( 'picture > img, img[srcset][sizes]' );
			for ( a = 0; a < b.length; a++ ) d( b[ a ] );
		}, f = function() {
			clearTimeout( b ), b = setTimeout( e, 99 );
		}, g = a.matchMedia && matchMedia( '(orientation: landscape)' ), h = function() {
			f(), g && g.addListener && g.addListener( f );
		};
		return c.srcset = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==', /^[c|i]|d$/.test( document.readyState || '' ) ? h() : document.addEventListener( 'DOMContentLoaded', h ), f;
	}() );
}( window ), function( a, b, c ) {
	'use strict';

	function d( a ) {
		return ' ' === a || '  ' === a || '\n' === a || '\f' === a || '\r' === a;
	}

	function e( b, c ) {
		var d = new a.Image;
		return d.onerror = function() {
			z[ b ] = ! 1, aa();
		}, d.onload = function() {
			z[ b ] = 1 === d.width, aa();
		}, d.src = c, 'pending';
	}

	function f() {
		L = ! 1, O = a.devicePixelRatio, M = {}, N = {}, s.DPR = O || 1, P.width = Math.max( a.innerWidth || 0, y.clientWidth ), P.height = Math.max( a.innerHeight || 0, y.clientHeight ), P.vw = P.width / 100, P.vh = P.height / 100, r = [ P.height, P.width, O ].join( '-' ), P.em = s.getEmValue(), P.rem = P.em;
	}

	function g( a, b, c, d ) {
		var e, f, g, h;
		return 'saveData' === A.algorithm ? a > 2.7 ? h = c + 1 : ( f = b - c, e = Math.pow( a - .6, 1.5 ), g = f * e, d && ( g += .1 * e ), h = a + g ) : h = c > 1 ? Math.sqrt( a * b ) : a, h > c;
	}

	function h( a ) {
		var b, c = s.getSet( a ), d = ! 1;
		'pending' !== c && ( d = r, c && ( b = s.setRes( c ), s.applySetCandidate( b, a ) ) ), a[ s.ns ].evaled = d;
	}

	function i( a, b ) {
		return a.res - b.res;
	}

	function j( a, b, c ) {
		var d;
		return ! c && b && ( c = a[ s.ns ].sets, c = c && c[ c.length - 1 ] ), d = k( b, c ), d && ( b = s.makeUrl( b ), a[ s.ns ].curSrc = b, a[ s.ns ].curCan = d, d.res || _( d, d.set.sizes ) ), d;
	}

	function k( a, b ) {
		var c, d, e;
		if ( a && b ) for ( e = s.parseSet( b ), a = s.makeUrl( a ), c = 0; c < e.length; c++ ) if ( a === s.makeUrl( e[ c ].url ) ) {
			d = e[ c ];
			break;
		}
		return d;
	}

	function l( a, b ) {
		var c, d, e, f, g = a.getElementsByTagName( 'source' );
		for ( c = 0, d = g.length; d > c; c++ ) e = g[ c ], e[ s.ns ] = ! 0, f = e.getAttribute( 'srcset' ), f && b.push( {
			srcset: f,
			media: e.getAttribute( 'media' ),
			type: e.getAttribute( 'type' ),
			sizes: e.getAttribute( 'sizes' )
		} );
	}

	function m( a, b ) {
		function c( b ) {
			var c, d = b.exec( a.substring( m ) );
			return d ? ( c = d[ 0 ], m += c.length, c ) : void 0;
		}

		function e() {
			var a, c, d, e, f, i, j, k, l, m = ! 1, o = {};
			for ( e = 0; e < h.length; e++ ) f = h[ e ], i = f[ f.length - 1 ], j = f.substring( 0, f.length - 1 ), k = parseInt( j, 10 ), l = parseFloat( j ), W.test( j ) && 'w' === i ? ( ( a || c ) && ( m = ! 0 ), 0 === k ? m = ! 0 : a = k ) : X.test( j ) && 'x' === i ? ( ( a || c || d ) && ( m = ! 0 ), 0 > l ? m = ! 0 : c = l ) : W.test( j ) && 'h' === i ? ( ( d || c ) && ( m = ! 0 ), 0 === k ? m = ! 0 : d = k ) : m = ! 0;
			m || ( o.url = g, a && ( o.w = a ), c && ( o.d = c ), d && ( o.h = d ), d || c || a || ( o.d = 1 ), 1 === o.d && ( b.has1x = ! 0 ), o.set = b, n.push( o ) );
		}

		function f() {
			for ( c( S ), i = '', j = 'in descriptor'; ; ) {
				if ( k = a.charAt( m ), 'in descriptor' === j ) if ( d( k ) ) i && ( h.push( i ), i = '', j = 'after descriptor' ); else {
					if ( ',' === k ) return m += 1, i && h.push( i ), void e();
					if ( '(' === k ) i += k, j = 'in parens'; else {
						if ( '' === k ) return i && h.push( i ), void e();
						i += k;
					}
				} else if ( 'in parens' === j ) if ( ')' === k ) i += k, j = 'in descriptor'; else {
					if ( '' === k ) return h.push( i ), void e();
					i += k;
				} else if ( 'after descriptor' === j ) if ( d( k ) ) ; else {
					if ( '' === k ) return void e();
					j = 'in descriptor', m -= 1;
				}
				m += 1;
			}
		}

		for ( var g, h, i, j, k, l = a.length, m = 0, n = []; ; ) {
			if ( c( T ), m >= l ) return n;
			g = c( U ), h = [], ',' === g.slice( -1 ) ? ( g = g.replace( V, '' ), e() ) : f();
		}
	}

	function n( a ) {
		function b( a ) {
			function b() {
				f && ( g.push( f ), f = '' );
			}

			function c() {
				g[ 0 ] && ( h.push( g ), g = [] );
			}

			for ( var e, f = '', g = [], h = [], i = 0, j = 0, k = ! 1; ; ) {
				if ( e = a.charAt( j ), '' === e ) return b(), c(), h;
				if ( k ) {
					if ( '*' === e && '/' === a[ j + 1 ] ) {
						k = ! 1, j += 2, b();
						continue;
					}
					j += 1;
				} else {
					if ( d( e ) ) {
						if ( a.charAt( j - 1 ) && d( a.charAt( j - 1 ) ) || ! f ) {
							j += 1;
							continue;
						}
						if ( 0 === i ) {
							b(), j += 1;
							continue;
						}
						e = ' ';
					} else if ( '(' === e ) i += 1; else if ( ')' === e ) i -= 1; else {
						if ( ',' === e ) {
							b(), c(), j += 1;
							continue;
						}
						if ( '/' === e && '*' === a.charAt( j + 1 ) ) {
							k = ! 0, j += 2;
							continue;
						}
					}
					f += e, j += 1;
				}
			}
		}

		function c( a ) {
			return k.test( a ) && parseFloat( a ) >= 0 ? ! 0 : l.test( a ) ? ! 0 : '0' === a || '-0' === a || '+0' === a ? ! 0 : ! 1;
		}

		var e, f, g, h, i, j,
			k = /^(?:[+-]?[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?(?:ch|cm|em|ex|in|mm|pc|pt|px|rem|vh|vmin|vmax|vw)$/i,
			l = /^calc\((?:[0-9a-z \.\+\-\*\/\(\)]+)\)$/i;
		for ( f = b( a ), g = f.length, e = 0; g > e; e++ ) if ( h = f[ e ], i = h[ h.length - 1 ], c( i ) ) {
			if ( j = i, h.pop(), 0 === h.length ) return j;
			if ( h = h.join( ' ' ), s.matchesMedia( h ) ) return j;
		}
		return '100vw';
	}

	b.createElement( 'picture' );
	var o, p, q, r, s = {}, t = function() {
		}, u = b.createElement( 'img' ), v = u.getAttribute, w = u.setAttribute, x = u.removeAttribute,
		y = b.documentElement, z = {}, A = { algorithm: '' }, B = 'data-pfsrc', C = B + 'set', D = navigator.userAgent,
		E = /rident/.test( D ) || /ecko/.test( D ) && D.match( /rv\:(\d+)/ ) && RegExp.$1 > 35, F = 'currentSrc',
		G = /\s+\+?\d+(e\d+)?w/, H = /(\([^)]+\))?\s*(.+)/, I = a.picturefillCFG,
		J = 'position:absolute;left:0;visibility:hidden;display:block;padding:0;border:none;font-size:1em;width:1em;overflow:hidden;clip:rect(0px, 0px, 0px, 0px)',
		K = 'font-size:100%!important;', L = ! 0, M = {}, N = {}, O = a.devicePixelRatio, P = { px: 1, 'in': 96 },
		Q = b.createElement( 'a' ), R = ! 1, S = /^[ \t\n\r\u000c]+/, T = /^[, \t\n\r\u000c]+/,
		U = /^[^ \t\n\r\u000c]+/, V = /[,]+$/, W = /^\d+$/, X = /^-?(?:[0-9]+|[0-9]*\.[0-9]+)(?:[eE][+-]?[0-9]+)?$/,
		Y = function( a, b, c, d ) {
			a.addEventListener ? a.addEventListener( b, c, d || ! 1 ) : a.attachEvent && a.attachEvent( 'on' + b, c );
		}, Z = function( a ) {
			var b = {};
			return function( c ) {
				return c in b || ( b[ c ] = a( c ) ), b[ c ];
			};
		}, $ = function() {
			var a = /^([\d\.]+)(em|vw|px)$/, b = function() {
				for ( var a = arguments, b = 0, c = a[ 0 ]; ++b in a; ) c = c.replace( a[ b ], a[ ++b ] );
				return c;
			}, c = Z( function( a ) {
				return 'return ' + b( ( a || '' ).toLowerCase(), /\band\b/g, '&&', /,/g, '||', /min-([a-z-\s]+):/g, 'e.$1>=', /max-([a-z-\s]+):/g, 'e.$1<=', /calc([^)]+)/g, '($1)', /(\d+[\.]*[\d]*)([a-z]+)/g, '($1 * e.$2)', /^(?!(e.[a-z]|[0-9\.&=|><\+\-\*\(\)\/])).*/gi, '' ) + ';';
			} );
			return function( b, d ) {
				var e;
				if ( ! ( b in M ) ) if ( M[ b ] = ! 1, d && ( e = b.match( a ) ) ) M[ b ] = e[ 1 ] * P[ e[ 2 ] ]; else try {
					M[ b ] = new Function( 'e', c( b ) )( P );
				} catch ( f ) {
				}
				return M[ b ];
			};
		}(), _ = function( a, b ) {
			return a.w ? ( a.cWidth = s.calcListLength( b || '100vw' ), a.res = a.w / a.cWidth ) : a.res = a.d, a;
		}, aa = function( a ) {
			var c, d, e, f = a || {};
			if ( f.elements && 1 === f.elements.nodeType && ( 'IMG' === f.elements.nodeName.toUpperCase() ? f.elements = [ f.elements ] : ( f.context = f.elements, f.elements = null ) ), c = f.elements || s.qsa( f.context || b, f.reevaluate || f.reselect ? s.sel : s.selShort ), e = c.length ) {
				for ( s.setupRun( f ), R = ! 0, d = 0; e > d; d++ ) s.fillImg( c[ d ], f );
				s.teardownRun( f );
			}
		};
	o = a.console && console.warn ? function( a ) {
		console.warn( a );
	} : t, F in u || ( F = 'src' ), z[ 'image/jpeg' ] = ! 0, z[ 'image/gif' ] = ! 0, z[ 'image/png' ] = ! 0, z[ 'image/svg+xml' ] = b.implementation.hasFeature( 'http://wwwindow.w3.org/TR/SVG11/feature#Image', '1.1' ), s.ns = ( 'pf' + ( new Date ).getTime() ).substr( 0, 9 ), s.supSrcset = 'srcset' in u, s.supSizes = 'sizes' in u, s.supPicture = !! a.HTMLPictureElement, s.supSrcset && s.supPicture && ! s.supSizes && ! function( a ) {
		u.srcset = 'data:,a', a.src = 'data:,a', s.supSrcset = u.complete === a.complete, s.supPicture = s.supSrcset && s.supPicture;
	}( b.createElement( 'img' ) ), s.selShort = 'picture>img,img[srcset]', s.sel = s.selShort, s.cfg = A, s.supSrcset && ( s.sel += ',img[' + C + ']' ), s.DPR = O || 1, s.u = P, s.types = z, q = s.supSrcset && ! s.supSizes, s.setSize = t, s.makeUrl = Z( function( a ) {
		return Q.href = a, Q.href;
	} ), s.qsa = function( a, b ) {
		return a.querySelectorAll( b );
	}, s.matchesMedia = function() {
		return a.matchMedia && ( matchMedia( '(min-width: 0.1em)' ) || {} ).matches ? s.matchesMedia = function( a ) {
			return ! a || matchMedia( a ).matches;
		} : s.matchesMedia = s.mMQ, s.matchesMedia.apply( this, arguments );
	}, s.mMQ = function( a ) {
		return a ? $( a ) : ! 0;
	}, s.calcLength = function( a ) {
		var b = $( a, ! 0 ) || ! 1;
		return 0 > b && ( b = ! 1 ), b;
	}, s.supportsType = function( a ) {
		return a ? z[ a ] : ! 0;
	}, s.parseSize = Z( function( a ) {
		var b = ( a || '' ).match( H );
		return { media: b && b[ 1 ], length: b && b[ 2 ] };
	} ), s.parseSet = function( a ) {
		return a.cands || ( a.cands = m( a.srcset, a ) ), a.cands;
	}, s.getEmValue = function() {
		var a;
		if ( ! p && ( a = b.body ) ) {
			var c = b.createElement( 'div' ), d = y.style.cssText, e = a.style.cssText;
			c.style.cssText = J, y.style.cssText = K, a.style.cssText = K, a.appendChild( c ), p = c.offsetWidth, a.removeChild( c ), p = parseFloat( p, 10 ), y.style.cssText = d, a.style.cssText = e;
		}
		return p || 16;
	}, s.calcListLength = function( a ) {
		if ( ! ( a in N ) || A.uT ) {
			var b = s.calcLength( n( a ) );
			N[ a ] = b ? b : P.width;
		}
		return N[ a ];
	}, s.setRes = function( a ) {
		var b;
		if ( a ) {
			b = s.parseSet( a );
			for ( var c = 0, d = b.length; d > c; c++ ) _( b[ c ], a.sizes );
		}
		return b;
	}, s.setRes.res = _, s.applySetCandidate = function( a, b ) {
		if ( a.length ) {
			var c, d, e, f, h, k, l, m, n, o = b[ s.ns ], p = s.DPR;
			if ( k = o.curSrc || b[ F ], l = o.curCan || j( b, k, a[ 0 ].set ), l && l.set === a[ 0 ].set && ( n = E && ! b.complete && l.res - .1 > p, n || ( l.cached = ! 0, l.res >= p && ( h = l ) ) ), ! h ) for ( a.sort( i ), f = a.length, h = a[ f - 1 ], d = 0; f > d; d++ ) if ( c = a[ d ], c.res >= p ) {
				e = d - 1, h = a[ e ] && ( n || k !== s.makeUrl( c.url ) ) && g( a[ e ].res, c.res, p, a[ e ].cached ) ? a[ e ] : c;
				break;
			}
			h && ( m = s.makeUrl( h.url ), o.curSrc = m, o.curCan = h, m !== k && s.setSrc( b, h ), s.setSize( b ) );
		}
	}, s.setSrc = function( a, b ) {
		var c;
		a.src = b.url, 'image/svg+xml' === b.set.type && ( c = a.style.width, a.style.width = a.offsetWidth + 1 + 'px', a.offsetWidth + 1 && ( a.style.width = c ) );
	}, s.getSet = function( a ) {
		var b, c, d, e = ! 1, f = a[ s.ns ].sets;
		for ( b = 0; b < f.length && ! e; b++ ) if ( c = f[ b ], c.srcset && s.matchesMedia( c.media ) && ( d = s.supportsType( c.type ) ) ) {
			'pending' === d && ( c = d ), e = c;
			break;
		}
		return e;
	}, s.parseSets = function( a, b, d ) {
		var e, f, g, h, i = b && 'PICTURE' === b.nodeName.toUpperCase(), j = a[ s.ns ];
		( j.src === c || d.src ) && ( j.src = v.call( a, 'src' ), j.src ? w.call( a, B, j.src ) : x.call( a, B ) ), ( j.srcset === c || d.srcset || ! s.supSrcset || a.srcset ) && ( e = v.call( a, 'srcset' ), j.srcset = e, h = ! 0 ), j.sets = [], i && ( j.pic = ! 0, l( b, j.sets ) ), j.srcset ? ( f = {
			srcset: j.srcset,
			sizes: v.call( a, 'sizes' )
		}, j.sets.push( f ), g = ( q || j.src ) && G.test( j.srcset || '' ), g || ! j.src || k( j.src, f ) || f.has1x || ( f.srcset += ', ' + j.src, f.cands.push( {
			url: j.src,
			d: 1,
			set: f
		} ) ) ) : j.src && j.sets.push( {
			srcset: j.src,
			sizes: null
		} ), j.curCan = null, j.curSrc = c, j.supported = ! ( i || f && ! s.supSrcset || g ), h && s.supSrcset && ! j.supported && ( e ? ( w.call( a, C, e ), a.srcset = '' ) : x.call( a, C ) ), j.supported && ! j.srcset && ( ! j.src && a.src || a.src !== s.makeUrl( j.src ) ) && ( null === j.src ? a.removeAttribute( 'src' ) : a.src = j.src ), j.parsed = ! 0;
	}, s.fillImg = function( a, b ) {
		var c, d = b.reselect || b.reevaluate;
		a[ s.ns ] || ( a[ s.ns ] = {} ), c = a[ s.ns ], ( d || c.evaled !== r ) && ( ( ! c.parsed || b.reevaluate ) && s.parseSets( a, a.parentNode, b ), c.supported ? c.evaled = r : h( a ) );
	}, s.setupRun = function() {
		( ! R || L || O !== a.devicePixelRatio ) && f();
	}, s.supPicture ? ( aa = t, s.fillImg = t ) : ! function() {
		var c, d = a.attachEvent ? /d$|^c/ : /d$|^c|^i/, e = function() {
			var a = b.readyState || '';
			f = setTimeout( e, 'loading' === a ? 200 : 999 ), b.body && ( s.fillImgs(), c = c || d.test( a ), c && clearTimeout( f ) );
		}, f = setTimeout( e, b.body ? 9 : 99 ), g = function( a, b ) {
			var c, d, e = function() {
				var f = new Date - d;
				b > f ? c = setTimeout( e, b - f ) : ( c = null, a() );
			};
			return function() {
				d = new Date, c || ( c = setTimeout( e, b ) );
			};
		}, h = y.clientHeight, i = function() {
			L = Math.max( a.innerWidth || 0, y.clientWidth ) !== P.width || y.clientHeight !== h, h = y.clientHeight, L && s.fillImgs();
		};
		Y( a, 'resize', g( i, 99 ) ), Y( b, 'readystatechange', e );
	}(), s.picturefill = aa, s.fillImgs = aa, s.teardownRun = t, aa._ = s, a.picturefillCFG = {
		pf: s,
		push: function( a ) {
			var b = a.shift();
			'function' == typeof s[ b ] ? s[ b ].apply( s, a ) : ( A[ b ] = a[ 0 ], R && s.fillImgs( { reselect: ! 0 } ) );
		}
	};
	for ( ; I && I.length; ) a.picturefillCFG.push( I.shift() );
	a.picturefill = aa, 'object' == typeof module && 'object' == typeof module.exports ? module.exports = aa : 'function' == typeof define && define.amd && define( 'picturefill', function() {
		return aa;
	} ), s.supPicture || ( z[ 'image/webp' ] = e( 'image/webp', 'data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA==' ) );
}( window, document );

/*
 _ _      _       _
 ___| (_) ___| | __  (_)___
 / __| | |/ __| |/ /  | / __|
 \__ \ | | (__|   < _ | \__ \
 |___/_|_|\___|_|\_(_)/ |___/
 |__/

 Version: 1.6.0
 Author: Ken Wheeler
 Website: http://kenwheeler.github.io
 Docs: http://kenwheeler.github.io/slick
 Repo: http://github.com/kenwheeler/slick
 Issues: http://github.com/kenwheeler/slick/issues

 */
! function( a ) {
	'use strict';
	'function' == typeof define && define.amd ? define( [ 'jquery' ], a ) : 'undefined' != typeof exports ? module.exports = a( require( 'jquery' ) ) : a( jQuery );
}( function( a ) {
	'use strict';
	var b = window.Slick || {};
	b = function() {
		function c( c, d ) {
			var f, e = this;
			e.defaults = {
				accessibility: ! 0,
				adaptiveHeight: ! 1,
				appendArrows: a( c ),
				appendDots: a( c ),
				arrows: ! 0,
				asNavFor: null,
				prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
				autoplay: ! 1,
				autoplaySpeed: 3e3,
				centerMode: ! 1,
				centerPadding: '50px',
				cssEase: 'ease',
				customPaging: function( b, c ) {
					return a( '<button type="button" data-role="none" role="button" tabindex="0" />' ).text( c + 1 );
				},
				dots: ! 1,
				dotsClass: 'slick-dots',
				draggable: ! 0,
				easing: 'linear',
				edgeFriction: .35,
				fade: ! 1,
				focusOnSelect: ! 1,
				infinite: ! 0,
				initialSlide: 0,
				lazyLoad: 'ondemand',
				mobileFirst: ! 1,
				pauseOnHover: ! 0,
				pauseOnFocus: ! 0,
				pauseOnDotsHover: ! 1,
				respondTo: 'window',
				responsive: null,
				rows: 1,
				rtl: ! 1,
				slide: '',
				slidesPerRow: 1,
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 500,
				swipe: ! 0,
				swipeToSlide: ! 1,
				touchMove: ! 0,
				touchThreshold: 5,
				useCSS: ! 0,
				useTransform: ! 0,
				variableWidth: ! 1,
				vertical: ! 1,
				verticalSwiping: ! 1,
				waitForAnimate: ! 0,
				zIndex: 1e3
			}, e.initials = {
				animating: ! 1,
				dragging: ! 1,
				autoPlayTimer: null,
				currentDirection: 0,
				currentLeft: null,
				currentSlide: 0,
				direction: 1,
				$dots: null,
				listWidth: null,
				listHeight: null,
				loadIndex: 0,
				$nextArrow: null,
				$prevArrow: null,
				slideCount: null,
				slideWidth: null,
				$slideTrack: null,
				$slides: null,
				sliding: ! 1,
				slideOffset: 0,
				swipeLeft: null,
				$list: null,
				touchObject: {},
				transformsEnabled: ! 1,
				unslicked: ! 1
			}, a.extend( e, e.initials ), e.activeBreakpoint = null, e.animType = null, e.animProp = null, e.breakpoints = [], e.breakpointSettings = [], e.cssTransitions = ! 1, e.focussed = ! 1, e.interrupted = ! 1, e.hidden = 'hidden', e.paused = ! 0, e.positionProp = null, e.respondTo = null, e.rowCount = 1, e.shouldClick = ! 0, e.$slider = a( c ), e.$slidesCache = null, e.transformType = null, e.transitionType = null, e.visibilityChange = 'visibilitychange', e.windowWidth = 0, e.windowTimer = null, f = a( c ).data( 'slick' ) || {}, e.options = a.extend( {}, e.defaults, d, f ), e.currentSlide = e.options.initialSlide, e.originalSettings = e.options, 'undefined' != typeof document.mozHidden ? ( e.hidden = 'mozHidden', e.visibilityChange = 'mozvisibilitychange' ) : 'undefined' != typeof document.webkitHidden && ( e.hidden = 'webkitHidden', e.visibilityChange = 'webkitvisibilitychange' ), e.autoPlay = a.proxy( e.autoPlay, e ), e.autoPlayClear = a.proxy( e.autoPlayClear, e ), e.autoPlayIterator = a.proxy( e.autoPlayIterator, e ), e.changeSlide = a.proxy( e.changeSlide, e ), e.clickHandler = a.proxy( e.clickHandler, e ), e.selectHandler = a.proxy( e.selectHandler, e ), e.setPosition = a.proxy( e.setPosition, e ), e.swipeHandler = a.proxy( e.swipeHandler, e ), e.dragHandler = a.proxy( e.dragHandler, e ), e.keyHandler = a.proxy( e.keyHandler, e ), e.instanceUid = b++, e.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, e.registerBreakpoints(), e.init( ! 0 );
		}

		var b = 0;
		return c;
	}(), b.prototype.activateADA = function() {
		var a = this;
		a.$slideTrack.find( '.slick-active' ).attr( { 'aria-hidden': 'false' } ).find( 'a, input, button, select' ).attr( { tabindex: '0' } );
	}, b.prototype.addSlide = b.prototype.slickAdd = function( b, c, d ) {
		var e = this;
		if ( 'boolean' == typeof c ) d = c, c = null; else if ( 0 > c || c >= e.slideCount ) return ! 1;
		e.unload(), 'number' == typeof c ? 0 === c && 0 === e.$slides.length ? a( b ).appendTo( e.$slideTrack ) : d ? a( b ).insertBefore( e.$slides.eq( c ) ) : a( b ).insertAfter( e.$slides.eq( c ) ) : d === ! 0 ? a( b ).prependTo( e.$slideTrack ) : a( b ).appendTo( e.$slideTrack ), e.$slides = e.$slideTrack.children( this.options.slide ), e.$slideTrack.children( this.options.slide ).detach(), e.$slideTrack.append( e.$slides ), e.$slides.each( function( b, c ) {
			a( c ).attr( 'data-slick-index', b );
		} ), e.$slidesCache = e.$slides, e.reinit();
	}, b.prototype.animateHeight = function() {
		var a = this;
		if ( 1 === a.options.slidesToShow && a.options.adaptiveHeight === ! 0 && a.options.vertical === ! 1 ) {
			var b = a.$slides.eq( a.currentSlide ).outerHeight( ! 0 );
			a.$list.animate( { height: b }, a.options.speed );
		}
	}, b.prototype.animateSlide = function( b, c ) {
		var d = {}, e = this;
		e.animateHeight(), e.options.rtl === ! 0 && e.options.vertical === ! 1 && ( b = -b ), e.transformsEnabled === ! 1 ? e.options.vertical === ! 1 ? e.$slideTrack.animate( { left: b }, e.options.speed, e.options.easing, c ) : e.$slideTrack.animate( { top: b }, e.options.speed, e.options.easing, c ) : e.cssTransitions === ! 1 ? ( e.options.rtl === ! 0 && ( e.currentLeft = -e.currentLeft ), a( { animStart: e.currentLeft } ).animate( { animStart: b }, {
			duration: e.options.speed,
			easing: e.options.easing,
			step: function( a ) {
				a = Math.ceil( a ), e.options.vertical === ! 1 ? ( d[ e.animType ] = 'translate(' + a + 'px, 0px)', e.$slideTrack.css( d ) ) : ( d[ e.animType ] = 'translate(0px,' + a + 'px)', e.$slideTrack.css( d ) );
			},
			complete: function() {
				c && c.call();
			}
		} ) ) : ( e.applyTransition(), b = Math.ceil( b ), e.options.vertical === ! 1 ? d[ e.animType ] = 'translate3d(' + b + 'px, 0px, 0px)' : d[ e.animType ] = 'translate3d(0px,' + b + 'px, 0px)', e.$slideTrack.css( d ), c && setTimeout( function() {
			e.disableTransition(), c.call();
		}, e.options.speed ) );
	}, b.prototype.getNavTarget = function() {
		var b = this, c = b.options.asNavFor;
		return c && null !== c && ( c = a( c ).not( b.$slider ) ), c;
	}, b.prototype.asNavFor = function( b ) {
		var c = this, d = c.getNavTarget();
		null !== d && 'object' == typeof d && d.each( function() {
			var c = a( this ).slick( 'getSlick' );
			c.unslicked || c.slideHandler( b, ! 0 );
		} );
	}, b.prototype.applyTransition = function( a ) {
		var b = this, c = {};
		b.options.fade === ! 1 ? c[ b.transitionType ] = b.transformType + ' ' + b.options.speed + 'ms ' + b.options.cssEase : c[ b.transitionType ] = 'opacity ' + b.options.speed + 'ms ' + b.options.cssEase, b.options.fade === ! 1 ? b.$slideTrack.css( c ) : b.$slides.eq( a ).css( c );
	}, b.prototype.autoPlay = function() {
		var a = this;
		a.autoPlayClear(), a.slideCount > a.options.slidesToShow && ( a.autoPlayTimer = setInterval( a.autoPlayIterator, a.options.autoplaySpeed ) );
	}, b.prototype.autoPlayClear = function() {
		var a = this;
		a.autoPlayTimer && clearInterval( a.autoPlayTimer );
	}, b.prototype.autoPlayIterator = function() {
		var a = this, b = a.currentSlide + a.options.slidesToScroll;
		a.paused || a.interrupted || a.focussed || ( a.options.infinite === ! 1 && ( 1 === a.direction && a.currentSlide + 1 === a.slideCount - 1 ? a.direction = 0 : 0 === a.direction && ( b = a.currentSlide - a.options.slidesToScroll, a.currentSlide - 1 === 0 && ( a.direction = 1 ) ) ), a.slideHandler( b ) );
	}, b.prototype.buildArrows = function() {
		var b = this;
		b.options.arrows === ! 0 && ( b.$prevArrow = a( b.options.prevArrow ).addClass( 'slick-arrow' ), b.$nextArrow = a( b.options.nextArrow ).addClass( 'slick-arrow' ), b.slideCount > b.options.slidesToShow ? ( b.$prevArrow.removeClass( 'slick-hidden' ).removeAttr( 'aria-hidden tabindex' ), b.$nextArrow.removeClass( 'slick-hidden' ).removeAttr( 'aria-hidden tabindex' ), b.htmlExpr.test( b.options.prevArrow ) && b.$prevArrow.prependTo( b.options.appendArrows ), b.htmlExpr.test( b.options.nextArrow ) && b.$nextArrow.appendTo( b.options.appendArrows ), b.options.infinite !== ! 0 && b.$prevArrow.addClass( 'slick-disabled' ).attr( 'aria-disabled', 'true' ) ) : b.$prevArrow.add( b.$nextArrow ).addClass( 'slick-hidden' ).attr( {
			'aria-disabled': 'true',
			tabindex: '-1'
		} ) );
	}, b.prototype.buildDots = function() {
		var c, d, b = this;
		if ( b.options.dots === ! 0 && b.slideCount > b.options.slidesToShow ) {
			for ( b.$slider.addClass( 'slick-dotted' ), d = a( '<ul />' ).addClass( b.options.dotsClass ), c = 0; c <= b.getDotCount(); c += 1 ) d.append( a( '<li />' ).append( b.options.customPaging.call( this, b, c ) ) );
			b.$dots = d.appendTo( b.options.appendDots ), b.$dots.find( 'li' ).first().addClass( 'slick-active' ).attr( 'aria-hidden', 'false' );
		}
	}, b.prototype.buildOut = function() {
		var b = this;
		b.$slides = b.$slider.children( b.options.slide + ':not(.slick-cloned)' ).addClass( 'slick-slide' ), b.slideCount = b.$slides.length, b.$slides.each( function( b, c ) {
			a( c ).attr( 'data-slick-index', b ).data( 'originalStyling', a( c ).attr( 'style' ) || '' );
		} ), b.$slider.addClass( 'slick-slider' ), b.$slideTrack = 0 === b.slideCount ? a( '<div class="slick-track"/>' ).appendTo( b.$slider ) : b.$slides.wrapAll( '<div class="slick-track"/>' ).parent(), b.$list = b.$slideTrack.wrap( '<div aria-live="polite" class="slick-list"/>' ).parent(), b.$slideTrack.css( 'opacity', 0 ), ( b.options.centerMode === ! 0 || b.options.swipeToSlide === ! 0 ) && ( b.options.slidesToScroll = 1 ), a( 'img[data-lazy]', b.$slider ).not( '[src]' ).addClass( 'slick-loading' ), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.setSlideClasses( 'number' == typeof b.currentSlide ? b.currentSlide : 0 ), b.options.draggable === ! 0 && b.$list.addClass( 'draggable' );
	}, b.prototype.buildRows = function() {
		var b, c, d, e, f, g, h, a = this;
		if ( e = document.createDocumentFragment(), g = a.$slider.children(), a.options.rows > 1 ) {
			for ( h = a.options.slidesPerRow * a.options.rows, f = Math.ceil( g.length / h ), b = 0; f > b; b++ ) {
				var i = document.createElement( 'div' );
				for ( c = 0; c < a.options.rows; c++ ) {
					var j = document.createElement( 'div' );
					for ( d = 0; d < a.options.slidesPerRow; d++ ) {
						var k = b * h + ( c * a.options.slidesPerRow + d );
						g.get( k ) && j.appendChild( g.get( k ) );
					}
					i.appendChild( j );
				}
				e.appendChild( i );
			}
			a.$slider.empty().append( e ), a.$slider.children().children().children().css( {
				width: 100 / a.options.slidesPerRow + '%',
				display: 'inline-block'
			} );
		}
	}, b.prototype.checkResponsive = function( b, c ) {
		var e, f, g, d = this, h = ! 1, i = d.$slider.width(), j = window.innerWidth || a( window ).width();
		if ( 'window' === d.respondTo ? g = j : 'slider' === d.respondTo ? g = i : 'min' === d.respondTo && ( g = Math.min( j, i ) ), d.options.responsive && d.options.responsive.length && null !== d.options.responsive ) {
			f = null;
			for ( e in d.breakpoints ) d.breakpoints.hasOwnProperty( e ) && ( d.originalSettings.mobileFirst === ! 1 ? g < d.breakpoints[ e ] && ( f = d.breakpoints[ e ] ) : g > d.breakpoints[ e ] && ( f = d.breakpoints[ e ] ) );
			null !== f ? null !== d.activeBreakpoint ? ( f !== d.activeBreakpoint || c ) && ( d.activeBreakpoint = f, 'unslick' === d.breakpointSettings[ f ] ? d.unslick( f ) : ( d.options = a.extend( {}, d.originalSettings, d.breakpointSettings[ f ] ), b === ! 0 && ( d.currentSlide = d.options.initialSlide ), d.refresh( b ) ), h = f ) : ( d.activeBreakpoint = f, 'unslick' === d.breakpointSettings[ f ] ? d.unslick( f ) : ( d.options = a.extend( {}, d.originalSettings, d.breakpointSettings[ f ] ), b === ! 0 && ( d.currentSlide = d.options.initialSlide ), d.refresh( b ) ), h = f ) : null !== d.activeBreakpoint && ( d.activeBreakpoint = null, d.options = d.originalSettings, b === ! 0 && ( d.currentSlide = d.options.initialSlide ), d.refresh( b ), h = f ), b || h === ! 1 || d.$slider.trigger( 'breakpoint', [ d, h ] );
		}
	}, b.prototype.changeSlide = function( b, c ) {
		var f, g, h, d = this, e = a( b.currentTarget );
		switch ( e.is( 'a' ) && b.preventDefault(), e.is( 'li' ) || ( e = e.closest( 'li' ) ), h = d.slideCount % d.options.slidesToScroll !== 0, f = h ? 0 : ( d.slideCount - d.currentSlide ) % d.options.slidesToScroll, b.data.message ) {
			case'previous':
				g = 0 === f ? d.options.slidesToScroll : d.options.slidesToShow - f, d.slideCount > d.options.slidesToShow && d.slideHandler( d.currentSlide - g, ! 1, c );
				break;
			case'next':
				g = 0 === f ? d.options.slidesToScroll : f, d.slideCount > d.options.slidesToShow && d.slideHandler( d.currentSlide + g, ! 1, c );
				break;
			case'index':
				var i = 0 === b.data.index ? 0 : b.data.index || e.index() * d.options.slidesToScroll;
				d.slideHandler( d.checkNavigable( i ), ! 1, c ), e.children().trigger( 'focus' );
				break;
			default:
				return;
		}
	}, b.prototype.checkNavigable = function( a ) {
		var c, d, b = this;
		if ( c = b.getNavigableIndexes(), d = 0, a > c[ c.length - 1 ] ) a = c[ c.length - 1 ]; else for ( var e in c ) {
			if ( a < c[ e ] ) {
				a = d;
				break;
			}
			d = c[ e ];
		}
		return a;
	}, b.prototype.cleanUpEvents = function() {
		var b = this;
		b.options.dots && null !== b.$dots && a( 'li', b.$dots ).off( 'click.slick', b.changeSlide ).off( 'mouseenter.slick', a.proxy( b.interrupt, b, ! 0 ) ).off( 'mouseleave.slick', a.proxy( b.interrupt, b, ! 1 ) ), b.$slider.off( 'focus.slick blur.slick' ), b.options.arrows === ! 0 && b.slideCount > b.options.slidesToShow && ( b.$prevArrow && b.$prevArrow.off( 'click.slick', b.changeSlide ), b.$nextArrow && b.$nextArrow.off( 'click.slick', b.changeSlide ) ), b.$list.off( 'touchstart.slick mousedown.slick', b.swipeHandler ), b.$list.off( 'touchmove.slick mousemove.slick', b.swipeHandler ), b.$list.off( 'touchend.slick mouseup.slick', b.swipeHandler ), b.$list.off( 'touchcancel.slick mouseleave.slick', b.swipeHandler ), b.$list.off( 'click.slick', b.clickHandler ), a( document ).off( b.visibilityChange, b.visibility ), b.cleanUpSlideEvents(), b.options.accessibility === ! 0 && b.$list.off( 'keydown.slick', b.keyHandler ), b.options.focusOnSelect === ! 0 && a( b.$slideTrack ).children().off( 'click.slick', b.selectHandler ), a( window ).off( 'orientationchange.slick.slick-' + b.instanceUid, b.orientationChange ), a( window ).off( 'resize.slick.slick-' + b.instanceUid, b.resize ), a( '[draggable!=true]', b.$slideTrack ).off( 'dragstart', b.preventDefault ), a( window ).off( 'load.slick.slick-' + b.instanceUid, b.setPosition ), a( document ).off( 'ready.slick.slick-' + b.instanceUid, b.setPosition );
	}, b.prototype.cleanUpSlideEvents = function() {
		var b = this;
		b.$list.off( 'mouseenter.slick', a.proxy( b.interrupt, b, ! 0 ) ), b.$list.off( 'mouseleave.slick', a.proxy( b.interrupt, b, ! 1 ) );
	}, b.prototype.cleanUpRows = function() {
		var b, a = this;
		a.options.rows > 1 && ( b = a.$slides.children().children(), b.removeAttr( 'style' ), a.$slider.empty().append( b ) );
	}, b.prototype.clickHandler = function( a ) {
		var b = this;
		b.shouldClick === ! 1 && ( a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault() );
	}, b.prototype.destroy = function( b ) {
		var c = this;
		c.autoPlayClear(), c.touchObject = {}, c.cleanUpEvents(), a( '.slick-cloned', c.$slider ).detach(), c.$dots && c.$dots.remove(), c.$prevArrow && c.$prevArrow.length && ( c.$prevArrow.removeClass( 'slick-disabled slick-arrow slick-hidden' ).removeAttr( 'aria-hidden aria-disabled tabindex' ).css( 'display', '' ), c.htmlExpr.test( c.options.prevArrow ) && c.$prevArrow.remove() ), c.$nextArrow && c.$nextArrow.length && ( c.$nextArrow.removeClass( 'slick-disabled slick-arrow slick-hidden' ).removeAttr( 'aria-hidden aria-disabled tabindex' ).css( 'display', '' ), c.htmlExpr.test( c.options.nextArrow ) && c.$nextArrow.remove() ), c.$slides && ( c.$slides.removeClass( 'slick-slide slick-active slick-center slick-visible slick-current' ).removeAttr( 'aria-hidden' ).removeAttr( 'data-slick-index' ).each( function() {
			a( this ).attr( 'style', a( this ).data( 'originalStyling' ) );
		} ), c.$slideTrack.children( this.options.slide ).detach(), c.$slideTrack.detach(), c.$list.detach(), c.$slider.append( c.$slides ) ), c.cleanUpRows(), c.$slider.removeClass( 'slick-slider' ), c.$slider.removeClass( 'slick-initialized' ), c.$slider.removeClass( 'slick-dotted' ), c.unslicked = ! 0, b || c.$slider.trigger( 'destroy', [ c ] );
	}, b.prototype.disableTransition = function( a ) {
		var b = this, c = {};
		c[ b.transitionType ] = '', b.options.fade === ! 1 ? b.$slideTrack.css( c ) : b.$slides.eq( a ).css( c );
	}, b.prototype.fadeSlide = function( a, b ) {
		var c = this;
		c.cssTransitions === ! 1 ? ( c.$slides.eq( a ).css( { zIndex: c.options.zIndex } ), c.$slides.eq( a ).animate( { opacity: 1 }, c.options.speed, c.options.easing, b ) ) : ( c.applyTransition( a ), c.$slides.eq( a ).css( {
			opacity: 1,
			zIndex: c.options.zIndex
		} ), b && setTimeout( function() {
			c.disableTransition( a ), b.call();
		}, c.options.speed ) );
	}, b.prototype.fadeSlideOut = function( a ) {
		var b = this;
		b.cssTransitions === ! 1 ? b.$slides.eq( a ).animate( {
			opacity: 0,
			zIndex: b.options.zIndex - 2
		}, b.options.speed, b.options.easing ) : ( b.applyTransition( a ), b.$slides.eq( a ).css( {
			opacity: 0,
			zIndex: b.options.zIndex - 2
		} ) );
	}, b.prototype.filterSlides = b.prototype.slickFilter = function( a ) {
		var b = this;
		null !== a && ( b.$slidesCache = b.$slides, b.unload(), b.$slideTrack.children( this.options.slide ).detach(), b.$slidesCache.filter( a ).appendTo( b.$slideTrack ), b.reinit() );
	}, b.prototype.focusHandler = function() {
		var b = this;
		b.$slider.off( 'focus.slick blur.slick' ).on( 'focus.slick blur.slick', '*:not(.slick-arrow)', function( c ) {
			c.stopImmediatePropagation();
			var d = a( this );
			setTimeout( function() {
				b.options.pauseOnFocus && ( b.focussed = d.is( ':focus' ), b.autoPlay() );
			}, 0 );
		} );
	}, b.prototype.getCurrent = b.prototype.slickCurrentSlide = function() {
		var a = this;
		return a.currentSlide;
	}, b.prototype.getDotCount = function() {
		var a = this, b = 0, c = 0, d = 0;
		if ( a.options.infinite === ! 0 ) for ( ; b < a.slideCount; ) ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow; else if ( a.options.centerMode === ! 0 ) d = a.slideCount; else if ( a.options.asNavFor ) for ( ; b < a.slideCount; ) ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow; else d = 1 + Math.ceil( ( a.slideCount - a.options.slidesToShow ) / a.options.slidesToScroll );
		return d - 1;
	}, b.prototype.getLeft = function( a ) {
		var c, d, f, b = this, e = 0;
		return b.slideOffset = 0, d = b.$slides.first().outerHeight( ! 0 ), b.options.infinite === ! 0 ? ( b.slideCount > b.options.slidesToShow && ( b.slideOffset = b.slideWidth * b.options.slidesToShow * -1, e = d * b.options.slidesToShow * -1 ), b.slideCount % b.options.slidesToScroll !== 0 && a + b.options.slidesToScroll > b.slideCount && b.slideCount > b.options.slidesToShow && ( a > b.slideCount ? ( b.slideOffset = ( b.options.slidesToShow - ( a - b.slideCount ) ) * b.slideWidth * -1, e = ( b.options.slidesToShow - ( a - b.slideCount ) ) * d * -1 ) : ( b.slideOffset = b.slideCount % b.options.slidesToScroll * b.slideWidth * -1, e = b.slideCount % b.options.slidesToScroll * d * -1 ) ) ) : a + b.options.slidesToShow > b.slideCount && ( b.slideOffset = ( a + b.options.slidesToShow - b.slideCount ) * b.slideWidth, e = ( a + b.options.slidesToShow - b.slideCount ) * d ), b.slideCount <= b.options.slidesToShow && ( b.slideOffset = 0, e = 0 ), b.options.centerMode === ! 0 && b.options.infinite === ! 0 ? b.slideOffset += b.slideWidth * Math.floor( b.options.slidesToShow / 2 ) - b.slideWidth : b.options.centerMode === ! 0 && ( b.slideOffset = 0, b.slideOffset += b.slideWidth * Math.floor( b.options.slidesToShow / 2 ) ), c = b.options.vertical === ! 1 ? a * b.slideWidth * -1 + b.slideOffset : a * d * -1 + e, b.options.variableWidth === ! 0 && ( f = b.slideCount <= b.options.slidesToShow || b.options.infinite === ! 1 ? b.$slideTrack.children( '.slick-slide' ).eq( a ) : b.$slideTrack.children( '.slick-slide' ).eq( a + b.options.slidesToShow ), c = b.options.rtl === ! 0 ? f[ 0 ] ? -1 * ( b.$slideTrack.width() - f[ 0 ].offsetLeft - f.width() ) : 0 : f[ 0 ] ? -1 * f[ 0 ].offsetLeft : 0, b.options.centerMode === ! 0 && ( f = b.slideCount <= b.options.slidesToShow || b.options.infinite === ! 1 ? b.$slideTrack.children( '.slick-slide' ).eq( a ) : b.$slideTrack.children( '.slick-slide' ).eq( a + b.options.slidesToShow + 1 ), c = b.options.rtl === ! 0 ? f[ 0 ] ? -1 * ( b.$slideTrack.width() - f[ 0 ].offsetLeft - f.width() ) : 0 : f[ 0 ] ? -1 * f[ 0 ].offsetLeft : 0, c += ( b.$list.width() - f.outerWidth() ) / 2 ) ), c;
	}, b.prototype.getOption = b.prototype.slickGetOption = function( a ) {
		var b = this;
		return b.options[ a ];
	}, b.prototype.getNavigableIndexes = function() {
		var e, a = this, b = 0, c = 0, d = [];
		for ( a.options.infinite === ! 1 ? e = a.slideCount : ( b = -1 * a.options.slidesToScroll, c = -1 * a.options.slidesToScroll, e = 2 * a.slideCount ); e > b; ) d.push( b ), b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
		return d;
	}, b.prototype.getSlick = function() {
		return this;
	}, b.prototype.getSlideCount = function() {
		var c, d, e, b = this;
		return e = b.options.centerMode === ! 0 ? b.slideWidth * Math.floor( b.options.slidesToShow / 2 ) : 0, b.options.swipeToSlide === ! 0 ? ( b.$slideTrack.find( '.slick-slide' ).each( function( c, f ) {
			return f.offsetLeft - e + a( f ).outerWidth() / 2 > -1 * b.swipeLeft ? ( d = f, ! 1 ) : void 0;
		} ), c = Math.abs( a( d ).attr( 'data-slick-index' ) - b.currentSlide ) || 1 ) : b.options.slidesToScroll;
	}, b.prototype.goTo = b.prototype.slickGoTo = function( a, b ) {
		var c = this;
		c.changeSlide( { data: { message: 'index', index: parseInt( a ) } }, b );
	}, b.prototype.init = function( b ) {
		var c = this;
		a( c.$slider ).hasClass( 'slick-initialized' ) || ( a( c.$slider ).addClass( 'slick-initialized' ), c.buildRows(), c.buildOut(), c.setProps(), c.startLoad(), c.loadSlider(), c.initializeEvents(), c.updateArrows(), c.updateDots(), c.checkResponsive( ! 0 ), c.focusHandler() ), b && c.$slider.trigger( 'init', [ c ] ), c.options.accessibility === ! 0 && c.initADA(), c.options.autoplay && ( c.paused = ! 1, c.autoPlay() );
	}, b.prototype.initADA = function() {
		var b = this;
		b.$slides.add( b.$slideTrack.find( '.slick-cloned' ) ).attr( {
			'aria-hidden': 'true',
			tabindex: '-1'
		} ).find( 'a, input, button, select' ).attr( { tabindex: '-1' } ), b.$slideTrack.attr( 'role', 'listbox' ), b.$slides.not( b.$slideTrack.find( '.slick-cloned' ) ).each( function( c ) {
			a( this ).attr( { role: 'option', 'aria-describedby': 'slick-slide' + b.instanceUid + c } );
		} ), null !== b.$dots && b.$dots.attr( 'role', 'tablist' ).find( 'li' ).each( function( c ) {
			a( this ).attr( {
				role: 'presentation',
				'aria-selected': 'false',
				'aria-controls': 'navigation' + b.instanceUid + c,
				id: 'slick-slide' + b.instanceUid + c
			} );
		} ).first().attr( 'aria-selected', 'true' ).end().find( 'button' ).attr( 'role', 'button' ).end().closest( 'div' ).attr( 'role', 'toolbar' ), b.activateADA();
	}, b.prototype.initArrowEvents = function() {
		var a = this;
		a.options.arrows === ! 0 && a.slideCount > a.options.slidesToShow && ( a.$prevArrow.off( 'click.slick' ).on( 'click.slick', { message: 'previous' }, a.changeSlide ), a.$nextArrow.off( 'click.slick' ).on( 'click.slick', { message: 'next' }, a.changeSlide ) );
	}, b.prototype.initDotEvents = function() {
		var b = this;
		b.options.dots === ! 0 && b.slideCount > b.options.slidesToShow && a( 'li', b.$dots ).on( 'click.slick', { message: 'index' }, b.changeSlide ), b.options.dots === ! 0 && b.options.pauseOnDotsHover === ! 0 && a( 'li', b.$dots ).on( 'mouseenter.slick', a.proxy( b.interrupt, b, ! 0 ) ).on( 'mouseleave.slick', a.proxy( b.interrupt, b, ! 1 ) );
	}, b.prototype.initSlideEvents = function() {
		var b = this;
		b.options.pauseOnHover && ( b.$list.on( 'mouseenter.slick', a.proxy( b.interrupt, b, ! 0 ) ), b.$list.on( 'mouseleave.slick', a.proxy( b.interrupt, b, ! 1 ) ) );
	}, b.prototype.initializeEvents = function() {
		var b = this;
		b.initArrowEvents(), b.initDotEvents(), b.initSlideEvents(), b.$list.on( 'touchstart.slick mousedown.slick', { action: 'start' }, b.swipeHandler ), b.$list.on( 'touchmove.slick mousemove.slick', { action: 'move' }, b.swipeHandler ), b.$list.on( 'touchend.slick mouseup.slick', { action: 'end' }, b.swipeHandler ), b.$list.on( 'touchcancel.slick mouseleave.slick', { action: 'end' }, b.swipeHandler ), b.$list.on( 'click.slick', b.clickHandler ), a( document ).on( b.visibilityChange, a.proxy( b.visibility, b ) ), b.options.accessibility === ! 0 && b.$list.on( 'keydown.slick', b.keyHandler ), b.options.focusOnSelect === ! 0 && a( b.$slideTrack ).children().on( 'click.slick', b.selectHandler ), a( window ).on( 'orientationchange.slick.slick-' + b.instanceUid, a.proxy( b.orientationChange, b ) ), a( window ).on( 'resize.slick.slick-' + b.instanceUid, a.proxy( b.resize, b ) ), a( '[draggable!=true]', b.$slideTrack ).on( 'dragstart', b.preventDefault ), a( window ).on( 'load.slick.slick-' + b.instanceUid, b.setPosition ), a( document ).on( 'ready.slick.slick-' + b.instanceUid, b.setPosition );
	}, b.prototype.initUI = function() {
		var a = this;
		a.options.arrows === ! 0 && a.slideCount > a.options.slidesToShow && ( a.$prevArrow.show(), a.$nextArrow.show() ), a.options.dots === ! 0 && a.slideCount > a.options.slidesToShow && a.$dots.show();
	}, b.prototype.keyHandler = function( a ) {
		var b = this;
		a.target.tagName.match( 'TEXTAREA|INPUT|SELECT' ) || ( 37 === a.keyCode && b.options.accessibility === ! 0 ? b.changeSlide( { data: { message: b.options.rtl === ! 0 ? 'next' : 'previous' } } ) : 39 === a.keyCode && b.options.accessibility === ! 0 && b.changeSlide( { data: { message: b.options.rtl === ! 0 ? 'previous' : 'next' } } ) );
	}, b.prototype.lazyLoad = function() {
		function g( c ) {
			a( 'img[data-lazy]', c ).each( function() {
				var c = a( this ), d = a( this ).attr( 'data-lazy' ), e = document.createElement( 'img' );
				e.onload = function() {
					c.animate( { opacity: 0 }, 100, function() {
						c.attr( 'src', d ).animate( { opacity: 1 }, 200, function() {
							c.removeAttr( 'data-lazy' ).removeClass( 'slick-loading' );
						} ), b.$slider.trigger( 'lazyLoaded', [ b, c, d ] );
					} );
				}, e.onerror = function() {
					c.removeAttr( 'data-lazy' ).removeClass( 'slick-loading' ).addClass( 'slick-lazyload-error' ), b.$slider.trigger( 'lazyLoadError', [ b, c, d ] );
				}, e.src = d;
			} );
		}

		var c, d, e, f, b = this;
		b.options.centerMode === ! 0 ? b.options.infinite === ! 0 ? ( e = b.currentSlide + ( b.options.slidesToShow / 2 + 1 ), f = e + b.options.slidesToShow + 2 ) : ( e = Math.max( 0, b.currentSlide - ( b.options.slidesToShow / 2 + 1 ) ), f = 2 + ( b.options.slidesToShow / 2 + 1 ) + b.currentSlide ) : ( e = b.options.infinite ? b.options.slidesToShow + b.currentSlide : b.currentSlide, f = Math.ceil( e + b.options.slidesToShow ), b.options.fade === ! 0 && ( e > 0 && e--, f <= b.slideCount && f++ ) ), c = b.$slider.find( '.slick-slide' ).slice( e, f ), g( c ), b.slideCount <= b.options.slidesToShow ? ( d = b.$slider.find( '.slick-slide' ), g( d ) ) : b.currentSlide >= b.slideCount - b.options.slidesToShow ? ( d = b.$slider.find( '.slick-cloned' ).slice( 0, b.options.slidesToShow ), g( d ) ) : 0 === b.currentSlide && ( d = b.$slider.find( '.slick-cloned' ).slice( -1 * b.options.slidesToShow ), g( d ) );
	}, b.prototype.loadSlider = function() {
		var a = this;
		a.setPosition(), a.$slideTrack.css( { opacity: 1 } ), a.$slider.removeClass( 'slick-loading' ), a.initUI(), 'progressive' === a.options.lazyLoad && a.progressiveLazyLoad();
	}, b.prototype.next = b.prototype.slickNext = function() {
		var a = this;
		a.changeSlide( { data: { message: 'next' } } );
	}, b.prototype.orientationChange = function() {
		var a = this;
		a.checkResponsive(), a.setPosition();
	}, b.prototype.pause = b.prototype.slickPause = function() {
		var a = this;
		a.autoPlayClear(), a.paused = ! 0;
	}, b.prototype.play = b.prototype.slickPlay = function() {
		var a = this;
		a.autoPlay(), a.options.autoplay = ! 0, a.paused = ! 1, a.focussed = ! 1, a.interrupted = ! 1;
	}, b.prototype.postSlide = function( a ) {
		var b = this;
		b.unslicked || ( b.$slider.trigger( 'afterChange', [ b, a ] ), b.animating = ! 1, b.setPosition(), b.swipeLeft = null, b.options.autoplay && b.autoPlay(), b.options.accessibility === ! 0 && b.initADA() );
	}, b.prototype.prev = b.prototype.slickPrev = function() {
		var a = this;
		a.changeSlide( { data: { message: 'previous' } } );
	}, b.prototype.preventDefault = function( a ) {
		a.preventDefault();
	}, b.prototype.progressiveLazyLoad = function( b ) {
		b = b || 1;
		var e, f, g, c = this, d = a( 'img[data-lazy]', c.$slider );
		d.length ? ( e = d.first(), f = e.attr( 'data-lazy' ), g = document.createElement( 'img' ), g.onload = function() {
			e.attr( 'src', f ).removeAttr( 'data-lazy' ).removeClass( 'slick-loading' ), c.options.adaptiveHeight === ! 0 && c.setPosition(), c.$slider.trigger( 'lazyLoaded', [ c, e, f ] ), c.progressiveLazyLoad();
		}, g.onerror = function() {
			3 > b ? setTimeout( function() {
				c.progressiveLazyLoad( b + 1 );
			}, 500 ) : ( e.removeAttr( 'data-lazy' ).removeClass( 'slick-loading' ).addClass( 'slick-lazyload-error' ), c.$slider.trigger( 'lazyLoadError', [ c, e, f ] ), c.progressiveLazyLoad() );
		}, g.src = f ) : c.$slider.trigger( 'allImagesLoaded', [ c ] );
	}, b.prototype.refresh = function( b ) {
		var d, e, c = this;
		e = c.slideCount - c.options.slidesToShow, ! c.options.infinite && c.currentSlide > e && ( c.currentSlide = e ), c.slideCount <= c.options.slidesToShow && ( c.currentSlide = 0 ), d = c.currentSlide, c.destroy( ! 0 ), a.extend( c, c.initials, { currentSlide: d } ), c.init(), b || c.changeSlide( {
			data: {
				message: 'index',
				index: d
			}
		}, ! 1 );
	}, b.prototype.registerBreakpoints = function() {
		var c, d, e, b = this, f = b.options.responsive || null;
		if ( 'array' === a.type( f ) && f.length ) {
			b.respondTo = b.options.respondTo || 'window';
			for ( c in f ) if ( e = b.breakpoints.length - 1, d = f[ c ].breakpoint, f.hasOwnProperty( c ) ) {
				for ( ; e >= 0; ) b.breakpoints[ e ] && b.breakpoints[ e ] === d && b.breakpoints.splice( e, 1 ), e--;
				b.breakpoints.push( d ), b.breakpointSettings[ d ] = f[ c ].settings;
			}
			b.breakpoints.sort( function( a, c ) {
				return b.options.mobileFirst ? a - c : c - a;
			} );
		}
	}, b.prototype.reinit = function() {
		var b = this;
		b.$slides = b.$slideTrack.children( b.options.slide ).addClass( 'slick-slide' ), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && ( b.currentSlide = b.currentSlide - b.options.slidesToScroll ), b.slideCount <= b.options.slidesToShow && ( b.currentSlide = 0 ), b.registerBreakpoints(), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.cleanUpSlideEvents(), b.initSlideEvents(), b.checkResponsive( ! 1, ! 0 ), b.options.focusOnSelect === ! 0 && a( b.$slideTrack ).children().on( 'click.slick', b.selectHandler ), b.setSlideClasses( 'number' == typeof b.currentSlide ? b.currentSlide : 0 ), b.setPosition(), b.focusHandler(), b.paused = ! b.options.autoplay, b.autoPlay(), b.$slider.trigger( 'reInit', [ b ] );
	}, b.prototype.resize = function() {
		var b = this;
		a( window ).width() !== b.windowWidth && ( clearTimeout( b.windowDelay ), b.windowDelay = window.setTimeout( function() {
			b.windowWidth = a( window ).width(), b.checkResponsive(), b.unslicked || b.setPosition();
		}, 50 ) );
	}, b.prototype.removeSlide = b.prototype.slickRemove = function( a, b, c ) {
		var d = this;
		return 'boolean' == typeof a ? ( b = a, a = b === ! 0 ? 0 : d.slideCount - 1 ) : a = b === ! 0 ? --a : a, d.slideCount < 1 || 0 > a || a > d.slideCount - 1 ? ! 1 : ( d.unload(), c === ! 0 ? d.$slideTrack.children().remove() : d.$slideTrack.children( this.options.slide ).eq( a ).remove(), d.$slides = d.$slideTrack.children( this.options.slide ), d.$slideTrack.children( this.options.slide ).detach(), d.$slideTrack.append( d.$slides ), d.$slidesCache = d.$slides, void d.reinit() );
	}, b.prototype.setCSS = function( a ) {
		var d, e, b = this, c = {};
		b.options.rtl === ! 0 && ( a = -a ), d = 'left' == b.positionProp ? Math.ceil( a ) + 'px' : '0px', e = 'top' == b.positionProp ? Math.ceil( a ) + 'px' : '0px', c[ b.positionProp ] = a, b.transformsEnabled === ! 1 ? b.$slideTrack.css( c ) : ( c = {}, b.cssTransitions === ! 1 ? ( c[ b.animType ] = 'translate(' + d + ', ' + e + ')', b.$slideTrack.css( c ) ) : ( c[ b.animType ] = 'translate3d(' + d + ', ' + e + ', 0px)', b.$slideTrack.css( c ) ) );
	}, b.prototype.setDimensions = function() {
		var a = this;
		a.options.vertical === ! 1 ? a.options.centerMode === ! 0 && a.$list.css( { padding: '0px ' + a.options.centerPadding } ) : ( a.$list.height( a.$slides.first().outerHeight( ! 0 ) * a.options.slidesToShow ), a.options.centerMode === ! 0 && a.$list.css( { padding: a.options.centerPadding + ' 0px' } ) ), a.listWidth = a.$list.width(), a.listHeight = a.$list.height(), a.options.vertical === ! 1 && a.options.variableWidth === ! 1 ? ( a.slideWidth = Math.ceil( a.listWidth / a.options.slidesToShow ), a.$slideTrack.width( Math.ceil( a.slideWidth * a.$slideTrack.children( '.slick-slide' ).length ) ) ) : a.options.variableWidth === ! 0 ? a.$slideTrack.width( 5e3 * a.slideCount ) : ( a.slideWidth = Math.ceil( a.listWidth ), a.$slideTrack.height( Math.ceil( a.$slides.first().outerHeight( ! 0 ) * a.$slideTrack.children( '.slick-slide' ).length ) ) );
		var b = a.$slides.first().outerWidth( ! 0 ) - a.$slides.first().width();
		a.options.variableWidth === ! 1 && a.$slideTrack.children( '.slick-slide' ).width( a.slideWidth - b );
	}, b.prototype.setFade = function() {
		var c, b = this;
		b.$slides.each( function( d, e ) {
			c = b.slideWidth * d * -1, b.options.rtl === ! 0 ? a( e ).css( {
				position: 'relative',
				right: c,
				top: 0,
				zIndex: b.options.zIndex - 2,
				opacity: 0
			} ) : a( e ).css( { position: 'relative', left: c, top: 0, zIndex: b.options.zIndex - 2, opacity: 0 } );
		} ), b.$slides.eq( b.currentSlide ).css( { zIndex: b.options.zIndex - 1, opacity: 1 } );
	}, b.prototype.setHeight = function() {
		var a = this;
		if ( 1 === a.options.slidesToShow && a.options.adaptiveHeight === ! 0 && a.options.vertical === ! 1 ) {
			var b = a.$slides.eq( a.currentSlide ).outerHeight( ! 0 );
			a.$list.css( 'height', b );
		}
	}, b.prototype.setOption = b.prototype.slickSetOption = function() {
		var c, d, e, f, h, b = this, g = ! 1;
		if ( 'object' === a.type( arguments[ 0 ] ) ? ( e = arguments[ 0 ], g = arguments[ 1 ], h = 'multiple' ) : 'string' === a.type( arguments[ 0 ] ) && ( e = arguments[ 0 ], f = arguments[ 1 ], g = arguments[ 2 ], 'responsive' === arguments[ 0 ] && 'array' === a.type( arguments[ 1 ] ) ? h = 'responsive' : 'undefined' != typeof arguments[ 1 ] && ( h = 'single' ) ), 'single' === h ) b.options[ e ] = f; else if ( 'multiple' === h ) a.each( e, function( a, c ) {
			b.options[ a ] = c;
		} ); else if ( 'responsive' === h ) for ( d in f ) if ( 'array' !== a.type( b.options.responsive ) ) b.options.responsive = [ f[ d ] ]; else {
			for ( c = b.options.responsive.length - 1; c >= 0; ) b.options.responsive[ c ].breakpoint === f[ d ].breakpoint && b.options.responsive.splice( c, 1 ), c--;
			b.options.responsive.push( f[ d ] );
		}
		g && ( b.unload(), b.reinit() );
	}, b.prototype.setPosition = function() {
		var a = this;
		a.setDimensions(), a.setHeight(), a.options.fade === ! 1 ? a.setCSS( a.getLeft( a.currentSlide ) ) : a.setFade(), a.$slider.trigger( 'setPosition', [ a ] );
	}, b.prototype.setProps = function() {
		var a = this, b = document.body.style;
		a.positionProp = a.options.vertical === ! 0 ? 'top' : 'left', 'top' === a.positionProp ? a.$slider.addClass( 'slick-vertical' ) : a.$slider.removeClass( 'slick-vertical' ), ( void 0 !== b.WebkitTransition || void 0 !== b.MozTransition || void 0 !== b.msTransition ) && a.options.useCSS === ! 0 && ( a.cssTransitions = ! 0 ), a.options.fade && ( 'number' == typeof a.options.zIndex ? a.options.zIndex < 3 && ( a.options.zIndex = 3 ) : a.options.zIndex = a.defaults.zIndex ), void 0 !== b.OTransform && ( a.animType = 'OTransform', a.transformType = '-o-transform', a.transitionType = 'OTransition', void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && ( a.animType = ! 1 ) ), void 0 !== b.MozTransform && ( a.animType = 'MozTransform', a.transformType = '-moz-transform', a.transitionType = 'MozTransition', void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && ( a.animType = ! 1 ) ), void 0 !== b.webkitTransform && ( a.animType = 'webkitTransform', a.transformType = '-webkit-transform', a.transitionType = 'webkitTransition', void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && ( a.animType = ! 1 ) ), void 0 !== b.msTransform && ( a.animType = 'msTransform', a.transformType = '-ms-transform', a.transitionType = 'msTransition', void 0 === b.msTransform && ( a.animType = ! 1 ) ), void 0 !== b.transform && a.animType !== ! 1 && ( a.animType = 'transform', a.transformType = 'transform', a.transitionType = 'transition' ), a.transformsEnabled = a.options.useTransform && null !== a.animType && a.animType !== ! 1;
	}, b.prototype.setSlideClasses = function( a ) {
		var c, d, e, f, b = this;
		d = b.$slider.find( '.slick-slide' ).removeClass( 'slick-active slick-center slick-current' ).attr( 'aria-hidden', 'true' ), b.$slides.eq( a ).addClass( 'slick-current' ), b.options.centerMode === ! 0 ? ( c = Math.floor( b.options.slidesToShow / 2 ), b.options.infinite === ! 0 && ( a >= c && a <= b.slideCount - 1 - c ? b.$slides.slice( a - c, a + c + 1 ).addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) : ( e = b.options.slidesToShow + a,
			d.slice( e - c + 1, e + c + 2 ).addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) ), 0 === a ? d.eq( d.length - 1 - b.options.slidesToShow ).addClass( 'slick-center' ) : a === b.slideCount - 1 && d.eq( b.options.slidesToShow ).addClass( 'slick-center' ) ), b.$slides.eq( a ).addClass( 'slick-center' ) ) : a >= 0 && a <= b.slideCount - b.options.slidesToShow ? b.$slides.slice( a, a + b.options.slidesToShow ).addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) : d.length <= b.options.slidesToShow ? d.addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) : ( f = b.slideCount % b.options.slidesToShow, e = b.options.infinite === ! 0 ? b.options.slidesToShow + a : a, b.options.slidesToShow == b.options.slidesToScroll && b.slideCount - a < b.options.slidesToShow ? d.slice( e - ( b.options.slidesToShow - f ), e + f ).addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) : d.slice( e, e + b.options.slidesToShow ).addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) ), 'ondemand' === b.options.lazyLoad && b.lazyLoad();
	}, b.prototype.setupInfinite = function() {
		var c, d, e, b = this;
		if ( b.options.fade === ! 0 && ( b.options.centerMode = ! 1 ), b.options.infinite === ! 0 && b.options.fade === ! 1 && ( d = null, b.slideCount > b.options.slidesToShow ) ) {
			for ( e = b.options.centerMode === ! 0 ? b.options.slidesToShow + 1 : b.options.slidesToShow, c = b.slideCount; c > b.slideCount - e; c -= 1 ) d = c - 1, a( b.$slides[ d ] ).clone( ! 0 ).attr( 'id', '' ).attr( 'data-slick-index', d - b.slideCount ).prependTo( b.$slideTrack ).addClass( 'slick-cloned' );
			for ( c = 0; e > c; c += 1 ) d = c, a( b.$slides[ d ] ).clone( ! 0 ).attr( 'id', '' ).attr( 'data-slick-index', d + b.slideCount ).appendTo( b.$slideTrack ).addClass( 'slick-cloned' );
			b.$slideTrack.find( '.slick-cloned' ).find( '[id]' ).each( function() {
				a( this ).attr( 'id', '' );
			} );
		}
	}, b.prototype.interrupt = function( a ) {
		var b = this;
		a || b.autoPlay(), b.interrupted = a;
	}, b.prototype.selectHandler = function( b ) {
		var c = this, d = a( b.target ).is( '.slick-slide' ) ? a( b.target ) : a( b.target ).parents( '.slick-slide' ),
			e = parseInt( d.attr( 'data-slick-index' ) );
		return e || ( e = 0 ), c.slideCount <= c.options.slidesToShow ? ( c.setSlideClasses( e ), void c.asNavFor( e ) ) : void c.slideHandler( e );
	}, b.prototype.slideHandler = function( a, b, c ) {
		var d, e, f, g, j, h = null, i = this;
		return b = b || ! 1, i.animating === ! 0 && i.options.waitForAnimate === ! 0 || i.options.fade === ! 0 && i.currentSlide === a || i.slideCount <= i.options.slidesToShow ? void 0 : ( b === ! 1 && i.asNavFor( a ), d = a, h = i.getLeft( d ), g = i.getLeft( i.currentSlide ), i.currentLeft = null === i.swipeLeft ? g : i.swipeLeft, i.options.infinite === ! 1 && i.options.centerMode === ! 1 && ( 0 > a || a > i.getDotCount() * i.options.slidesToScroll ) ? void ( i.options.fade === ! 1 && ( d = i.currentSlide, c !== ! 0 ? i.animateSlide( g, function() {
			i.postSlide( d );
		} ) : i.postSlide( d ) ) ) : i.options.infinite === ! 1 && i.options.centerMode === ! 0 && ( 0 > a || a > i.slideCount - i.options.slidesToScroll ) ? void ( i.options.fade === ! 1 && ( d = i.currentSlide, c !== ! 0 ? i.animateSlide( g, function() {
			i.postSlide( d );
		} ) : i.postSlide( d ) ) ) : ( i.options.autoplay && clearInterval( i.autoPlayTimer ), e = 0 > d ? i.slideCount % i.options.slidesToScroll !== 0 ? i.slideCount - i.slideCount % i.options.slidesToScroll : i.slideCount + d : d >= i.slideCount ? i.slideCount % i.options.slidesToScroll !== 0 ? 0 : d - i.slideCount : d, i.animating = ! 0, i.$slider.trigger( 'beforeChange', [ i, i.currentSlide, e ] ), f = i.currentSlide, i.currentSlide = e, i.setSlideClasses( i.currentSlide ), i.options.asNavFor && ( j = i.getNavTarget(), j = j.slick( 'getSlick' ), j.slideCount <= j.options.slidesToShow && j.setSlideClasses( i.currentSlide ) ), i.updateDots(), i.updateArrows(), i.options.fade === ! 0 ? ( c !== ! 0 ? ( i.fadeSlideOut( f ), i.fadeSlide( e, function() {
			i.postSlide( e );
		} ) ) : i.postSlide( e ), void i.animateHeight() ) : void ( c !== ! 0 ? i.animateSlide( h, function() {
			i.postSlide( e );
		} ) : i.postSlide( e ) ) ) );
	}, b.prototype.startLoad = function() {
		var a = this;
		a.options.arrows === ! 0 && a.slideCount > a.options.slidesToShow && ( a.$prevArrow.hide(), a.$nextArrow.hide() ), a.options.dots === ! 0 && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass( 'slick-loading' );
	}, b.prototype.swipeDirection = function() {
		var a, b, c, d, e = this;
		return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2( b, a ), d = Math.round( 180 * c / Math.PI ), 0 > d && ( d = 360 - Math.abs( d ) ), 45 >= d && d >= 0 ? e.options.rtl === ! 1 ? 'left' : 'right' : 360 >= d && d >= 315 ? e.options.rtl === ! 1 ? 'left' : 'right' : d >= 135 && 225 >= d ? e.options.rtl === ! 1 ? 'right' : 'left' : e.options.verticalSwiping === ! 0 ? d >= 35 && 135 >= d ? 'down' : 'up' : 'vertical';
	}, b.prototype.swipeEnd = function( a ) {
		var c, d, b = this;
		if ( b.dragging = ! 1, b.interrupted = ! 1, b.shouldClick = b.touchObject.swipeLength > 10 ? ! 1 : ! 0, void 0 === b.touchObject.curX ) return ! 1;
		if ( b.touchObject.edgeHit === ! 0 && b.$slider.trigger( 'edge', [ b, b.swipeDirection() ] ), b.touchObject.swipeLength >= b.touchObject.minSwipe ) {
			switch ( d = b.swipeDirection() ) {
				case'left':
				case'down':
					c = b.options.swipeToSlide ? b.checkNavigable( b.currentSlide + b.getSlideCount() ) : b.currentSlide + b.getSlideCount(), b.currentDirection = 0;
					break;
				case'right':
				case'up':
					c = b.options.swipeToSlide ? b.checkNavigable( b.currentSlide - b.getSlideCount() ) : b.currentSlide - b.getSlideCount(), b.currentDirection = 1;
			}
			'vertical' != d && ( b.slideHandler( c ), b.touchObject = {}, b.$slider.trigger( 'swipe', [ b, d ] ) );
		} else b.touchObject.startX !== b.touchObject.curX && ( b.slideHandler( b.currentSlide ), b.touchObject = {} );
	}, b.prototype.swipeHandler = function( a ) {
		var b = this;
		if ( ! ( b.options.swipe === ! 1 || 'ontouchend' in document && b.options.swipe === ! 1 || b.options.draggable === ! 1 && -1 !== a.type.indexOf( 'mouse' ) ) ) switch ( b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, b.options.verticalSwiping === ! 0 && ( b.touchObject.minSwipe = b.listHeight / b.options.touchThreshold ), a.data.action ) {
			case'start':
				b.swipeStart( a );
				break;
			case'move':
				b.swipeMove( a );
				break;
			case'end':
				b.swipeEnd( a );
		}
	}, b.prototype.swipeMove = function( a ) {
		var d, e, f, g, h, b = this;
		return h = void 0 !== a.originalEvent ? a.originalEvent.touches : null, ! b.dragging || h && 1 !== h.length ? ! 1 : ( d = b.getLeft( b.currentSlide ), b.touchObject.curX = void 0 !== h ? h[ 0 ].pageX : a.clientX, b.touchObject.curY = void 0 !== h ? h[ 0 ].pageY : a.clientY, b.touchObject.swipeLength = Math.round( Math.sqrt( Math.pow( b.touchObject.curX - b.touchObject.startX, 2 ) ) ), b.options.verticalSwiping === ! 0 && ( b.touchObject.swipeLength = Math.round( Math.sqrt( Math.pow( b.touchObject.curY - b.touchObject.startY, 2 ) ) ) ), e = b.swipeDirection(), 'vertical' !== e ? ( void 0 !== a.originalEvent && b.touchObject.swipeLength > 4 && a.preventDefault(), g = ( b.options.rtl === ! 1 ? 1 : -1 ) * ( b.touchObject.curX > b.touchObject.startX ? 1 : -1 ), b.options.verticalSwiping === ! 0 && ( g = b.touchObject.curY > b.touchObject.startY ? 1 : -1 ), f = b.touchObject.swipeLength, b.touchObject.edgeHit = ! 1, b.options.infinite === ! 1 && ( 0 === b.currentSlide && 'right' === e || b.currentSlide >= b.getDotCount() && 'left' === e ) && ( f = b.touchObject.swipeLength * b.options.edgeFriction, b.touchObject.edgeHit = ! 0 ), b.options.vertical === ! 1 ? b.swipeLeft = d + f * g : b.swipeLeft = d + f * ( b.$list.height() / b.listWidth ) * g, b.options.verticalSwiping === ! 0 && ( b.swipeLeft = d + f * g ), b.options.fade === ! 0 || b.options.touchMove === ! 1 ? ! 1 : b.animating === ! 0 ? ( b.swipeLeft = null, ! 1 ) : void b.setCSS( b.swipeLeft ) ) : void 0 );
	}, b.prototype.swipeStart = function( a ) {
		var c, b = this;
		return b.interrupted = ! 0, 1 !== b.touchObject.fingerCount || b.slideCount <= b.options.slidesToShow ? ( b.touchObject = {}, ! 1 ) : ( void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && ( c = a.originalEvent.touches[ 0 ] ), b.touchObject.startX = b.touchObject.curX = void 0 !== c ? c.pageX : a.clientX, b.touchObject.startY = b.touchObject.curY = void 0 !== c ? c.pageY : a.clientY, void ( b.dragging = ! 0 ) );
	}, b.prototype.unfilterSlides = b.prototype.slickUnfilter = function() {
		var a = this;
		null !== a.$slidesCache && ( a.unload(), a.$slideTrack.children( this.options.slide ).detach(), a.$slidesCache.appendTo( a.$slideTrack ), a.reinit() );
	}, b.prototype.unload = function() {
		var b = this;
		a( '.slick-cloned', b.$slider ).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && b.htmlExpr.test( b.options.prevArrow ) && b.$prevArrow.remove(), b.$nextArrow && b.htmlExpr.test( b.options.nextArrow ) && b.$nextArrow.remove(), b.$slides.removeClass( 'slick-slide slick-active slick-visible slick-current' ).attr( 'aria-hidden', 'true' ).css( 'width', '' );
	}, b.prototype.unslick = function( a ) {
		var b = this;
		b.$slider.trigger( 'unslick', [ b, a ] ), b.destroy();
	}, b.prototype.updateArrows = function() {
		var b, a = this;
		b = Math.floor( a.options.slidesToShow / 2 ), a.options.arrows === ! 0 && a.slideCount > a.options.slidesToShow && ! a.options.infinite && ( a.$prevArrow.removeClass( 'slick-disabled' ).attr( 'aria-disabled', 'false' ), a.$nextArrow.removeClass( 'slick-disabled' ).attr( 'aria-disabled', 'false' ), 0 === a.currentSlide ? ( a.$prevArrow.addClass( 'slick-disabled' ).attr( 'aria-disabled', 'true' ), a.$nextArrow.removeClass( 'slick-disabled' ).attr( 'aria-disabled', 'false' ) ) : a.currentSlide >= a.slideCount - a.options.slidesToShow && a.options.centerMode === ! 1 ? ( a.$nextArrow.addClass( 'slick-disabled' ).attr( 'aria-disabled', 'true' ), a.$prevArrow.removeClass( 'slick-disabled' ).attr( 'aria-disabled', 'false' ) ) : a.currentSlide >= a.slideCount - 1 && a.options.centerMode === ! 0 && ( a.$nextArrow.addClass( 'slick-disabled' ).attr( 'aria-disabled', 'true' ), a.$prevArrow.removeClass( 'slick-disabled' ).attr( 'aria-disabled', 'false' ) ) );
	}, b.prototype.updateDots = function() {
		var a = this;
		null !== a.$dots && ( a.$dots.find( 'li' ).removeClass( 'slick-active' ).attr( 'aria-hidden', 'true' ), a.$dots.find( 'li' ).eq( Math.floor( a.currentSlide / a.options.slidesToScroll ) ).addClass( 'slick-active' ).attr( 'aria-hidden', 'false' ) );
	}, b.prototype.visibility = function() {
		var a = this;
		a.options.autoplay && ( document[ a.hidden ] ? a.interrupted = ! 0 : a.interrupted = ! 1 );
	}, a.fn.slick = function() {
		var f, g, a = this, c = arguments[ 0 ], d = Array.prototype.slice.call( arguments, 1 ), e = a.length;
		for ( f = 0; e > f; f++ ) if ( 'object' == typeof c || 'undefined' == typeof c ? a[ f ].slick = new b( a[ f ], c ) : g = a[ f ].slick[ c ].apply( a[ f ].slick, d ), 'undefined' != typeof g ) return g;
		return a;
	};
} );
