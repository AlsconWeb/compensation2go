<?php 

header('Content-Type: application/json;charset=utf-8');

require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php'); ?>

   <?php 

	$file =  file_get_contents(get_template_directory_uri().'/inc/autojson.php');
	$file = fopen(get_template_directory()."/inc/autojson.txt","r"); 
	$file_text = fgets($file);
	
	$json = json_decode($file_text,true);
	if( isset($_GET['departure']) ) $search = $_GET['departure'];
	if( isset($_GET['arrival']) )  $search = $_GET['arrival'];
    
	$array_city = array();
	foreach( $json['airports'] as $airdata ):
		if( $airdata['classification'] != 5 ):
        	if( preg_match("/^".$search."/i", $airdata['city']) || preg_match("/^".$search."/i", $airdata['name']) || preg_match("/^".$search."/i", $airdata['iata']) ):
        	 	$array_city['airports'][] = $airdata;
        	endif; 
        endif; 
	endforeach;
	function cmp($a, $b){
	    if ($a["classification"] == $b["classification"]){
      		return 0;
    	}
    	return ($a["classification"] < $b["classification"]) ? -1 : 1; 
    }
	usort($array_city['airports'],'cmp');

    echo json_encode($array_city);
	 ?>