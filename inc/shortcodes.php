<?php 


function conditions_of_compensations(){ 
	$comp = '';
	$comp ='<section class="compensation-section"><div class="white-box">';
		    if( $title1 = get_field('block_title1','option') ): 
				$comp .='<header class="head"><span class="title">'.$title1.'</span></header>';
			endif;
			if( have_rows('compensations','option') ):
				$comp .='<ul class="compensation-list">';
					while ( have_rows('compensations','option') ) : the_row();
						$comp .='<li><div class="box">';
							if( $image = get_sub_field('image','option') ):
								$comp .='<div class="picture-holder">
									<img class="img-responsive" src="'.$image['url'].'" alt="';
										if( $image['alt'] ) { $comp .= $image['alt']; } else { $comp .= 'image description';}
								$comp .='" width="'.get_sub_field('width','option').'" height="'.get_sub_field('height','option').'"></div>';
							endif;
							if( $condition = get_sub_field('condition','option') ):
								$comp .='<span class="text">'.$condition.'</span>';
							endif;
							$comp .='</div></li>';
					endwhile;
				$comp .='</ul>';
			endif; 
		$comp .='</div></section>';
	return $comp;	
}
add_shortcode( 'compensations', 'conditions_of_compensations' );


function compensations_amount(){ 
	$am = "";
	$am ='<section class="declaration-section"><div class="white-box">';
			if( $title2 = get_field('block_title2','option') ):
				$am .='<header class="head"><span class="title">'.$title2.'</span></header>';
			endif;
			if( have_rows('amounts','option') ):
				$am .='<ul class="declaration-list">';
					while ( have_rows('amounts','option') ) : the_row();
						$am .= '<li>';
						    if( $distance = get_sub_field('distance','option')):
								$am .='<div class="distance-holder"><span class="holder">'.$distance.'</span></div>';
							endif;
							$am .= '<div class="description-holder">';
							$image = get_sub_field('image','option');
							$retina_image = get_sub_field('retina_image','option');
							    if( $image || $retina_image ):
									$am .='<div class="picture-holder">';
										$am .='<picture><source srcset="'.$image['sizes']['thumbnail_96x98'].', '.$retina_image['sizes']['thumbnail_192x166'].' 2x"/><img class="img-responsive" src="'.$image['sizes']['thumbnail_96x98'].'" alt="image description"/></picture>';
									$am .='</div>';
								endif;
								if( $flight = get_sub_field('flight','option')):	
									$am .='<span class="text">'.$flight.'</span>';
								endif;	
							$am .='</div>';
							if( $amount = get_sub_field('amount','option')):	
								$am .='<span class="price">'.$amount.'</span>';
							endif;	
						$am .='</li>';
					endwhile;
				$am .='</ul>';
			endif;
		$am .='</div></section>';
	return $am;	
}						
add_shortcode( 'compensations_amount', 'compensations_amount' );

function quote_wrap( $atts, $content = null) {
	return '<div class="note-box"><div class="note">'.$content.'</div></div>';
}
add_shortcode( 'quote_wrapper', 'quote_wrap' );

function wrap_with_image( $atts, $content = null) { 
	return '<div class="warning-box">
		<div class="picture-holder">
			<picture>
				<source srcset="'.get_template_directory_uri().'/images/warning.png, '.get_template_directory_uri().'/images/warning@2x.png 2x"/><img class="img-responsive" src="'.get_template_directory_uri().'/images/warning.png" alt="image description" width="20" height="25"/>
			</picture>
		</div>
		<div class="text-holder">'.$content.'</div></div>';
 }												
add_shortcode( 'wrapper_note', 'wrap_with_image' );

function pricing_table(){
if( have_rows('variations','option') ): 
	$output = "";
	$output .= "<section class=\"info-section\">";
	    if( $title3 = get_field('block_title3','option') ):
			$output .= '<div class="container-fluid"><div class="text-holder"><h2>'.$title3.'</h2></div></div>';
		endif;
		$output .= '<div class="container-fluid">';
		    $section_title = get_field('section_title','option'); 
		    $description = get_field('description','option');
		    if( $section_title || $description ): 
				$output .= '<div class="row"><div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">';
				    if( $section_title ):
						$output .= '<h2>'.$section_title.'</h2>';
					endif; 	
					$output .= $description;
				$output .= '</div></div>';
			endif;
			$output .= '<div class="row">';
			    while ( have_rows('variations','option') ) : the_row();
					$output .= '<div class="col-md-3 ';
						if($mark = get_sub_field('mark','option')):
							$output .= 'large-col';
					    endif;
						$output .='"><div class="info-box">';
							if( $title = get_sub_field('title','option') ):
								$output .='<span class="title">'.$title.'</span>';
							endif;
							if( get_sub_field('mark','option')):
							$output .='<span class="title"><img class="img-responsive" src="'.get_template_directory_uri().'/images/logo-white.svg" alt="'.bloginfo('description').'" width="207" height="49"></span>';
							endif;
						    if( have_rows('items','option') ): 
								$output .='<ul class="list">';
									while ( have_rows('items','option') ) : the_row();
										$output .='<li>';
										    if( $item_title = get_sub_field('item_title','option')):
												$output .='<span class="title">'.$item_title.'</span>';
											endif;
											if( $description = get_sub_field('description','option')):
												$output .='<span class="item">'.$description.'</span>';
											endif;
											if( $note = get_sub_field('note','option')):	
												$output .='<span class="note">'. $note.'</span>';
											endif;
										$output .='</li>';
									endwhile; 
								$output .='</ul>';
							endif;
						$output .='</div></div>';
				endwhile;
			$output .='</div></div>';	
			if( $note = get_field('note','option')):
				$output .='<div class="container-fluid"><span class="note">'.$note.'</span></div>';
			endif;
		$output .='</section>';
	endif; 
return $output;
}												
add_shortcode( 'pricing_table', 'pricing_table' );