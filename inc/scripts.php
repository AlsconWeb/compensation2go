<?php
// Theme css & js

function base_scripts_styles() {
	$in_footer = true;
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	wp_deregister_script( 'comment-reply' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply', get_template_directory_uri() . '/js/comment-reply.js', '', '', $in_footer );
	}
	// Loads our main stylesheet.
	wp_enqueue_style( 'fonts-theme', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800,900', [] );
	wp_enqueue_style( 'base-theme', get_template_directory_uri() . '/css/bootstrap.css', [] );
	wp_enqueue_style( 'base-style', get_stylesheet_uri(), [] );
	wp_enqueue_style( 'form-style', get_template_directory_uri() . '/css/subscription-form.css', [] );

	// Loads JavaScript file with functionality specific.
	wp_enqueue_script( 'min-script', get_template_directory_uri() . '/js/jquery-1.12.1.min.js', [ 'jquery' ], '', $in_footer );
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', [ 'jquery' ], '', $in_footer );
	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/js/jquery.main.js', [ 'jquery' ], '', $in_footer );
	// Implementation stylesheet.
	wp_enqueue_style( 'base-theme', get_template_directory_uri() . '/theme.css', [] );

}

add_action( 'wp_enqueue_scripts', 'base_scripts_styles' );