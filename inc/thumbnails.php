<?php
// Theme thumbnails

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
add_image_size( 'thumbnail_400x9999', 400, 9999, true );
add_image_size( 'thumbnail_375x160', 375, 160, true );
add_image_size( 'thumbnail_750x320', 750, 320, true );
add_image_size( 'thumbnail_2513x1345', 2513, 1345, true );
add_image_size( 'thumbnail_1256x672', 1256, 672, true );
add_image_size( 'thumbnail_1251x887', 1251, 887, true );
add_image_size( 'thumbnail_2502x1772', 2502, 1772, true );
add_image_size( 'thumbnail_1200x300', 1200, 300, true );
add_image_size( 'thumbnail_2400x600', 2400, 600, true );
add_image_size( 'thumbnail_96x98', 96, 98, true );
add_image_size( 'thumbnail_192x166', 192, 166, true );