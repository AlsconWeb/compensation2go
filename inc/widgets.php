<?php
/**
 * Created 21.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Widgets
 */

use IWP\Widgets\CustomWidgetText;
use IWP\Widgets\OurServicesWidgetText;
use IWP\Widgets\WidgetRecentPostsFromCategory;

/**
 * Register Custom Widget to Theme compensation2go.
 */
function iwp_register_custom_widget() {
	register_widget( CustomWidgetText::class );
	register_widget( WidgetRecentPostsFromCategory::class );
	register_widget( OurServicesWidgetText::class );
}

add_action( 'widgets_init', 'iwp_register_custom_widget' );

