<?php
// Theme menus

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'compensation2go' ),
	'footer'  => __( 'Footer Navigation', 'compensation2go' ),
) );