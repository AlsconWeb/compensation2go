<?php
// Custom Post Types

add_action('init', 'register_post_types');
function register_post_types(){
	$labels = array(
        'name' => __( 'Testimonials', 'compensation2go' ),
        'singular_name' => __( 'Testimonial', 'compensation2go' ),
        'menu_name' => __( 'Testimonials', 'compensation2go' ),
        'add_new' => __( 'Add Testimonial', 'compensation2go' ),
        'add_new_item' => __( 'Add New Testimonial', 'compensation2go' ),
        'new_item' => __( 'New Testimonial', 'compensation2go' ),
        'edit_item' => __( 'Edit Testimonial', 'compensation2go' ),
        'view_item' => __( 'View Testimonial', 'compensation2go' ),
        'all_items' => __( 'View Testimonials', 'compensation2go' ),
        'search_items' => __( 'Search Testimonials', 'compensation2go' ),
        'not_found' => __( 'No Testimonials Found', 'compensation2go' ),
        'not_found_in_trash' => __( 'No Testimonials found in Trash.', 'compensation2go' ),
	    );
	$args = array(
        'labels' => $labels,
        'description' => '',
        'public' => true,
        'menu_icon' => 'dashicons-building',
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array('slug' => 'testimonials', 'with_front' => false),
        'query_var' => true,
        'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' ), 
        'has_archive' => false,
    );
	register_post_type( 'testimonial', $args );
}

 