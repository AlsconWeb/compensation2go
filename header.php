<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="google-site-verification" content="Xda1X9Cwy7mKhafcQpj6MeWETVEww7pEimtkf8m7xLE"/>

	<meta property="fb:page_id" content="1103530726324278">
	<meta property="fb:pages" content="1103530726324278">


	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php wp_head(); ?>

	<link
			rel="apple-touch-icon"
			sizes="152x152"
			href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicons/apple-touch-icon.png' ); ?>">
	<link
			rel="icon"
			type="image/png"
			sizes="32x32"
			href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicons/favicon-32x32.png' ); ?>">
	<link
			rel="icon"
			type="image/png"
			sizes="16x16"
			href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicons/favicon-16x16.png' ); ?>">
	<link
			rel="manifest"
			href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicons/manifest.json' ); ?>">
	<link
			rel="mask-icon"
			href="<?php echo esc_url( get_stylesheet_directory_uri() . '/favicons/safari-pinned-tab.svg' ); ?>"
			color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
	<header id="header">
		<div class="container-fluid">
			<div class="logo">
				<a href="<?php bloginfo( 'url' ); ?>">
					<img
							class="img-responsive"
							src="<?php echo esc_url( get_template_directory_uri() . '/images/logo.svg' ); ?>"
							alt="<?php bloginfo( 'description' ); ?>"
							width="182"
							height="42">
				</a>
			</div>
			<div class="header-bar">
				<div class="contact-box hidden-xs">
					<?php
					$block_title = get_field( 'block_title', 'option' );
					if ( $block_title ) :
						?>
						<span class="text-frame">
							<?php echo esc_html( $block_title ); ?>
						</span>
					<?php
					endif;
					$consultant_name = get_field( 'consultant_name', 'option' );
					if ( $consultant_name ) :
						?>
						<span class="name">
							<?php echo esc_html( $consultant_name ); ?>
						</span>
					<?php
					endif;
					$phone = get_field( 'phone', 'option' );
					if ( $phone ) :
						?>
						<a href="#">
							<?php echo esc_html( $phone ); ?>
						</a>
					<?php endif; ?>
				</div>
				<?php
				$star_block_content = get_field( 'star_block_content', 'option' );
				if ( $star_block_content ) :
					?>
					<div class="valuation-box hidden-xs">
						<a target="_blank" href="https://www.ausgezeichnet.org/bewertungen-compensation2go.com-6FZAT6">
							<?php echo wp_kses_post( $star_block_content ); ?>
						</a>
					</div>
				<?php endif; ?>
				<?php $btn_text = get_field( 'header_button', 'option' ); ?>
				<?php $btn_url = get_field( 'header_button_url', 'option' ); ?>
				<?php if ( $btn_text && $btn_url ) : ?>
					<a class="btn btn-fixed" href="<?php echo esc_url( $btn_url ); ?>">
						<?php echo esc_html( $btn_text ); ?>
					</a>
				<?php endif; ?>
				<nav id="nav">
					<a class="nav-opener" href="#">
						<span></span>
					</a>
					<?php
					if ( has_nav_menu( 'primary' ) ) {
						wp_nav_menu(
							[
								'container'      => false,
								'theme_location' => 'primary',
								'menu_id'        => 'navigation',
								'menu_class'     => 'navigation',
								'items_wrap'     => '<div class="nav-drop"><div class="holder">%3$s</div></div>',
								'walker'         => new Custom_Walker_Nav_Menu,
							]
						);
					}
					?>
				</nav>
			</div>
		</div>
	</header>
	<main id="main" role="main">
