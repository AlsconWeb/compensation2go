<div class="container-fluid">
    <div class="row">
        <div class="col-sm-11"><span class="icon-arrow-left"></span>
            <?php the_title('<h1>','</h1>'); ?>
            <div class="text-holder">
                <?php if( $subtitle = get_field('subtitle')): ?>
                <span class="text">
						<?php echo $subtitle; ?>
					</span>
                <?php endif; ?>
                <div class="fb-like" data-href="http://facebook.com/compensation2go" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-7">
                    <?php include( get_template_directory() .'/blocks/form.php'); ?>
                    <?php include( get_template_directory_uri().'/inc/autocomplite.php'); ?>
                </div>
            </div>
            <?php if( have_rows('companies') ): ?>
            <div class="logos-holder">
                <?php if( $featured_title = get_field('featured_title')): ?>
                <span class="title"><?php echo $featured_title; ?></span>
                <?php endif; ?>
                <ul class="logos-list">
                    <?php while ( have_rows('companies') ) : the_row(); ?>
                    <?php if( $logo = get_sub_field('logo')): ?>
                    <li>
                        <a target="_blank" <?php if( $url=g et_sub_field( 'url')) echo 'href="'.$url. '"'; ?>><img src="<?php echo $logo['url']; ?>" alt="<?php if( $logo['alt'] ) echo $logo['alt']; else echo 'image description'; ?>" width="<?php the_sub_field('width'); ?>" height="<?php the_sub_field('height'); ?>"/></a>
                    </li>
                    <?php endif; ?>
                    <?php endwhile; ?>
                </ul>
            </div>
            <?php endif; ?>
            <!--	<?php if( have_rows('advantages') ): ?>
				<ul class="benefits-list">
					<?php while ( have_rows('advantages') ) : the_row(); ?>
						<?php if( $image = get_sub_field('image')): ?>	
							<li>
								<div class="picture-holder"><img class="img-responsive" src="<?php echo $image['url']; ?>" alt="<?php if( $image['alt'] ) echo $image['alt']; else echo 'image description'; ?>" width="<?php the_sub_field('width'); ?>" height="<?php the_sub_field('height'); ?>"/>
								</div>
								<?php if( $item = get_sub_field('item')): ?>
									<span class="text"><?php echo $item; ?></span>
								<?php endif; ?>	
							</li>
						<?php endif; ?>	
					<?php endwhile; ?>	
				</ul>
			<?php endif; ?>-->
        </div>
    </div>
</div>
