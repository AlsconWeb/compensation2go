<?php if( have_rows('blocks') ): ?>
	<section class="works-section">
		<div class="container-fluid">
			<?php if( $title_block = get_field('title_block')): ?>
				<h2><?php echo $title_block; ?></h2>
			<?php endif; ?>	
			<ol class="works-list">
				<?php while ( have_rows('blocks') ) : the_row(); ?>
					<li>
					    <?php if( $title = get_sub_field('title')): ?>
							<h3><?php echo $title; ?></h3>
						<?php endif; ?>	
						<?php the_sub_field('content'); ?><span class="arrow"><span></span></span>
					</li>
				<?php endwhile; ?>	
			</ol>
		</div>
	</section>
<?php endif; ?>		