<?php if( $posts = get_field('posts') ): ?>
	<section class="post-section">
	    <?php if( $posts_title = get_field('posts_title')): ?>
			<div class="container-fluid">
				<h2><?php echo $posts_title; ?></h2>
			</div>
		<?php endif; ?>
		<div class="slick-slider post-slider">
			<?php foreach( $posts as $post): ?>
				<div class="slide">
					<div class="post-box">
						<div class="picture-holder">
							<a href="<?php the_permalink(); ?>" tabindex="0">
							    <?php if( has_post_thumbnail() ): ?>
							    	<?php $id = get_post_thumbnail_id($post->ID); ?>
									<div class="bg-retina">
										<span data-srcset="<?php echo wp_get_attachment_image_url($id,'thumbnail_375x160'); ?>, <?php echo wp_get_attachment_image_url($id,'thumbnail_375x160'); ?> 2x"></span><span data-srcset="<?php echo wp_get_attachment_image_url($id,'thumbnail_375x160'); ?>" data-media="(min-width: 768px)"></span>
									</div>
								<?php endif; ?>
								<header class="head-post">
								    <?php the_title('<h3>','</h3>'); ?>
									<ul class="list">
										<li>
											<time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php the_time( 'Y/m/d' ) ?></time>
										</li>
										<li>
											<span class="author"><?php the_author(); ?></span>
										</li>
									</ul>
								</header>
							</a>	
						</div>
						<div class="text-box">
							<?php theme_the_excerpt(); ?><a href="<?php the_permalink(); ?>"><?php _e('Lesen Sie mehr','compensation2go'); ?></a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>	
		</div>
	</section>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>