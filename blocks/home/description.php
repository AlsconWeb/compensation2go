<?php if ( have_rows( 'all_items' ) ): ?>
	<section class="description-section">
		<div class="container-fluid">
			<div class="row">
				<?php $all_items = get_field( 'all_items', get_the_ID() ); ?>
				<?php $count = count( $all_items ); ?>
				<?php $i = 1;
				while ( have_rows( 'all_items' ) ) : the_row(); ?>
					<?php if ( $i == 1 ): ?>
						<div class="col-sm-6">
					<?php endif; ?>
					<div class="description-box">
						<?php if ( $image = get_sub_field( 'image' ) ): ?>
							<div class="picture-holder">
								<img class="img-responsive" src="<?php echo $image['url']; ?>"
									 alt="<?php if ( $image['alt'] ) {
									     echo $image['alt'];
								     } else {
									     echo 'image description';
								     } ?>" width="<?php the_sub_field( 'width' ); ?>"
									 height="<?php the_sub_field( 'height' ); ?>"/>
							</div>
						<?php endif; ?>
						<?php $title = get_sub_field( 'title' ); ?>
						<?php $content = get_sub_field( 'content' ); ?>
						<?php if ( $title || $content ): ?>
							<div class="text-box">
								<?php if ( $title ): ?>
									<h2><?php echo $title; ?></h2>
								<?php endif; ?>
								<?php echo $content; ?>
							</div>
						<?php endif; ?>
					</div>
					<?php if ( $i == round( $count / 2 ) ): ?>
						</div>
						<?php $i = 0; ?>
					<?php endif; ?>
					<?php $i ++; endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>