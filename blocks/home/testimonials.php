<?php
$pid_main = get_the_ID();
if ( get_field( 'number_of_posts', $pid_main ) ) {
	$number_of_posts = get_field( 'number_of_posts', $pid_main );
} else {
	$number_of_posts = - 1;
}

$query = new WP_Query(
	[
		'post_type'      => 'testimonial',
		'post_status'    => 'publish',
		'posts_per_page' => $number_of_posts,
	]
);

if ( $query->have_posts() ) :
	$pid = get_the_ID();
	?>
	<section class="reviews-section">
		<div class="container-fluid">
			<?php
			$t_title = get_field( 't_title', $pid );
			if ( $t_title ) :
				?>
				<h2><?php echo esc_html( $t_title ); ?></h2>
			<?php endif; ?>
			<?php
			$average_rating = get_field( 'average_rating', $pid );
			if ( $average_rating ) :
				?>
				<div class="valuation-box">
					<?php echo wp_kses_post( $average_rating ); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="slick-slider testimonials-slider">
			<?php
			while ( $query->have_posts() ) :
				$query->the_post();
				?>
				<div class="slide">
					<div class="text-blockquote">
						<span class="title"><?php the_title(); ?></span>
						<?php theme_the_excerpt(); ?>
						<?php $rating = get_field( 'rating', get_the_ID() ); ?>
						<ul class="rating-list">
							<?php
							$i = 0;
							while ( $i < $rating ) :
								?>
								<li>
									<img
											src="<?php echo esc_url( get_template_directory_uri() . '/images/star.svg' ); ?>"
											alt="star"
											width="17"
											height="17">
								</li>
								<?php
								$i ++;
							endwhile;
							?>
						</ul>
					</div>
					<div class="author-blockquote">
						<?php
						$thumbnail = get_field( 'thumbnail' );
						if ( $thumbnail ) :
							?>
							<div class="picture-holder">
								<img
										src="<?php echo esc_url( $thumbnail['url'] ); ?>"
										alt="<?php echo esc_attr( $thumbnail['alt'] ) ?? 'image description'; ?>"
										width="<?php esc_attr( the_field( 'width' ) ); ?>"
										height="<?php esc_attr( the_field( 'height' ) ); ?>">
							</div>
						<?php endif; ?>
						<div class="info-holder">
							<?php
							$author = get_field( 'author' );
							if ( $author ) :
								?>
								<div class="name">
									<?php echo esc_html( $author ); ?>
								</div>
							<?php endif; ?>
							<div class="date-holder">
								<?php the_field( 'location' ); ?>
								<?php the_time( 'a' ); ?>
								<time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>">
									<?php the_time( 'd.m.Y' ); ?>
								</time>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>
<?php
wp_reset_postdata();
