<?php if( have_rows('useful_links') ): ?>
	<section class="leader-section">
		<div class="container-fluid">
			<div class="row">
			    <?php  while ( have_rows('useful_links') ) : the_row(); ?>
					<div class="col-sm-6">
					    <?php if( $links_group = get_sub_field('links_group')): ?>
							<h2><?php echo $links_group; ?></h2>
						<?php endif; ?>
						<?php if( $thumbnail = get_sub_field('thumbnail')): ?>
							<div class="label-box">
								<img class="img-responsive" src="<?php echo $thumbnail['url']; ?>" alt="<?php if( $thumbnail['alt'] ) echo $thumbnail['alt']; else echo 'image description'; ?>" width="<?php the_sub_field('width'); ?>" height="<?php the_sub_field('height'); ?>">
							</div>
						<?php endif; ?>
						<?php if( have_rows('links') ): ?>
						<ul class="list">
						    <?php  while ( have_rows('links') ) : the_row(); ?>
						    	<?php $link_title = get_sub_field('link_title'); ?>
						    	<?php $link_url = get_sub_field('link_url'); ?>
						    	<?php if( $link_title || $link_url ): ?>
							    	<li><a href="<?php echo $link_url; ?>"><?php echo $link_title; ?></a></li>
								<?php endif; ?>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>	