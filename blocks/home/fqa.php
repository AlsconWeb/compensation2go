<?php if( $fqa = get_field('fqa') ): ?>
	<section class="tabs-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-offset-2 col-md-3 col-sm-5 hidden-xs">
					<div class="tabset">
					    <?php $k = 1; $key = 0; foreach( $fqa as $key => $q ): ?>
					        <?php if( $key > 4 ) break; ?>
							<div class="tab-holder active"><a class="open-tab" href="#tabs-0<?php echo $k; ?>"><?php echo $q['qestion']; ?></a></div>
						<?php $k++; $key++; endforeach; ?>	
						
						<div class="collapse tabs-container" id="collapse-tabs-list">
							<?php $k = 6; foreach( array_slice($fqa, 5) as $q ): ?>
								<div class="tab-holder"><a class="open-tab" href="#tabs-0<?php echo $k; ?>"><?php echo $q['qestion']; ?></a></div>
							<?php $k++; endforeach; ?>
						</div><a class="btn-more collapsed" data-toggle="collapse" href="#collapse-tabs-list" aria-expanded="false"><span class="collapsed"><?php _e('Mehr','compensation2go'); ?></span><span class="open"><?php _e('Weniger','compensation2go'); ?></span> <?php _e('anzeigen','compensation2go'); ?></a>
					</div>
				</div>
				<div class="col-md-5 col-sm-7">
					<div class="panel-group accordion-block" id="accordion">
					    <?php $k = 1; while ( have_rows('fqa') ) : the_row(); ?>
							<div class="panel panel-default">
							    <?php if( $qestion = get_sub_field('qestion') ): ?>
									<div class="panel-heading">
										<a class="<?php if($k != 1) echo 'collapsed'; ?> visible-xs-inline-block" href="#collapse-0<?php echo $k; ?>" data-parent="#accordion" data-toggle="collapse"><?php echo $qestion; ?></a>
									</div>
								<?php endif; ?>
								<?php if( $answer = get_sub_field('answer')): ?>
									<div class="panel-collapse collapse <?php if($k == 1) echo 'in'; ?>" id="collapse-0<?php echo $k; ?>">
										<div class="tab-pane" id="tabs-0<?php echo $k; ?>">
											<?php echo $answer; ?>
										</div>
									</div>
								<?php endif; ?>
							</div>
						<?php $k++; endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>