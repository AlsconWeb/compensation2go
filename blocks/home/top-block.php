<div class="container-fluid">
	<div class="row">
		<div class="col-sm-11"><span class="icon-arrow-left"></span>
			<?php the_title( '<h1>', '</h1>' ); ?>
			<!--			<div style="margin-bottom: 15px; background-color: #00C852; padding: 4px 15px; font-size: 16px; border-radius: 5px; color: #fff; display: inline-block; text-align: left">-->
			<!--				<strong>Bis zu 600€ pro Passagier</strong><br><span style="font-size: 14px">abzgl. Gebühr (29% zzgl. MwSt.)</span>-->
			<!--			</div>-->
			<div class="row">
				<div class="col-md-5 col-sm-7">
					<?php include get_template_directory() . '/blocks/form.php'; ?>

					<?php //include ( get_template_directory().'/inc/autocomplite.php' ); ?>
				</div>
			</div>
			<?php if ( have_rows( 'companies' ) ): ?>
				<div class="logos-holder">
					<?php if ( $featured_title = get_field( 'featured_title' ) ): ?>
						<span class="title"><?php echo $featured_title; ?></span>
					<?php endif; ?>
					<ul class="logos-list">
						<?php
						while ( have_rows( 'companies' ) ) :
							the_row();
							$logo = get_sub_field( 'logo' );
							?>
							<?php
							if ( $logo ) :
								$url = get_sub_field( 'url' );
								?>
								<li>
									<a href="<?php echo esc_url( $url ) ?? '#'; ?>">
										<img
												src="<?php echo esc_url( $logo['url'] ); ?>"
												alt="<?php echo esc_html( $logo['alt'] ) ?? 'image description'; ?>"
												width="<?php echo esc_attr( get_sub_field( 'width' ) ); ?>"
												height="<?php echo esc_attr( get_sub_field( 'height' ) ); ?>"
										/>
									</a>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					</ul>
				</div>
			<?php endif; ?>
			<!--			--><?php //if ( have_rows( 'advantages' ) ): ?>
			<!--				<ul class="benefits-list">-->
			<!--					--><?php //while ( have_rows( 'advantages' ) ) : the_row(); ?>
			<!--						--><?php //if ( $image = get_sub_field( 'image' ) ): ?>
			<!--							<li>-->
			<!--								<div class="picture-holder"><img class="img-responsive"-->
			<!--																 src="-->
			<?php //echo $image['url']; ?><!--"-->
			<!--																 alt="--><?php //if ( $image['alt'] ) {
			//									                                 echo $image['alt'];
			//								                                 } else {
			//									                                 echo 'image description';
			//								                                 } ?><!--" width="-->
			<?php //the_sub_field( 'width' ); ?><!--"-->
			<!--																 height="-->
			<?php //the_sub_field( 'height' ); ?><!--"/>-->
			<!--								</div>-->
			<!--								--><?php //if ( $item = get_sub_field( 'item' ) ): ?>
			<!--									<span class="text">--><?php //echo $item; ?><!--</span>-->
			<!--								--><?php //endif; ?>
			<!--							</li>-->
			<!--						--><?php //endif; ?>
			<!--					--><?php //endwhile; ?>
			<!--				</ul>-->
			<!--			--><?php //endif; ?>
		</div>
	</div>
</div>