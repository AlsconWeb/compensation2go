<?php
$pid = get_the_ID();
?>

<?php if ( have_rows( 'variations' ) ): ?>
	<section class="info-section">
		<div class="bg-retina">
			<span data-srcset="<?php echo get_template_directory_uri(); ?>/images/img-02.jpg, <?php echo get_template_directory_uri(); ?>/images/img-02@2x.jpg 2x"></span><span
					data-srcset="<?php echo get_template_directory_uri(); ?>/images/img-02.jpg"
					data-media="(min-width: 768px)"></span>
		</div>
		<div class="container-fluid">
			<?php $section_title = get_field( 'section_title', $pid, false ); ?>
			<?php $description = get_field( 'description', $pid, false ); ?>
			<?php if ( $section_title || $description ): ?>
			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
					<div class="text-holder">
						<?php if ( $section_title ): ?>
							<h2><?php echo $section_title; ?></h2>
						<?php endif; ?>
						<?php echo $description; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="container-fluid">
			<div class="row">
				<?php while ( have_rows( 'variations' ) ) : the_row(); ?>
					<div class="col-md-3 <?php if ( get_sub_field( 'mark', $pid ) ) {
						echo 'large-col';
					} ?>">
						<div class="info-box">
							<?php if ( $title = get_sub_field( 'title', $pid ) ): ?>
								<span class="title"><?php echo $title; ?></span>
							<?php endif; ?>
							<?php if ( get_sub_field( 'mark', $pid ) ): ?>
								<span class="title"><img class="img-responsive"
														 src="<?php echo get_template_directory_uri(); ?>/images/logo-white.svg"
														 alt="<?php echo bloginfo( 'description' ); ?>" width="207"
														 height="49"></span>
							<?php endif; ?>
							<?php if ( have_rows( 'items' ) ): ?>
								<ul class="list">
									<?php while ( have_rows( 'items' ) ) : the_row(); ?>
										<li>
											<?php if ( $item_title = get_sub_field( 'item_title', $pid ) ): ?>
												<span class="title"><?php echo $item_title; ?></span>
											<?php endif; ?>
											<?php if ( $description = get_sub_field( 'description', $pid ) ): ?>
												<span class="item"><?php echo $description; ?></span>
											<?php endif; ?>
											<?php if ( $note = get_sub_field( 'note', $pid ) ): ?>
												<span class="note"><?php echo $note; ?></span>
											<?php endif; ?>
										</li>
									<?php endwhile; ?>
								</ul>
							<?php endif; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
		<?php if ( $note = get_field( 'note', $pid ) ): ?>
			<div class="container-fluid">
				<div class="row"></div>
				<div class="col-md-offset-2 col-md-8"><span class="note"><?php echo $note; ?></span></div>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>