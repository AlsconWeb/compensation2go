<?php
	the_posts_pagination( array(
		'prev_text' => __( 'Previous page', 'compensation2go' ),
		'next_text' => __( 'Next page', 'compensation2go' ),
	) );
?>
