<?php require( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' ); ?>
<form class="booking-form" action="https://form.compensation2go.com/">
	<!-- <fieldset>
        <!--<?php if ( is_single() ): ?>
		    	<span class="title"><?php _e( 'Claim your immediate compensation', 'compensation2go' ); ?></span>
			<?php endif; ?>-->

	<!--  <ul class="form-benefits">
            <li><img src="https://compensation2go.com/wp-content/uploads/2017/03/check.svg" /> <b>Auszahlung in 24h</b> nach Auftrag</li>
            <li><img src="https://compensation2go.com/wp-content/uploads/2017/03/check.svg" /> Bis 600€ abzgl. 34,5% Gebühr <span style="font-size: 13px;">(inkl. MwSt.)</span></li>
            <li><img src="https://compensation2go.com/wp-content/uploads/2017/03/check.svg" /> Verfahren gegen die Airline ersparen</li>
        </ul>
        <div class="autocomplete-form">
            <div class="form-group">
                <div class="picture-box"><img src="<?php echo get_template_directory_uri(); ?>/images/departure.svg" alt="image description" width="22" height="20"></div>
                <input class="autocomplete form-control" type="text" data-source="<?php echo get_template_directory_uri(); ?>/inc/autocomplite.php" name="departure" autocomplete="off" placeholder="Abflughafen" data-required="true" data-min="2">
            </div>
            <div class="form-group">
                <div class="picture-box"><img src="<?php echo get_template_directory_uri(); ?>/images/arrival.svg" alt="image description" width="22" height="20"></div>
                <input class="autocomplete form-control" data-source="<?php echo get_template_directory_uri(); ?>/inc/autocomplite.php" name="arrival" autocomplete="off" type="text" placeholder="Zielflughafen" data-required="true" data-min="2">
            </div>
            <button class="btn btn-green btn-block" type="submit" style="color: #fff"><strong><?php _e( 'Jetzt kostenlos prüfen', 'compensation2go' ); ?></strong> (30 Sek.)</button>
        </div>
    </fieldset>-->
	<fieldset>
		<div class="autocomplete-form">
			<span style="display: block; font-size: 18px; font-weight: bold; margin-bottom: 10px;">Wie können wir Ihnen helfen?</span>
			<a class="btn btn-green btn-block" style="color: #fff" href="https://coronaritter.de"
			   target="_blank"><strong>Erstattung für abgesagten Flug</strong></a>

			<a class="btn btn-green btn-block" style="color: #fff" href="https://form.compensation2go.com"
			   target="_blank"><strong>Mein Flug war verspätet</strong></a>


		</div>
	</fieldset>

</form>
<div style="display: block; margin-top: 10px; text-align: left">
	<img style="width: 140px; height: auto; position: relative; left: -10px;"
		 src="https://compensation2go.com/layout-media/finanztip.png" alt="Testergebnis bei Flugverspätung">
	<a href="https://www.ausgezeichnet.org/bewertungen-compensation2go.com-6FZAT6" alt="TOP-Dienstleister 2019"
	   target="_blank">
		<img style="width: 137px; height: auto; display: inline-block; position:relative; top: -4px"
			 src="https://compensation2go.com/layout-media/topdienstleister.png" alt="Testergebnis bei Flugverspätung">
	</a>
</div>