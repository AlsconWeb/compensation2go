<?php
/**
 * Created 21.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Widgets.
 */

namespace IWP\Widgets;

use WP_Widget;

/**
 * Custom Text Widget without <div>.
 * Class file Custom_Widget_Text.
 */
class CustomWidgetText extends WP_Widget {

	/**
	 * Custom_Widget_Text construct.
	 */
	public function __construct() {
		$widget_ops  = [
			'classname'   => 'widget_text',
			'description' => __( 'Arbitrary text or HTML', 'compensation2go' ),
		];
		$control_ops = [
			'width'  => 400,
			'height' => 350,
		];
		parent::__construct( 'text', __( 'Text', 'compensation2go' ), $widget_ops, $control_ops );
	}

	/**
	 * Widget Output.
	 *
	 * @param array $args     Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ): void {
		$before_widget = $args['before_widget'];
		$after_title   = $args['after_title'];
		$before_title  = $args['before_title'];
		$after_widget  = $args['after_widget'];

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$text  = apply_filters( 'widget_text', $instance['text'], $instance );

		echo wp_kses_post( $before_widget );
		if ( ! empty( $title ) ) {
			echo wp_kses_post( $before_title . $title . $after_title );
		}
		echo $instance['filter'] ? wp_kses_post( wpautop( $text ) ) : wp_kses_post( $text );
		echo wp_kses_post( $after_widget );
	}

	/**
	 * Update settings widget.
	 *
	 * @param array $new_instance New Parameters.
	 * @param array $old_instance Old Parameters.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ): array {
		$instance          = $old_instance;
		$instance['title'] = wp_strip_all_tags( $new_instance['title'] );
		if ( current_user_can( 'unfiltered_html' ) ) {
			$instance['text'] = $new_instance['text'];
		} else {
			$instance['text'] = stripslashes( wp_filter_post_kses( addslashes( $new_instance['text'] ) ) );
		}
		// wp_filter_post_kses() expects slashed.
		$instance['filter'] = isset( $new_instance['filter'] );

		// replace site-url by shortcodes.
		$instance['text'] = str_replace( get_template_directory_uri(), '[template-url]', $instance['text'] );
		$instance['text'] = str_replace( home_url(), '[site-url]', $instance['text'] );

		return $instance;
	}

	/**
	 * Form widget in admin.
	 *
	 * @param array|object $instance Instance.
	 *
	 * @return void
	 */
	public function form( $instance ): void {

		$instance = (object) wp_parse_args(
			(array) $instance,
			[
				'title' => '',
				'text'  => '',
			]
		);

		$title = wp_strip_all_tags( $instance->title );
		$text  = esc_textarea( $instance->text );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:', 'compensation2go' ); ?>
			</label>
			<input
					class="widefat"
					id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text"
					value="<?php echo esc_attr( $title ); ?>"/></p>

		<textarea
				class="widefat"
				rows="16"
				cols="20"
				id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>">
			<?php echo esc_html( $text ); ?>
		</textarea>

		<p>
			<input
					id="<?php echo esc_attr( $this->get_field_id( 'filter' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'filter' ) ); ?>"
					type="checkbox" <?php checked( isset( $instance->filter ) ? esc_attr( $instance->filter ) : 0 ); ?>
			/>&nbsp;
			<label for="<?php echo esc_attr( $this->get_field_id( 'filter' ) ); ?>">
				<?php esc_html_e( 'Automatically add paragraphs', 'compensation2go' ); ?></label>
		</p>
		<?php
	}
}
