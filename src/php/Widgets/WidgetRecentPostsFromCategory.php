<?php
/**
 * Created 21.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Widgets
 */

namespace IWP\Widgets;

use WP_Query;
use WP_Widget;

/**
 * WidgetRecentPostsFromCategory file class.
 */
class WidgetRecentPostsFromCategory extends WP_Widget {
	/**
	 * WidgetRecentPostsFromCategory construct.
	 */
	public function __construct() {
		$widget_ops = [
			'classname'   => 'widget_recent_entries_from_category',
			'description' => __( 'The most recent posts from specific category on your site', 'compensation2go' ),
		];
		parent::__construct( 'recent-posts-from-category', __( 'Recent Posts From Specific Category', 'compensation2go' ), $widget_ops );
		$this->alt_option_name = 'widget_recent_entries_from_category';

		add_action( 'save_post', [ $this, 'flush_widget_cache' ] );
		add_action( 'deleted_post', [ $this, 'flush_widget_cache' ] );
		add_action( 'switch_theme', [ $this, 'flush_widget_cache' ] );
	}

	/**
	 * Widget Output.
	 *
	 * @param array $args     Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ): void {

		$cache = wp_cache_get( 'widget_recent_posts_from_category', 'widget' );

		if ( ! is_array( $cache ) ) {
			$cache = [];
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo esc_attr( $cache[ $args['widget_id'] ] );

			return;
		}

		ob_start();

		$before_widget = $args['before_widget'];
		$after_title   = $args['after_title'];
		$before_title  = $args['before_title'];
		$after_widget  = $args['after_widget'];

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Posts', 'compensation2go' ) : $instance['title'], $instance, $this->id_base );

		$number = absint( $instance['number'] );
		if ( empty( $number ) ) {
			$number = 10;
		}

		$query = new WP_Query(
			[
				'posts_per_page'      => $number,
				'no_found_rows'       => true,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => true,
				'cat'                 => $instance['cat'],
			]
		);
		if ( $query->have_posts() ) : ?>
			<?php echo wp_kses_post( $before_widget ); ?>
			<?php
			if ( $title ) {
				echo wp_kses_post( $before_title . $title . $after_title );
			}
			?>
			<ul>
				<?php
				while ( $query->have_posts() ) :
					$query->the_post();
					?>
					<li>
						<a
								href="<?php the_permalink(); ?>"
								title="<?php echo esc_attr( get_the_title() ?: get_the_ID() ); ?>">
							<?php
							if ( get_the_title() ) {
								the_title();
							} else {
								the_ID();
							}
							?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
			<?php echo wp_kses_post( $after_widget ); ?>
			<?php
			wp_reset_postdata();

		endif;

		$cache[ $args['widget_id'] ] = ob_get_flush();
		wp_cache_set( 'widget_recent_posts_from_category', $cache, 'widget' );
	}

	/**
	 * Update data.
	 *
	 * @param array $new_instance New Instance.
	 * @param array $old_instance Old Instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ): array {
		$instance           = $old_instance;
		$instance['title']  = wp_strip_all_tags( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['cat']    = (int) $new_instance['cat'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );

		if ( isset( $alloptions['widget_recent_entries_from_category'] ) ) {
			delete_option( 'widget_recent_entries_from_category' );
		}

		return $instance;
	}

	/**
	 * Delete Cache.
	 */
	public function flush_widget_cache(): void {
		wp_cache_delete( 'widget_recent_posts_from_category', 'widget' );
	}

	/**
	 * Form output to admin panel.
	 *
	 * @param array $instance Instance.
	 *
	 * @return void
	 */
	public function form( $instance ): void {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$cat    = $instance['cat'] ?? 0;
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:', 'compensation2go' ); ?>
			</label>
			<input
					class="widefat"
					id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
					type="text"
					value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
			<label>
				<?php esc_html_e( 'Category', 'compensation2go' ); ?>:
				<?php
				wp_dropdown_categories(
					[
						'name'     => $this->get_field_name( 'cat' ),
						'selected' => $cat,
					]
				);
				?>
			</label>
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>">
				<?php esc_html_e( 'Number of posts to show:', 'compensation2go' ); ?>
			</label>
			<input
					id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>"
					type="text"
					value="<?php echo esc_attr( $number ); ?>"
					size="3"/>
		</p>
		<?php
	}
}
