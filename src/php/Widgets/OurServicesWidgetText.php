<?php
/**
 * Created 21.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Widgets
 */

namespace IWP\Widgets;

use WP_Widget;

/**
 * OurServicesWidgetText class file.
 */
class OurServicesWidgetText extends WP_Widget {
	/**
	 * OurServicesWidgetText  construct.
	 */
	public function __construct() {
		$widget_ops  = [
			'classname'   => 'services_text',
			'description' => __( 'Our services', 'compensation2go' ),
		];
		$control_ops = [
			'width'  => 400,
			'height' => 350,
		];
		parent::__construct( 'services', __( 'Our services', 'compensation2go' ), $widget_ops, $control_ops );
	}

	/**
	 * Widget Output.
	 *
	 * @param array $args     Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ): void {
		$before_widget = $args['before_widget'];
		$after_widget  = $args['after_widget'];
		$widget_id     = $args['widget_id'];

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		echo wp_kses_post( $before_widget );

		if ( have_rows( 'our_services', 'widget_' . $widget_id ) ) :
			?>
			<div class="white-box">
				<?php
				if ( ! empty( $title ) ) {
					echo '<header class="head"><span class="title">' . esc_html( $title ) . '</span></header>';
				}
				?>
				<ul class="benefits-list">
					<?php
					while ( have_rows( 'our_services', 'widget_' . $widget_id ) ) :
						the_row();
						?>
						<li>
							<?php
							$image = get_sub_field( 'image', 'widget_' . $widget_id );
							if ( $image ) :
								?>
								<div class="picture-holder">
									<img
											class="img-responsive" src="<?php echo esc_url( $image['url'] ); ?>"
											alt="image description"
											width="<?php the_sub_field( 'width', 'widget_' . $widget_id ); ?>"
											height="<?php the_sub_field( 'height', 'widget_' . $widget_id ); ?>"/>
								</div>
							<?php endif; ?>
							<?php
							$title = get_sub_field( 'title', 'widget_' . $widget_id );
							if ( $title ) :
								?>
								<span class="text"><?php echo esc_html( $title ); ?></span>
							<?php endif; ?>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
		<?php
		endif;
		echo wp_kses_post( $after_widget );
	}

	/**
	 * Update
	 *
	 * @param array $new_instance New Instance.
	 * @param array $old_instance Old Instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ): array {

		$instance          = $old_instance;
		$instance['title'] = wp_strip_all_tags( $new_instance['title'] );

		if ( current_user_can( 'unfiltered_html' ) ) {
			$instance['services'] = $new_instance['services'];
		} else {
			$instance['services'] = stripslashes( wp_filter_post_kses( addslashes( $new_instance['services'] ) ) );
		}

		$instance['filter'] = isset( $new_instance['filter'] );

		// replace site-url by shortcodes.
		$instance['services'] = str_replace( get_template_directory_uri(), '[template-url]', $instance['services'] );
		$instance['services'] = str_replace( home_url(), '[site-url]', $instance['services'] );

		return $instance;
	}

	/**
	 * Form output to admin panel.
	 *
	 * @param array|object $instance Instance.
	 *
	 * @return void
	 */
	public function form( $instance ): void {
		$instance = (object) wp_parse_args(
			(array) $instance,
			[
				'title'    => '',
				'services' => '',
			]
		);
		$title    = wp_strip_all_tags( $instance->title );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_html_e( 'Title:', 'compensation2go' ); ?>
			</label>
			<input
					class="widefat"
					id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
					type="text"
					value="<?php echo esc_attr( $title ); ?>"/>
		</p>


		<p>
			<input
					id="<?php echo esc_attr( $this->get_field_id( 'filter' ) ); ?>"
					name="<?php echo esc_attr( $this->get_field_name( 'filter' ) ); ?>"
					type="checkbox" <?php checked( $instance->filter ?? 0 ); ?> />&nbsp;
			<label for="<?php echo esc_attr( $this->get_field_id( 'filter' ) ); ?>">
				<?php esc_html_e( 'Automatically add paragraphs', 'compensation2go' ); ?>
			</label>
		</p>
		<?php
	}
}

