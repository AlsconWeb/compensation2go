<?php $sq = get_search_query() ? get_search_query() : __( 'Enter search terms&hellip;', 'compensation2go' ); ?>
<form method="get" class="search-form" action="<?php echo home_url(); ?>" >
	<fieldset>
		<input type="search" name="s" placeholder="" value="<?php echo get_search_query(); ?>" />
		<input type="submit" value="<?php _e( 'Search', 'compensation2go' ); ?>" />
	</fieldset>
</form>