<?php
/*
Template Name: Home Template
*/

get_header();
$pid = get_the_ID();
?>

<?php
while ( have_posts() ) :
	the_post();
	?>
	<div class="home-banner">
		<?php $banner_id = get_field( 'banner', $pid ); ?>
		<?php if ( $banner_id ) : ?>
			<div class="bg-retina">
				<span
						data-srcset="<?php echo esc_url( wp_get_attachment_image_url( $banner_id, 'thumbnail_1256x672' ) ); ?>,
						<?php echo esc_url( wp_get_attachment_image_url( $banner_id, 'thumbnail_2513x1345' ) ); ?> 2x">
				</span>
				<span
						data-srcset="<?php echo esc_url( wp_get_attachment_image_url( $banner_id, 'thumbnail_1256x672' ) ); ?>"
						data-media="(min-width: 768px)">

				</span>
			</div>
			<div class="bg-retina bg-mobile">
				<span
						data-srcset="<?php echo esc_url( get_site_url() . '/wp-content/themes/compensation2go-website/images/bg-mobile.jpg' ); ?>,
						<?php echo esc_url( get_site_url() . '/wp-content/themes/compensation2go/images/bg-mobile.jpg 2x' ); ?>">

				</span>
			</div>
		<?php endif; ?>
		<?php get_template_part( 'blocks/home/top-block' ); ?>
	</div>
	<?php get_template_part( 'blocks/home/works-block' ); ?>
	<?php get_template_part( 'blocks/home/testimonials' ); ?>
	<?php get_template_part( 'blocks/home/variations' ); ?>
	<?php get_template_part( 'blocks/home/fqa' ); ?>
	<?php if ( have_rows( 'logos' ) ) : ?>
	<section class="logos-section">
		<div class="container-fluid">
			<div class="row">
				<?php
				while ( have_rows( 'logos' ) ) :
					the_row();
					?>
					<div class="col-md-3 col-sm-6">
						<?php
						the_sub_field( 'description' );
						$image = get_sub_field( 'image', $pid );
						?>
						<?php if ( $image ) : ?>
							<div class="picture-holder">
								<img
										src="<?php echo esc_url( $image['url'] ); ?>"
										alt="<?php ! empty( $image['alt'] ) ? esc_attr( $image['alt'] ) : 'image description'; ?>"
										width="<?php esc_attr( the_sub_field( 'width' ) ); ?>"
										height="<?php esc_attr( the_sub_field( 'height' ) ); ?>">
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
	<?php get_template_part( 'blocks/home/post' ); ?>
	
	<?php get_template_part( 'blocks/home/description' ); ?>
<?php endwhile; ?>
<?php
get_footer();
