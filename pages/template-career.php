<?php
/*
Template Name: Career
*/
get_header(); ?>
<div class="header-career">
    <h1>Join the legal tech movement</h1>
    <h2>Stellenangebote bei C2Go</h2>
    <a class="btn-danger header-button" href="#positions">Offene Positionen</a>
    <div class="header-background-career"></div>
</div>
<section class="what-we-do">
    <div class="container-fluid">
        <h6>Unsere Produkte</h6>
        <div class="col-sm-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/c2go.png">
            <span><strong>Compensation2Go</strong> - Sofortentschädigung für Flugstörungen</span>
        </div>
        <div class="col-sm-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/autopilot261.png">
            <span><strong>autopilot261</strong> - Datengestützte Auswertung von Ausgleichsansprüchen</span>
        </div>
        <div class="col-sm-4">
            <img style="width: 150px;" src="<?php echo get_template_directory_uri(); ?>/images/airlytics.png">
            <span><strong>Airlytics</strong> - Datenintelligenz für Fluggäste</span>
        </div>
    </div>
</section>
    <div class="container-fluid">
		<div id="content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
				<?php edit_post_link( __( 'Edit', 'compensation2go' ) ); ?>
			<?php endwhile; ?>
			<?php wp_link_pages(); ?>
			<?php comments_template(); ?>
		</div>
	</div>
<?php get_footer(); ?>
<style>
    .header-background-career {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/particles.jpg");
        background-size: 800px auto;
        background-position: 0 0;
        background-position: center bottom;
        background-repeat: no-repeat;
        width: 100%;
        height: 350px;
        position: absolute;
        z-index: -1;
        opacity: 0.5;
        top:-0px;
    }
    .header-career {
        height: 350px;
        width: 100%;
        position: relative;
        z-index: 1;
        background-color: #fff;
        text-align: center;
        padding-top: 80px;
        
    }
    .header-button {
        padding: 10px 20px;
        border-radius: 2px;
        margin-top: 20px;
        display: block;
        width: 170px;
        margin: 0px auto;
    }
    .what-we-do h6 {
        display: block;
        background-color: #fff;
        padding: 10px;
        border-radius: 20px;
        width: 150px;
        position: relative;
        top: -25px;
        margin: 0px auto;
        z-index: 3;
        color: #213347;
        border: 3px solid #0091EA
    }
    .what-we-do {
        background-color: #0091EA;
        color: #fff;
        padding-top: 10px;
        padding-bottom: 15px;
        text-align: center;
    }
    .what-we-do img {
        width: 170px;
        display: block;
        margin: 0px auto
    }
    .what-we-do span {
        padding-top: 10px;
        display: block;
        padding-bottom: 20px;
    }
    .career-benefits li {
        position: relative;  
        padding-right: 15px;
        padding-left: 10px;
    }
    .career-benefits img {
        position: absolute;
        width: 40px;
        height: auto;
        left: -45px;
        top: 3px;
    }
    img.slick-slide-image {
        border-radius: 3px;
    }
    
</style>