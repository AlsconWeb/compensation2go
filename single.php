<?php get_header(); ?>
	<div class="breadcrumb-holder">
		<div class="container-fluid" typeof="BreadcrumbList" vocab="https://schema.org/">
		    <ul class="breadcrumb">
			    <?php if(function_exists('bcn_display'))
			    {
			        bcn_display();
			    }?>
		    </ul>
		</div>
	</div>
	<?php while ( have_posts() ) : the_post(); ?>
	<div class="banner">
	    <?php if( has_post_thumbnail()): ?>
			<div class="bg-retina">
				<span data-srcset="<?php the_post_thumbnail_url( 'thumbnail_1200x300' ); ?>, <?php the_post_thumbnail_url( 'thumbnail_2400x600' ); ?> 2x"></span><span data-srcset="<?php the_post_thumbnail_url( 'thumbnail_1200x300' ); ?>" data-media="(min-width: 768px)"></span>
			</div>
		<?php endif; ?>
		<div class="container-fluid">
			<div class="two-col">
				<div class="large-col">
					<div class="box">
						<?php the_title( '<h1>', '</h1>' ); ?>
						<span class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></span>
						<!--<ul class="share-list">
							<li>
								<div class="fb-like" data-href="http://compensation2go.stgng.com" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
							</li>
							<li>
								<div class="st_linkedin_custom"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.svg" alt="image description" width="15" height="15"><?php _e('Share','compensation2go'); ?></div>
							</li>
						</ul>-->
					</div>
				</div>
				<?php if( get_field('display_search_form',get_the_ID())): ?>
					<div class="small-col">
						<?php include(TEMPLATEPATH.'/blocks/form.php'); ?>
						<?php if( $form_note = get_field('form_note')): ?>
							<span class="note"><?php echo $form_note; ?></span>
						<?php endif; ?>	
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div id="content">
				<?php remove_filter( 'the_content', 'wpautop' ); ?>
				<?php the_content(); ?>
			<ul class="share-list">
				<!--<ul class="share-list">
					<li>
						<div class="fb-like" data-href="http://compensation2go.stgng.com" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
					</li>
					<li>
						<div class="st_linkedin_custom"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.svg" alt="image description" width="15" height="15"><?php _e('Share','compensation2go'); ?></div>
					</li>
				</ul>-->
			</ul>	
		</div>
		<?php get_sidebar(); ?>
	</div>
	<?php get_template_part( 'blocks/recommended-post' ); ?>
	<section class="table-section">
		<div class="container-fluid">
			<?php if( $table_title = get_field('table_title')): ?>
				<h2><?php echo $table_title; ?></h2>
			<?php endif; ?>
			<?php if( $table_shortcode = get_field('table_shortcode')): ?>
            	<?php echo do_shortcode($table_shortcode); ?>
			<?php endif; ?>
		</div>
	</section>
<?php endwhile; ?>	
<?php get_footer(); ?>